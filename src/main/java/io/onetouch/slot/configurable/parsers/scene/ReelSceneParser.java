package io.onetouch.slot.configurable.parsers.scene;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.BasePaytableProvider;
import io.onetouch.slot.core.ReelsProvider;
import io.onetouch.slot.core.SceneBehavior;
import io.onetouch.slot.core.SlotSceneCalculator;
import io.onetouch.slot.core.scene.ReelScene;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.*;

@SuppressWarnings("unused")
public class ReelSceneParser implements GenericDataParser<ReelScene> {

    @SuppressWarnings("unchecked")
    @Override
    public ReelScene parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return ReelScene.builder()
                .id(string(element, "id").orElseThrow())
                .credits(bigDecimal(element, "credits").orElseThrow())
                .reelsProvider((ReelsProvider) external(element, "reelsProvider", dataSupplier).orElseThrow())
                .paytableProvider((BasePaytableProvider) external(element, "paytableProvider", dataSupplier).orElseThrow())
                .calculators((List<SlotSceneCalculator>) external(element, "calculators", dataSupplier).orElseThrow())
                .behavior((SceneBehavior) external(element, "behavior", dataSupplier).orElseThrow())
                .build();
    }
}
