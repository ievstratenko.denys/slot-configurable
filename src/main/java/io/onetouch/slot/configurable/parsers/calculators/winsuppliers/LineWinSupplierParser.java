package io.onetouch.slot.configurable.parsers.calculators.winsuppliers;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.SlotGameManager;
import io.onetouch.slot.core.calculators.winCalculator.winSuppliers.LineWinSupplier;

import java.math.BigDecimal;
import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.external;

@SuppressWarnings("unused")
public class LineWinSupplierParser implements GenericDataParser<LineWinSupplier> {

    @SuppressWarnings("unchecked")
    @Override
    public LineWinSupplier parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new LineWinSupplier(
                (Function<SlotGameManager, BigDecimal>) external(element, "winMultiplierSupplier", dataSupplier).orElse(null)
        );
    }
}
