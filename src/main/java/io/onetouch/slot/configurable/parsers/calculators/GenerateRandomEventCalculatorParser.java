package io.onetouch.slot.configurable.parsers.calculators;

import io.onetouch.core.rng.WeightEntry;
import io.onetouch.slot.api.SlotEvent;
import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.calculators.generateEvent.GenerateRandomEventCalculator;
import io.onetouch.slot.core.dataproviders.one.ConstantOne;
import io.onetouch.slot.core.dataproviders.one.One;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.external;

@SuppressWarnings("unused")
public class GenerateRandomEventCalculatorParser implements GenericDataParser<GenerateRandomEventCalculator> {

    @SuppressWarnings("unchecked")
    @Override
    public GenerateRandomEventCalculator parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        One<List<WeightEntry<SlotEvent>>> eventsProvider = external(element, "events", dataSupplier)
                .map(events -> (List<WeightEntry<SlotEvent>>) events)
                .map(events -> (One<List<WeightEntry<SlotEvent>>>) new ConstantOne<>(events))
                .orElseGet(() -> (One<List<WeightEntry<SlotEvent>>>) external(element, "eventsProvider", dataSupplier).orElseThrow());
        return new GenerateRandomEventCalculator(eventsProvider);
    }
}
