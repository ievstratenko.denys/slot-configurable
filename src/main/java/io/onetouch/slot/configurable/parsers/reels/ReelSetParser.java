package io.onetouch.slot.configurable.parsers.reels;

import io.onetouch.slot.api.Reel;
import io.onetouch.slot.api.SlotSymbol;
import io.onetouch.slot.api.SlotSymbolType;
import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.reels.BaseReel;
import io.onetouch.slot.core.reels.BaseSlotSymbol;
import io.onetouch.slot.core.reels.ReelSet;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.list;
import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.string;

@SuppressWarnings("unused")
public class ReelSetParser implements GenericDataParser<ReelSet> {

    private static final String WEIGHT_SEPARATOR = "_w";

    @SuppressWarnings("unchecked")
    @Override
    public ReelSet parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        String id = string(element, "id").orElseThrow();
        List<Reel> reels = list(element, "reels").orElseThrow()
                .stream()
                .map(e -> (List<String>) e)
                .map(this::convert)
                .collect(Collectors.toList());
        return new ReelSet(id, reels);
    }

    private Reel convert(List<String> strings) {
        List<SlotSymbol> symbols = strings.stream()
                .map(this::buildSymbol)
                .collect(Collectors.toList());
        return new BaseReel(symbols);
    }

    private BaseSlotSymbol buildSymbol(String typeAndWeight) {
        String[] parts = typeAndWeight.split(WEIGHT_SEPARATOR);
        SlotSymbolType type = SlotSymbolType.valueOf(parts[0]);
        Integer weight = parts.length > 1 ? Integer.parseInt(parts[1]) : null;
        return new BaseSlotSymbol(type, weight);
    }
}
