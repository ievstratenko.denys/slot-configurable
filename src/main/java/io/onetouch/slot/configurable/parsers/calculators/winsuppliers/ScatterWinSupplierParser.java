package io.onetouch.slot.configurable.parsers.calculators.winsuppliers;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.SlotGameManager;
import io.onetouch.slot.core.calculators.winCalculator.winSuppliers.ScatterWinSupplier;

import java.math.BigDecimal;
import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.external;

@SuppressWarnings("unused")
public class ScatterWinSupplierParser implements GenericDataParser<ScatterWinSupplier> {

    @SuppressWarnings("unchecked")
    @Override
    public ScatterWinSupplier parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new ScatterWinSupplier(
                (Function<SlotGameManager, BigDecimal>) external(element, "winMultiplierSupplier", dataSupplier).orElse(null)
        );
    }
}
