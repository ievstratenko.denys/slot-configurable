package io.onetouch.slot.configurable.parsers.dataproviders.many;

import io.onetouch.slot.configurable.parsers.common.ListOfReferencesParser;
import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.dataproviders.many.IntersectionMany;
import io.onetouch.slot.core.dataproviders.many.Many;

import java.util.Map;
import java.util.function.Function;

@SuppressWarnings("unused")
public class IntersectionManyParser implements GenericDataParser<IntersectionMany<?>> {

    private final ListOfReferencesParser listOfReferencesParser = new ListOfReferencesParser();

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    public IntersectionMany<?> parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new IntersectionMany(
                listOfReferencesParser.parse(element, "providers", dataSupplier, Many.class)
        );
    }
}
