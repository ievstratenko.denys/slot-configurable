package io.onetouch.slot.configurable.parsers.calculators.settings;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.calculators.addFreeSpins.CombinationBasedAddFreeSpinsSettingsSupplier;
import io.onetouch.slot.core.calculators.winCalculator.CombinationSupplier;

import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.external;

@SuppressWarnings("unused")
public class CombinationBasedAddFreeSpinsSettingsParser implements GenericDataParser<CombinationBasedAddFreeSpinsSettingsSupplier> {

    @SuppressWarnings("unchecked")
    @Override
    public CombinationBasedAddFreeSpinsSettingsSupplier parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new CombinationBasedAddFreeSpinsSettingsSupplier(
                (CombinationSupplier) external(element, "combinationSupplier", dataSupplier).orElseThrow(),
                (Map<Integer, Integer>) external(element, "spinsLengthMapping", dataSupplier).orElseThrow()
        );
    }
}
