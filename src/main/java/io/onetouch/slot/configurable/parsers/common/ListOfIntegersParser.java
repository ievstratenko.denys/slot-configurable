package io.onetouch.slot.configurable.parsers.common;

import io.onetouch.slot.configurable.parsing.GenericDataParser;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.list;

@SuppressWarnings("unused")
public class ListOfIntegersParser implements GenericDataParser<List<Integer>> {

    @Override
    public List<Integer> parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return list(element, Integer.class).orElseThrow();
    }
}
