package io.onetouch.slot.configurable.parsers.gamepredicates;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.gamepredicates.HasPayIdPredicate;

import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.string;

@SuppressWarnings("unused")
public class HasPayIdPredicateParser implements GenericDataParser<HasPayIdPredicate> {

    @Override
    public HasPayIdPredicate parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new HasPayIdPredicate(string(element, "payId").orElseThrow());
    }
}
