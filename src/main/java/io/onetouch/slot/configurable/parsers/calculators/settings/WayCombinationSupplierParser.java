package io.onetouch.slot.configurable.parsers.calculators.settings;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.calculators.winCalculator.CombinationSettings;
import io.onetouch.slot.core.calculators.winCalculator.combinationSuppliers.FirstPositionLineBuilder;
import io.onetouch.slot.core.calculators.winCalculator.combinationSuppliers.WayCombinationSupplier;
import io.onetouch.slot.core.reels.BaseLine;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.external;

@SuppressWarnings("unused")
public class WayCombinationSupplierParser implements GenericDataParser<WayCombinationSupplier> {

    @SuppressWarnings("unchecked")
    @Override
    public WayCombinationSupplier parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new WayCombinationSupplier(
                (List<BaseLine>) external(element, "paylines", dataSupplier).orElseThrow(),
                (List<CombinationSettings>) external(element, "combinationSettings", dataSupplier).orElseThrow(),
                new FirstPositionLineBuilder()
        );
    }
}
