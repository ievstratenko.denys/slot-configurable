package io.onetouch.slot.configurable.parsers.reels;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.configurable.parsing.SlotConfigParserUtils;
import io.onetouch.slot.core.ReelsProvider;
import io.onetouch.slot.core.reels.providers.SymbolShufflingReelsProvider;

import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.external;

@SuppressWarnings("unused")
public class SymbolShufflingReelsProviderParser implements GenericDataParser<SymbolShufflingReelsProvider> {

    @Override
    public SymbolShufflingReelsProvider parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new SymbolShufflingReelsProvider(
                (ReelsProvider) external(element, "delegate", dataSupplier).orElseThrow(),
                SlotConfigParserUtils.<Integer>many(element, "freezePositions", dataSupplier).orElse(null)
        );
    }
}
