package io.onetouch.slot.configurable.parsers.gamepredicates.generic;

import io.onetouch.slot.configurable.parsers.common.ListOfReferencesParser;
import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.dataproviders.one.One;
import io.onetouch.slot.core.gamepredicates.generic.AllEqualsPredicate;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
public class AllEqualsPredicateParser implements GenericDataParser<AllEqualsPredicate> {

    private final ListOfReferencesParser listOfReferencesParser = new ListOfReferencesParser();

    @Override
    public AllEqualsPredicate parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new AllEqualsPredicate(
                listOfReferencesParser.parse(element, "values", dataSupplier, One.class).stream()
                        .map(v -> (One<?>) v)
                        .collect(Collectors.toList())
        );
    }
}
