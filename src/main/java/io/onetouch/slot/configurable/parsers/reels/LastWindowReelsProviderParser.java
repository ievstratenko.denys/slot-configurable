package io.onetouch.slot.configurable.parsers.reels;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.reels.providers.LastWindowReelsProvider;
import io.onetouch.slot.core.reels.SceneSize;

import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.external;

@SuppressWarnings("unused")
public class LastWindowReelsProviderParser implements GenericDataParser<LastWindowReelsProvider> {

    @Override
    public LastWindowReelsProvider parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new LastWindowReelsProvider(
                (SceneSize) external(element, "sceneSize", dataSupplier).orElseThrow()
        );
    }
}
