package io.onetouch.slot.configurable.parsers.gamepredicates;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.gamepredicates.LastFreeSpinPredicate;

import java.util.Map;
import java.util.function.Function;

@SuppressWarnings("unused")
public class LastFreeSpinPredicateParser implements GenericDataParser<LastFreeSpinPredicate> {

    @Override
    public LastFreeSpinPredicate parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new LastFreeSpinPredicate();
    }
}
