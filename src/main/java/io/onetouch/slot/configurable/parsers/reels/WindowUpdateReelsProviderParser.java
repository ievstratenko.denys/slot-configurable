package io.onetouch.slot.configurable.parsers.reels;

import io.onetouch.core.rng.WeightEntry;
import io.onetouch.slot.api.SlotPosition;
import io.onetouch.slot.api.SlotSymbolType;
import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.configurable.parsing.SlotConfigParserUtils;
import io.onetouch.slot.core.ReelsProvider;
import io.onetouch.slot.core.dataproviders.one.ConstantOne;
import io.onetouch.slot.core.dataproviders.one.One;
import io.onetouch.slot.core.reels.providers.WindowUpdateReelsProvider;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.external;

@SuppressWarnings("unused")
public class WindowUpdateReelsProviderParser implements GenericDataParser<WindowUpdateReelsProvider> {

    @SuppressWarnings("unchecked")
    @Override
    public WindowUpdateReelsProvider parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        Object symbolWeights = external(element, "symbolWeights", dataSupplier).orElseThrow();
        One<?> symbolWeightsProvider = symbolWeights instanceof One ? (One<?>) symbolWeights : new ConstantOne<>(symbolWeights);
        One<Function<SlotPosition, List<WeightEntry<SlotSymbolType>>>> positionAgnosticProvider = game -> {
            var weightEntries = (List<WeightEntry<SlotSymbolType>>) symbolWeightsProvider.get(game);
            return (__ -> weightEntries);
        };
        return new WindowUpdateReelsProvider(
                (ReelsProvider) external(element, "delegate", dataSupplier).orElseThrow(),
                positionAgnosticProvider,
                SlotConfigParserUtils.<SlotPosition>many(element, "exceptPositions", dataSupplier).orElseThrow()
        );
    }
}
