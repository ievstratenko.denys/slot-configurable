package io.onetouch.slot.configurable.parsers.reels;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.reels.BaseReelsImpl;
import io.onetouch.slot.core.reels.ReelSet;
import io.onetouch.slot.core.reels.SceneSize;
import io.onetouch.slot.core.reels.providers.reelstops.ReelStopsProvider;

import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.external;

@SuppressWarnings("unused")
public class BaseReelsProviderParser implements GenericDataParser<BaseReelsImpl> {

    @Override
    public BaseReelsImpl parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        BaseReelsImpl result = new BaseReelsImpl(
                (SceneSize) external(element, "sceneSize", dataSupplier).orElseThrow(),
                (ReelSet) external(element, "reelSet", dataSupplier).orElseThrow()
        );
        external(element, "reelStopsProvider", dataSupplier)
                .map(o -> (ReelStopsProvider) o)
                .ifPresent(result::reelStopsProvider);
        return result;
    }
}
