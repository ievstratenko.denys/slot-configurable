package io.onetouch.slot.configurable.parsers.calculators;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.SlotGameManager;
import io.onetouch.slot.core.SlotSceneCalculator;
import io.onetouch.slot.core.calculators.generic.ConditionalCalculator;

import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.external;

@SuppressWarnings("unused")
public class ConditionalCalculatorParser implements GenericDataParser<ConditionalCalculator> {

    @SuppressWarnings("unchecked")
    @Override
    public ConditionalCalculator parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new ConditionalCalculator(
                (Predicate<SlotGameManager>) external(element, "gamePredicate", dataSupplier).orElseThrow(),
                (SlotSceneCalculator) external(element, "delegate", dataSupplier).orElseThrow()
        ).otherwiseDelegate((SlotSceneCalculator) external(element, "otherwiseDelegate", dataSupplier).orElse(null));
    }
}
