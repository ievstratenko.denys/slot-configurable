package io.onetouch.slot.configurable.parsers.dataproviders.one;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.dataproviders.one.ConstantOne;

import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.object;

@SuppressWarnings("unused")
public class ConstantOneParser implements GenericDataParser<ConstantOne<?>> {

    @Override
    public ConstantOne<?> parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new ConstantOne<>(object(element, "value").orElseThrow());
    }
}
