package io.onetouch.slot.configurable.parsers.gamepredicates.generic;

import io.onetouch.slot.configurable.parsers.common.ListOfReferencesParser;
import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.SlotGameManager;
import io.onetouch.slot.core.gamepredicates.generic.AllMatchPredicate;

import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
public class AllMatchPredicateParser implements GenericDataParser<AllMatchPredicate> {

    private final ListOfReferencesParser listOfReferencesParser = new ListOfReferencesParser();

    @SuppressWarnings("unchecked")
    @Override
    public AllMatchPredicate parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new AllMatchPredicate(
                listOfReferencesParser.parse(element, "delegates", dataSupplier, Predicate.class).stream()
                        .map(v -> (Predicate<SlotGameManager>) v)
                        .collect(Collectors.toList())
        );
    }
}
