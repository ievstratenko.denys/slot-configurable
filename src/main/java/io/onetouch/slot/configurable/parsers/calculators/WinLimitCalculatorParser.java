package io.onetouch.slot.configurable.parsers.calculators;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.configurable.parsing.SlotConfigParserUtils;
import io.onetouch.slot.core.calculators.WinLimitCalculator;

import java.math.BigDecimal;
import java.util.Map;
import java.util.function.Function;

@SuppressWarnings("unused")
public class WinLimitCalculatorParser implements GenericDataParser<WinLimitCalculator> {

    @Override
    public WinLimitCalculator parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new WinLimitCalculator(
                SlotConfigParserUtils.<BigDecimal>one(element, "limitInBets", dataSupplier).orElse(null)
        );
    }
}
