package io.onetouch.slot.configurable.parsers.reels.reelstops;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.configurable.parsing.SlotConfigParserUtils;
import io.onetouch.slot.core.reels.providers.reelstops.FromManyReelStopsProvider;

import java.util.Map;
import java.util.function.Function;

@SuppressWarnings("unused")
public class FromManyReelStopsProviderParser implements GenericDataParser<FromManyReelStopsProvider> {

    @Override
    public FromManyReelStopsProvider parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new FromManyReelStopsProvider(
                SlotConfigParserUtils.<Integer>many(element, "values", dataSupplier).orElseThrow()
        );
    }
}
