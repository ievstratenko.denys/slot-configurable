package io.onetouch.slot.configurable.parsers.symbols;

import io.onetouch.slot.api.SlotSymbolType;
import io.onetouch.slot.configurable.parsing.GenericDataParser;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.list;

@SuppressWarnings("unused")
public class SymbolsParser implements GenericDataParser<List<SlotSymbolType>> {

    @Override
    public List<SlotSymbolType> parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return list(element).orElseThrow()
                .stream()
                .map(o -> (String) o)
                .map(SlotSymbolType::valueOf)
                .collect(Collectors.toList());
    }
}
