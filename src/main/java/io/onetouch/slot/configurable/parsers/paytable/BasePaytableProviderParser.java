package io.onetouch.slot.configurable.parsers.paytable;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.BasePaytableProvider;
import io.onetouch.slot.core.Paytable;

import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.external;

@SuppressWarnings("unused")
public class BasePaytableProviderParser implements GenericDataParser<BasePaytableProvider> {

    @Override
    public BasePaytableProvider parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new BasePaytableProvider((Paytable) external(element, "paytable", dataSupplier).orElseThrow());
    }
}
