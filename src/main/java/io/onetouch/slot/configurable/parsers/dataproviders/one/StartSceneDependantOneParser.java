package io.onetouch.slot.configurable.parsers.dataproviders.one;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.dataproviders.one.StartSceneDependantOne;

import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.external;

@SuppressWarnings("unused")
public class StartSceneDependantOneParser implements GenericDataParser<StartSceneDependantOne<?>> {

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    public StartSceneDependantOne<?> parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new StartSceneDependantOne((Map<String, ?>) external(element, "mapBySceneIds", dataSupplier).orElseThrow());
    }
}
