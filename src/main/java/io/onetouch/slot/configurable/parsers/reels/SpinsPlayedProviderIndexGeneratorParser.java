package io.onetouch.slot.configurable.parsers.reels;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.reels.providers.SpinsPlayedProviderIndexGenerator;

import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.external;
import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.integer;

@SuppressWarnings("unused")
public class SpinsPlayedProviderIndexGeneratorParser implements GenericDataParser<SpinsPlayedProviderIndexGenerator> {

    @SuppressWarnings("unchecked")
    @Override
    public SpinsPlayedProviderIndexGenerator parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new SpinsPlayedProviderIndexGenerator(
                (Map<Integer, Integer>) external(element, "spinsPlayedToProviderIndex", dataSupplier).orElseThrow(),
                integer(element, "defaultProviderIndex").orElseThrow()
        );
    }
}
