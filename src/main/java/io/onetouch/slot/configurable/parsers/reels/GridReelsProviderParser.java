package io.onetouch.slot.configurable.parsers.reels;

import io.onetouch.core.rng.WeightEntry;
import io.onetouch.slot.api.SlotPosition;
import io.onetouch.slot.api.SlotSymbolType;
import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.dataproviders.one.ConstantOne;
import io.onetouch.slot.core.dataproviders.one.One;
import io.onetouch.slot.core.reels.providers.GridReelsProvider;
import io.onetouch.slot.core.reels.SceneSize;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.external;

@SuppressWarnings("unused")
public class GridReelsProviderParser implements GenericDataParser<GridReelsProvider> {

    @SuppressWarnings("unchecked")
    @Override
    public GridReelsProvider parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        Object symbolWeights = external(element, "symbolWeights", dataSupplier).orElseThrow();
        One<?> symbolWeightsProvider = symbolWeights instanceof One ? (One<?>) symbolWeights : new ConstantOne<>(symbolWeights);
        One<Function<SlotPosition, List<WeightEntry<SlotSymbolType>>>> positionAgnosticProvider = game -> {
            var weightEntries = (List<WeightEntry<SlotSymbolType>>) symbolWeightsProvider.get(game);
            return (__ -> weightEntries);
        };
        return new GridReelsProvider(
                (SceneSize) external(element, "sceneSize", dataSupplier).orElseThrow(),
                positionAgnosticProvider
        );
    }
}
