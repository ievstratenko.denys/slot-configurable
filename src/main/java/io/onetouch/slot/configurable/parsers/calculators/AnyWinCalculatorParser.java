package io.onetouch.slot.configurable.parsers.calculators;

import io.onetouch.slot.configurable.parsers.calculators.settings.AnyPositionCombinationSupplierParser;
import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.calculators.WinCalculator;
import io.onetouch.slot.core.calculators.winCalculator.CombinationSupplier;
import io.onetouch.slot.core.calculators.winCalculator.WinSupplier;
import io.onetouch.slot.core.calculators.winCalculator.winSuppliers.LineWinSupplier;

import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.external;

@SuppressWarnings("unused")
public class AnyWinCalculatorParser implements GenericDataParser<WinCalculator> {

    private final AnyPositionCombinationSupplierParser combinationSupplierParser = new AnyPositionCombinationSupplierParser();

    @Override
    public WinCalculator parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new WinCalculator(
                (CombinationSupplier) external(element, "combinationSupplier", dataSupplier)
                        .orElseGet(() -> combinationSupplierParser.parse(element, dataSupplier)),
                (WinSupplier) external(element, "winSupplier", dataSupplier).orElse(new LineWinSupplier())
        );
    }
}
