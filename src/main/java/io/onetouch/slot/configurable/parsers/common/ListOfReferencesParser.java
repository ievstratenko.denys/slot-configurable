package io.onetouch.slot.configurable.parsers.common;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.configurable.parsing.SlotConfigParserUtils;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.list;

@SuppressWarnings("unused")
public class ListOfReferencesParser implements GenericDataParser<List<Object>> {

    @Override
    public List<Object> parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return parse(element, SlotConfigParserUtils.LIST_VALUE_KEY, dataSupplier);
    }

    public List<Object> parse(Map<String, Object> element, String propertyName, Function<String, Object> dataSupplier) {
        return parse(element, propertyName, dataSupplier, Object.class);
    }

    public <T> List<T> parse(Map<String, Object> element, Function<String, Object> dataSupplier, Class<T> tClass) {
        return parse(element, SlotConfigParserUtils.LIST_VALUE_KEY, dataSupplier, tClass);
    }

    @SuppressWarnings("unchecked")
    public <T> List<T> parse(Map<String, Object> element, String propertyName, Function<String, Object> dataSupplier,
                             Class<T> tClass) {
        return list(element, propertyName)
                .orElseThrow()
                .stream()
                .map(o -> (String) o)
                .map(key -> key.replace(SlotConfigParserUtils.EXTERNAL_VALUE_PREFIX, ""))
                .map(key -> find(dataSupplier, key))
                .map(e -> (T) e)
                .collect(Collectors.toList());
    }

    private Object find(Function<String, Object> dataSupplier, String key) {
        Object result = dataSupplier.apply(key);
        if (result == null) {
            throw new NullPointerException("Element not found " + key);
        } else {
            return result;
        }
    }
}
