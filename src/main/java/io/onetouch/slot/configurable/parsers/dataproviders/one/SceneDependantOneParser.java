package io.onetouch.slot.configurable.parsers.dataproviders.one;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.dataproviders.one.SceneDependantOne;

import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.external;

@SuppressWarnings("unused")
public class SceneDependantOneParser implements GenericDataParser<SceneDependantOne<?>> {

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    public SceneDependantOne<?> parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new SceneDependantOne((Map<String, ?>) external(element, "mapBySceneIds", dataSupplier).orElseThrow());
    }
}
