package io.onetouch.slot.configurable.parsers.gamepredicates;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.gamepredicates.FreeSpinsRemainedPredicate;

import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.integer;

@SuppressWarnings("unused")
public class FreeSpinsRemainedPredicateParser implements GenericDataParser<FreeSpinsRemainedPredicate> {

    @Override
    public FreeSpinsRemainedPredicate parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new FreeSpinsRemainedPredicate(parseAllowed(element));
    }

    private Set<Integer> parseAllowed(Map<String, Object> element) {
        return Set.of(integer(element, "allowed").orElseThrow());
    }
}
