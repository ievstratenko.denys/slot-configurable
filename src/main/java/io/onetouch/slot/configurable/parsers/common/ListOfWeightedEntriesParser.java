package io.onetouch.slot.configurable.parsers.common;

import io.onetouch.core.rng.RngUtils;
import io.onetouch.core.rng.WeightEntry;
import io.onetouch.slot.configurable.parsing.GenericDataParser;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.*;

@SuppressWarnings("unused")
public class ListOfWeightedEntriesParser implements GenericDataParser<List<WeightEntry<?>>> {

    @Override
    public List<WeightEntry<?>> parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return list(element, WeightEntry.class)
                .orElseGet(() -> zip(element, dataSupplier))
                .stream()
                .map(we -> (WeightEntry<?>) we)
                .collect(Collectors.toList());
    }

    @SuppressWarnings("rawtypes")
    private List<WeightEntry> zip(Map<String, Object> element, Function<String, Object> dataSupplier) {
        List<Integer> weights = list(element, "weights", Integer.class).orElseThrow();
        
        UnaryOperator<Object> convertor = string(element, "convertor")
                .map(ConvertorType::valueOf)
                .map(ConvertorType::getConvertor)
                .orElse(UnaryOperator.identity());
        
        List<Object> values = list(element, "values").orElseThrow().stream()
                .map(o -> selfOrExternal(o, dataSupplier))
                .map(convertor)
                .collect(Collectors.toList());

        if (weights.size() != values.size())
            throw new IllegalArgumentException("weights and values have different sizes");

        return IntStream.range(0, weights.size())
                .mapToObj(i -> RngUtils.weightEntry(weights.get(i), values.get(i)))
                .collect(Collectors.toList());
    }
}
