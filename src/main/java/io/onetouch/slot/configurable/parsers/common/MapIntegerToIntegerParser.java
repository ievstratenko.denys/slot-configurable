package io.onetouch.slot.configurable.parsers.common;

import io.onetouch.slot.configurable.parsing.GenericDataParser;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@SuppressWarnings("unused")
public class MapIntegerToIntegerParser implements GenericDataParser<Map<Integer, Integer>> {
    
    private static final String RANGE_BOUNDS_DELIMITER = "..";
    private static final String LIST_ELEMENTS_DELIMITER = ",";

    @Override
    public Map<Integer, Integer> parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return element.entrySet().stream()
                .flatMap(this::processEntry)
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (u, v) -> {
                            throw new IllegalArgumentException("Duplicates found: " + u);
                        },
                        LinkedHashMap::new
                ));
    }

    private Stream<Map.Entry<Integer, Integer>> processEntry(Map.Entry<String, Object> entry) {
        String key = entry.getKey();
        Integer value = (Integer) entry.getValue();
        if (key.contains(RANGE_BOUNDS_DELIMITER)) {
            return processRangeEntry(key, value);
        }
        if (key.contains(LIST_ELEMENTS_DELIMITER)) {
            return processListEntry(key, value);
        }
        return processSingleEntry(key, value);
    }

    private Stream<Map.Entry<Integer, Integer>> processRangeEntry(String key, Integer value) {
        String[] bounds = key.split(Pattern.quote(RANGE_BOUNDS_DELIMITER));
        int min, max;
        try {
            if (bounds.length != 2) throw new IllegalArgumentException();
            min = Integer.parseInt(bounds[0]);
            max = Integer.parseInt(bounds[1]);
        } catch (Exception e) {
            throw new IllegalArgumentException("Wrong range format for key " + key + ". Example '3..7'");
        }
        return IntStream.rangeClosed(min, max)
                .mapToObj(k -> new AbstractMap.SimpleEntry<>(k, value));
    }

    private Stream<Map.Entry<Integer, Integer>> processListEntry(String key, Integer value) {
        String[] elements = key.split(Pattern.quote(LIST_ELEMENTS_DELIMITER));
        try {
            return Arrays.stream(elements)
                    .map(Integer::parseInt)
                    .map(k -> new AbstractMap.SimpleEntry<>(k, value));
        } catch (Exception e) {
            throw new IllegalArgumentException("Wrong range format for key " + key + ". Example '3,7,11'");
        }
    }

    private Stream<Map.Entry<Integer, Integer>> processSingleEntry(String key, Integer value) {
        try {
            return Stream.of(new AbstractMap.SimpleEntry<>(Integer.parseInt(key), value));
        } catch (Exception e) {
            throw new IllegalArgumentException("Wrong range format for key " + key + ". Example '3'");
        }
    }
}
