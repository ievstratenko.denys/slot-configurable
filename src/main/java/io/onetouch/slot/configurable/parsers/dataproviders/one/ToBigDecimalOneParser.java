package io.onetouch.slot.configurable.parsers.dataproviders.one;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.configurable.parsing.SlotConfigParserUtils;
import io.onetouch.slot.core.dataproviders.one.ToBigDecimalOne;

import java.util.Map;
import java.util.function.Function;

@SuppressWarnings("unused")
public class ToBigDecimalOneParser implements GenericDataParser<ToBigDecimalOne> {

    @Override
    public ToBigDecimalOne parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new ToBigDecimalOne(
                SlotConfigParserUtils.one(element, "value", dataSupplier).orElseThrow()
        );
    }
}
