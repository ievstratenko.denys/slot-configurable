package io.onetouch.slot.configurable.parsers.reels;

import io.onetouch.slot.api.SlotPosition;
import io.onetouch.slot.api.SlotSymbolType;
import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.configurable.parsing.SlotConfigParserUtils;
import io.onetouch.slot.core.ReelsProvider;
import io.onetouch.slot.core.reels.providers.SymbolLandingReelsProvider;

import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.external;
import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.string;

@SuppressWarnings("unused")
public class SymbolLandingReelsProviderParser implements GenericDataParser<SymbolLandingReelsProvider> {

    @Override
    public SymbolLandingReelsProvider parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new SymbolLandingReelsProvider(
                (ReelsProvider) external(element, "delegate", dataSupplier).orElseThrow(),
                string(element, "symbolType").map(SlotSymbolType::valueOf).orElseThrow(),
                SlotConfigParserUtils.<SlotPosition>many(element, "positions", dataSupplier).orElseThrow()
        );
    }
}
