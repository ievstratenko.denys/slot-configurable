package io.onetouch.slot.configurable.parsers.gamepredicates;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.configurable.parsing.SlotConfigParserUtils;
import io.onetouch.slot.core.common.HasTotalWin;
import io.onetouch.slot.core.gamepredicates.HasWinPredicate;

import java.util.Map;
import java.util.function.Function;

@SuppressWarnings("unused")
public class HasWinPredicateParser implements GenericDataParser<HasWinPredicate> {

    @Override
    public HasWinPredicate parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new HasWinPredicate(
                SlotConfigParserUtils.<HasTotalWin>one(element, "provider", dataSupplier).orElse(game -> game)
        );
    }
}
