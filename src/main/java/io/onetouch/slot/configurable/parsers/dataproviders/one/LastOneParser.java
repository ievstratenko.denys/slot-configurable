package io.onetouch.slot.configurable.parsers.dataproviders.one;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.configurable.parsing.SlotConfigParserUtils;
import io.onetouch.slot.core.dataproviders.one.LastOne;

import java.util.Map;
import java.util.function.Function;

@SuppressWarnings("unused")
public class LastOneParser implements GenericDataParser<LastOne<?>> {

    @Override
    public LastOne<?> parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new LastOne<>(SlotConfigParserUtils.many(element, "provider", dataSupplier).orElseThrow());
    }
}
