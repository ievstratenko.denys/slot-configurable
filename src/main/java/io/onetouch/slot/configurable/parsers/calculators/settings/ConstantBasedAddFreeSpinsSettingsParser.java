package io.onetouch.slot.configurable.parsers.calculators.settings;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.configurable.parsing.SlotConfigParserUtils;
import io.onetouch.slot.core.calculators.addFreeSpins.ConstantBasedAddFreeSpinsSettingsSupplier;

import java.util.Map;
import java.util.function.Function;

@SuppressWarnings("unused")
public class ConstantBasedAddFreeSpinsSettingsParser implements GenericDataParser<ConstantBasedAddFreeSpinsSettingsSupplier> {

    @Override
    public ConstantBasedAddFreeSpinsSettingsSupplier parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new ConstantBasedAddFreeSpinsSettingsSupplier(
                SlotConfigParserUtils.<Integer>one(element, "count", dataSupplier).orElseThrow()
        );
    }
}
