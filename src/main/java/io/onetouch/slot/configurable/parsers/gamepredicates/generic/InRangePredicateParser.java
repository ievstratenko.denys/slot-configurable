package io.onetouch.slot.configurable.parsers.gamepredicates.generic;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.configurable.parsing.SlotConfigParserUtils;
import io.onetouch.slot.core.gamepredicates.generic.InRangePredicate;

import java.util.Map;
import java.util.function.Function;

@SuppressWarnings("unused")
public class InRangePredicateParser implements GenericDataParser<InRangePredicate> {

    @Override
    public InRangePredicate parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new InRangePredicate(
                SlotConfigParserUtils.<Integer>one(element, "value", dataSupplier).orElseThrow(),
                SlotConfigParserUtils.<Integer>one(element, "min", dataSupplier).orElse(null),
                SlotConfigParserUtils.<Integer>one(element, "max", dataSupplier).orElse(null)
        );
    }
}
