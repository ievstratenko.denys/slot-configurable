package io.onetouch.slot.configurable.parsers.calculators.miniscene;

import io.onetouch.slot.api.SlotWindow;
import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.configurable.parsing.SlotConfigParserUtils;
import io.onetouch.slot.core.ReelsProvider;
import io.onetouch.slot.core.SlotGameManager;
import io.onetouch.slot.core.SlotSceneCalculator;
import io.onetouch.slot.core.calculators.generic.DoNothingCalculator;
import io.onetouch.slot.core.calculators.miniscene.MiniSceneDelegateCalculator;
import io.onetouch.slot.core.dataproviders.one.One;

import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.external;

@SuppressWarnings("unused")
public class MiniSceneDelegateCalculatorParser implements GenericDataParser<MiniSceneDelegateCalculator> {

    @Override
    public MiniSceneDelegateCalculator parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new MiniSceneDelegateCalculator(
                external(element, "reelsProvider", dataSupplier)
                        .map(o -> (ReelsProvider) o)
                        .map(rp -> (One<SlotWindow>) rp::spinReels)
                        .orElse(SlotGameManager::getWindow),
                external(element, "delegate", dataSupplier)
                        .map(o -> (SlotSceneCalculator) o)
                        .orElse(new DoNothingCalculator())
        ).eventIdProvider(SlotConfigParserUtils.<String>one(element, "eventId", dataSupplier).orElse(null));
    }
}
