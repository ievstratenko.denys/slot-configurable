package io.onetouch.slot.configurable.parsers.dataproviders.one;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.configurable.parsing.SlotConfigParserUtils;
import io.onetouch.slot.core.dataproviders.one.FirstOne;

import java.util.Map;
import java.util.function.Function;

@SuppressWarnings("unused")
public class FirstOneParser implements GenericDataParser<FirstOne<?>> {

    @Override
    public FirstOne<?> parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new FirstOne<>(SlotConfigParserUtils.many(element, "provider", dataSupplier).orElseThrow());
    }
}
