package io.onetouch.slot.configurable.parsers.dataproviders.many;

import io.onetouch.slot.api.SlotSymbolType;
import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.configurable.parsing.SlotConfigParserUtils;
import io.onetouch.slot.core.common.HasWindow;
import io.onetouch.slot.core.dataproviders.many.PositionsOnWindowMany;

import java.util.Map;
import java.util.function.Function;

@SuppressWarnings("unused")
public class PositionsOnWindowManyParser implements GenericDataParser<PositionsOnWindowMany> {

    @Override
    public PositionsOnWindowMany parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new PositionsOnWindowMany(
                SlotConfigParserUtils.<SlotSymbolType>many(element, "symbolTypes", dataSupplier).orElseThrow(),
                SlotConfigParserUtils.<HasWindow>one(element, "provider", dataSupplier).orElseThrow()
        );
    }
}
