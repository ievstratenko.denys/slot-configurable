package io.onetouch.slot.configurable.parsers.calculators.generateEvent;

import io.onetouch.slot.api.SlotPosition;
import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.configurable.parsing.SlotConfigParserUtils;
import io.onetouch.slot.core.calculators.generateEvent.GeneratePositionsEventCalculator;

import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.bool;

@SuppressWarnings("unused")
public class GeneratePositionsEventCalculatorParser implements GenericDataParser<GeneratePositionsEventCalculator> {

    @Override
    public GeneratePositionsEventCalculator parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new GeneratePositionsEventCalculator(
                SlotConfigParserUtils.<SlotPosition>many(element, "positions", dataSupplier).orElseThrow()
        )
                .eventIdProvider(SlotConfigParserUtils.<String>one(element, "eventId", dataSupplier).orElse(null))
                .generateIfEmpty(bool(element, "generateIfEmpty").orElse(false));
    }
}
