package io.onetouch.slot.configurable.parsers.common;

import io.onetouch.slot.configurable.parsing.GenericDataParser;

import java.util.AbstractMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.selfOrExternal;

/**
 * Originally it was called like ..ToReference but now it works with both references and simple values
 */
@SuppressWarnings("unused")
public class MapStringToReferenceParser implements GenericDataParser<Map<String, Object>> {

    @Override
    public Map<String, Object> parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return element.entrySet().stream()
                .map(e -> new AbstractMap.SimpleEntry<>(e.getKey(), selfOrExternal(e.getValue(), dataSupplier)))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (u, v) -> {
                            throw new IllegalArgumentException("Duplicates found: " + u);
                        },
                        LinkedHashMap::new
                ));
    }
}
