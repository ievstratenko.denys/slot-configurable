package io.onetouch.slot.configurable.parsers.calculators.settings;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.configurable.parsing.SlotConfigParserUtils;
import io.onetouch.slot.core.calculators.startSceneCalculator.CombinationStartSceneSettingsSupplier;
import io.onetouch.slot.core.calculators.winCalculator.CombinationSupplier;

import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.external;

@SuppressWarnings("unused")
public class CombinationStartSceneSettingsParser implements GenericDataParser<CombinationStartSceneSettingsSupplier> {

    @Override
    public CombinationStartSceneSettingsSupplier parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new CombinationStartSceneSettingsSupplier(
                SlotConfigParserUtils.<Map<Integer, Integer>>one(element, "spinsLengthMapping", dataSupplier).orElseThrow(),
                (CombinationSupplier) external(element, "combinationSupplier", dataSupplier).orElseThrow()
        );
    }
}
