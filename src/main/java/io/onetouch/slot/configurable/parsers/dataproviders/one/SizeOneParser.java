package io.onetouch.slot.configurable.parsers.dataproviders.one;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.configurable.parsing.SlotConfigParserUtils;
import io.onetouch.slot.core.dataproviders.one.SizeOne;

import java.util.Map;
import java.util.function.Function;

@SuppressWarnings("unused")
public class SizeOneParser implements GenericDataParser<SizeOne> {

    @Override
    public SizeOne parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new SizeOne(SlotConfigParserUtils.many(element, "provider", dataSupplier).orElseThrow());
    }
}
