package io.onetouch.slot.configurable.parsers.scenesize;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.reels.SceneSize;

import java.util.Collections;
import java.util.Map;
import java.util.function.Function;

@SuppressWarnings("unused")
public class SceneSizeRectangularParser implements GenericDataParser<SceneSize> {

    @Override
    public SceneSize parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        Integer width = (Integer) element.get("width");
        Integer height = (Integer) element.get("height");
        return new SceneSize(Collections.nCopies(width, height));
    }
}
