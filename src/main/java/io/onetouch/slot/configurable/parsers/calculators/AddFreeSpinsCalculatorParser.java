package io.onetouch.slot.configurable.parsers.calculators;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.calculators.AddFreeSpinsCalculator;
import io.onetouch.slot.core.calculators.addFreeSpins.AddFreeSpinsSettingsSupplier;

import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.external;
import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.integer;

@SuppressWarnings("unused")
public class AddFreeSpinsCalculatorParser implements GenericDataParser<AddFreeSpinsCalculator> {

    @Override
    public AddFreeSpinsCalculator parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new AddFreeSpinsCalculator(
                (AddFreeSpinsSettingsSupplier) external(element, "settings", dataSupplier).orElseThrow(),
                integer(element, "maxSpins").orElse(null)
        );
    }
}
