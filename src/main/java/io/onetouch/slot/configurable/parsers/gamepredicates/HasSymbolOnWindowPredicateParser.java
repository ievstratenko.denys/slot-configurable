package io.onetouch.slot.configurable.parsers.gamepredicates;

import io.onetouch.slot.api.SlotSymbolType;
import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.gamepredicates.HasSymbolOnWindowPredicate;

import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.string;

@SuppressWarnings("unused")
public class HasSymbolOnWindowPredicateParser implements GenericDataParser<HasSymbolOnWindowPredicate> {

    @Override
    public HasSymbolOnWindowPredicate parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        String symbol = string(element, "symbol").orElseThrow();
        return new HasSymbolOnWindowPredicate(
                SlotSymbolType.valueOf(symbol)
        );
    }
}
