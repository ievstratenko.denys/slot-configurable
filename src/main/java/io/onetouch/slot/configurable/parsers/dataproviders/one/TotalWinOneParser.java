package io.onetouch.slot.configurable.parsers.dataproviders.one;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.configurable.parsing.SlotConfigParserUtils;
import io.onetouch.slot.core.common.HasTotalWin;
import io.onetouch.slot.core.dataproviders.one.TotalWinOne;

import java.util.Map;
import java.util.function.Function;

@SuppressWarnings("unused")
public class TotalWinOneParser implements GenericDataParser<TotalWinOne> {

    @Override
    public TotalWinOne parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new TotalWinOne(
                SlotConfigParserUtils.<HasTotalWin>one(element, "provider", dataSupplier).orElseThrow()
        );
    }
}
