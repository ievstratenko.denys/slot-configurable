package io.onetouch.slot.configurable.parsers.dataproviders.many;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.configurable.parsing.SlotConfigParserUtils;
import io.onetouch.slot.core.dataproviders.many.EventsMany;

import java.util.Map;
import java.util.function.Function;

@SuppressWarnings("unused")
public class EventsManyParser implements GenericDataParser<EventsMany> {

    @Override
    public EventsMany parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new EventsMany(SlotConfigParserUtils.<String>many(element, "eventIds", dataSupplier).orElseThrow());
    }
}
