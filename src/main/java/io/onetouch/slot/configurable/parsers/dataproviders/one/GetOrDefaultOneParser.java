package io.onetouch.slot.configurable.parsers.dataproviders.one;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.configurable.parsing.SlotConfigParserUtils;
import io.onetouch.slot.core.dataproviders.one.GetOrDefaultOne;

import java.util.Map;
import java.util.function.Function;

@SuppressWarnings("unused")
public class GetOrDefaultOneParser implements GenericDataParser<GetOrDefaultOne<?>> {

    @Override
    public GetOrDefaultOne<?> parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new GetOrDefaultOne<>(
                SlotConfigParserUtils.one(element, "delegate", dataSupplier).orElseThrow(),
                SlotConfigParserUtils.one(element, "defaultValue", dataSupplier).orElseThrow()
        );
    }
}
