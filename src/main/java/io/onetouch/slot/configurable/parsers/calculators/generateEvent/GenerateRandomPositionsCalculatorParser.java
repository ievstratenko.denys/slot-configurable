package io.onetouch.slot.configurable.parsers.calculators.generateEvent;

import io.onetouch.core.rng.WeightEntry;
import io.onetouch.slot.api.SlotPosition;
import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.configurable.parsing.SlotConfigParserUtils;
import io.onetouch.slot.core.calculators.generateEvent.GenerateRandomPositionsCalculator;
import io.onetouch.slot.core.dataproviders.one.ConstantOne;
import io.onetouch.slot.core.dataproviders.one.One;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.bool;
import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.external;

@SuppressWarnings("unused")
public class GenerateRandomPositionsCalculatorParser implements GenericDataParser<GenerateRandomPositionsCalculator> {

    @SuppressWarnings("unchecked")
    @Override
    public GenerateRandomPositionsCalculator parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        Object symbolCountWeights = external(element, "symbolCountWeights", dataSupplier).orElseThrow();
        One<List<WeightEntry<Integer>>> symbolCountWeightsProvider = symbolCountWeights instanceof One
                ? (One<List<WeightEntry<Integer>>>) symbolCountWeights
                : new ConstantOne<>((List<WeightEntry<Integer>>) symbolCountWeights);
        return new GenerateRandomPositionsCalculator(symbolCountWeightsProvider)
                .eventIdProvider(SlotConfigParserUtils.<String>one(element, "eventId", dataSupplier).orElse(null))
                .avoidPositionsProvider(SlotConfigParserUtils.<SlotPosition>many(element, "avoidPositions", dataSupplier).orElse(null))
                .generateIfEmpty(bool(element, "generateIfEmpty").orElse(false));
    }
}
