package io.onetouch.slot.configurable.parsers.calculators.winsuppliers;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.SlotGameManager;

import java.math.BigDecimal;
import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.integer;

@SuppressWarnings("unused")
public class ConstantWinMultiplierParser implements GenericDataParser<Function<SlotGameManager, BigDecimal>> {

    @Override
    public Function<SlotGameManager, BigDecimal> parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        BigDecimal multiplier = new BigDecimal(integer(element).orElseThrow());
        return __ -> multiplier;
    }
}
