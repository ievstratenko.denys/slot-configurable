package io.onetouch.slot.configurable.parsers.calculators.settings;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.calculators.winCalculator.CombinationSettings;
import io.onetouch.slot.core.calculators.winCalculator.combinationSuppliers.AnyPositionCombinationSupplier;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.external;

@SuppressWarnings("unused")
public class AnyPositionCombinationSupplierParser implements GenericDataParser<AnyPositionCombinationSupplier> {

    @SuppressWarnings("unchecked")
    @Override
    public AnyPositionCombinationSupplier parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new AnyPositionCombinationSupplier(
                (List<CombinationSettings>) external(element, "combinationSettings", dataSupplier).orElseThrow()
        );
    }
}
