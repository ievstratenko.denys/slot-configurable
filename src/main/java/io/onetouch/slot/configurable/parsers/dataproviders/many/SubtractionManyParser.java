package io.onetouch.slot.configurable.parsers.dataproviders.many;

import io.onetouch.slot.configurable.parsers.common.ListOfReferencesParser;
import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.dataproviders.many.Many;
import io.onetouch.slot.core.dataproviders.many.SubtractionMany;

import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.many;

@SuppressWarnings("unused")
public class SubtractionManyParser implements GenericDataParser<SubtractionMany<?>> {

    private final ListOfReferencesParser listOfReferencesParser = new ListOfReferencesParser();

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    public SubtractionMany<?> parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new SubtractionMany(
                many(element, "source", dataSupplier).orElseThrow(),
                listOfReferencesParser.parse(element, "subtractors", dataSupplier, Many.class)
        );
    }
}
