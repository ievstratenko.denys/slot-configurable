package io.onetouch.slot.configurable.parsers.behavior;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.SceneBehavior;
import io.onetouch.slot.core.scene.MainSceneBehavior;

import java.util.Map;
import java.util.function.Function;

@SuppressWarnings("unused")
public class MainSceneBehaviorParser implements GenericDataParser<SceneBehavior> {

    @Override
    public SceneBehavior parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new MainSceneBehavior();
    }
}
