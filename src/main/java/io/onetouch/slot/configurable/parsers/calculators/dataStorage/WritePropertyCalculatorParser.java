package io.onetouch.slot.configurable.parsers.calculators.dataStorage;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.calculators.dataStorage.WritePropertyCalculator;
import io.onetouch.slot.core.dataproviders.DataScope;

import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.one;
import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.string;

@SuppressWarnings("unused")
public class WritePropertyCalculatorParser implements GenericDataParser<WritePropertyCalculator> {

    @Override
    public WritePropertyCalculator parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new WritePropertyCalculator(
                string(element, "scope").map(DataScope::valueOf).orElse(DataScope.ROUND),
                string(element, "key").orElseThrow(),
                one(element, "value", dataSupplier).orElseThrow()
        );
    }
}
