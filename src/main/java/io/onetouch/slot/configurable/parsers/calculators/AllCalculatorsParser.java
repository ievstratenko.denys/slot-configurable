package io.onetouch.slot.configurable.parsers.calculators;

import io.onetouch.slot.configurable.parsers.common.ListOfReferencesParser;
import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.SlotSceneCalculator;
import io.onetouch.slot.core.calculators.generic.AllCalculators;

import java.util.Map;
import java.util.function.Function;

@SuppressWarnings("unused")
public class AllCalculatorsParser implements GenericDataParser<AllCalculators> {

    private final ListOfReferencesParser listOfReferencesParser = new ListOfReferencesParser();

    @Override
    public AllCalculators parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new AllCalculators(
                listOfReferencesParser.parse(element, "delegates", dataSupplier, SlotSceneCalculator.class)
        );
    }
}
