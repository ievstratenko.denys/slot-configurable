package io.onetouch.slot.configurable.parsers.dataproviders.one;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.configurable.parsing.SlotConfigParserUtils;
import io.onetouch.slot.core.dataproviders.one.SumIntegersOne;

import java.util.Map;
import java.util.function.Function;

@SuppressWarnings("unused")
public class SumIntegersOneParser implements GenericDataParser<SumIntegersOne> {

    @Override
    public SumIntegersOne parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new SumIntegersOne(
                SlotConfigParserUtils.<Integer>ones(element, "values", dataSupplier).orElseThrow()
        );
    }
}
