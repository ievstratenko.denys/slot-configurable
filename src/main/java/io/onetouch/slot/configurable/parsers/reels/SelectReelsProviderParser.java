package io.onetouch.slot.configurable.parsers.reels;

import io.onetouch.slot.configurable.parsers.common.ListOfReferencesParser;
import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.configurable.parsing.SlotConfigParserUtils;
import io.onetouch.slot.core.ReelsProvider;
import io.onetouch.slot.core.reels.providers.SelectReelsProvider;

import java.util.Map;
import java.util.function.Function;

@SuppressWarnings("unused")
public class SelectReelsProviderParser implements GenericDataParser<SelectReelsProvider> {

    private final ListOfReferencesParser listOfReferencesParser = new ListOfReferencesParser();

    @Override
    public SelectReelsProvider parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new SelectReelsProvider(
                listOfReferencesParser.parse(element, dataSupplier, ReelsProvider.class),
                SlotConfigParserUtils.<Integer>one(element, "weightsByIndex", dataSupplier).orElseThrow()
        );
    }
}
