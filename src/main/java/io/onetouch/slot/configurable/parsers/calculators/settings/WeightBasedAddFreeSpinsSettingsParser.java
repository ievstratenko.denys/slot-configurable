package io.onetouch.slot.configurable.parsers.calculators.settings;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.calculators.addFreeSpins.WeightBasedAddFreeSpinsSettingsSupplier;
import io.onetouch.slot.core.dataproviders.one.One;

import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.external;

@SuppressWarnings("unused")
public class WeightBasedAddFreeSpinsSettingsParser implements GenericDataParser<WeightBasedAddFreeSpinsSettingsSupplier> {

    @SuppressWarnings("unchecked")
    @Override
    public WeightBasedAddFreeSpinsSettingsSupplier parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        Object mapping = external(element, "countToWeightMapping", dataSupplier).orElseThrow();
        if (mapping instanceof Map) return 
                new WeightBasedAddFreeSpinsSettingsSupplier((Map<Integer, Integer>) mapping);
        if (mapping instanceof One) return 
                new WeightBasedAddFreeSpinsSettingsSupplier((One<Map<Integer, Integer>>) mapping);
        throw new UnsupportedOperationException("Unsupported type of " + mapping);
    }
}
