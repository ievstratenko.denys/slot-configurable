package io.onetouch.slot.configurable.parsers.gamepredicates;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.gamepredicates.HasEventPredicate;

import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.string;

@SuppressWarnings("unused")
public class HasEventPredicateParser implements GenericDataParser<HasEventPredicate> {

    @Override
    public HasEventPredicate parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new HasEventPredicate(string(element, "eventId").orElseThrow());
    }
}
