package io.onetouch.slot.configurable.parsers.calculators;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.SlotGameManager;
import io.onetouch.slot.core.SlotSceneCalculator;
import io.onetouch.slot.core.calculators.generic.WhileCalculator;

import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.external;

@SuppressWarnings("unused")
public class WhileCalculatorParser implements GenericDataParser<WhileCalculator> {

    @SuppressWarnings("unchecked")
    @Override
    public WhileCalculator parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new WhileCalculator(
                (Predicate<SlotGameManager>) external(element, "gamePredicate", dataSupplier).orElseThrow(),
                (SlotSceneCalculator) external(element, "delegate", dataSupplier).orElseThrow()
        );
    }
}
