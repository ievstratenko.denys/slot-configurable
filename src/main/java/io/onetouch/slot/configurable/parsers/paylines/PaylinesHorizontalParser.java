package io.onetouch.slot.configurable.parsers.paylines;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.reels.BaseLine;
import io.onetouch.slot.core.utils.SlotUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.list;

@SuppressWarnings("unused")
public class PaylinesHorizontalParser implements GenericDataParser<List<BaseLine>> {

    @Override
    public List<BaseLine> parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        List<Object> lines = list(element).orElseThrow();
        List<BaseLine> result = new ArrayList<>();
        for (int i = 0; i < lines.size(); i++) {
            int[] slotIndexes = ((List<?>) lines.get(i)).stream()
                    .mapToInt(e -> (Integer) e)
                    .toArray();
            result.add(SlotUtils.horizontalLine(i + 1, slotIndexes));
        }
        return result;
    }
}
