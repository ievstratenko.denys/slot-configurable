package io.onetouch.slot.configurable.parsers.calculators.miniscene;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.configurable.parsing.SlotConfigParserUtils;
import io.onetouch.slot.core.calculators.miniscene.MiniSceneAddRewardsCalculator;

import java.util.Map;
import java.util.function.Function;

@SuppressWarnings("unused")
public class MiniSceneGroupRewardsCalculatorParser implements GenericDataParser<MiniSceneAddRewardsCalculator> {

    @Override
    public MiniSceneAddRewardsCalculator parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new MiniSceneAddRewardsCalculator(
                SlotConfigParserUtils.<Integer>one(element, "winMultiplier", dataSupplier).orElse(null)
        );
    }
}
