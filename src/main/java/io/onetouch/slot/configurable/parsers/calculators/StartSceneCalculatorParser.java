package io.onetouch.slot.configurable.parsers.calculators;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.calculators.StartSceneCalculator;
import io.onetouch.slot.core.calculators.startSceneCalculator.StartSceneSettingsSupplier;
import io.onetouch.slot.core.dataproviders.one.ConstantOne;
import io.onetouch.slot.core.dataproviders.one.One;

import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.external;
import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.string;

@SuppressWarnings("unused")
public class StartSceneCalculatorParser implements GenericDataParser<StartSceneCalculator> {

    @SuppressWarnings("unchecked")
    @Override
    public StartSceneCalculator parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        One<String> sceneIdProvider = string(element, "sceneId")
                .map(id -> (One<String>) new ConstantOne<>(id))
                .orElseGet(() -> (One<String>) external(element, "sceneIdProvider", dataSupplier).orElseThrow());
        return new StartSceneCalculator(
                sceneIdProvider,
                (StartSceneSettingsSupplier) external(element, "settings", dataSupplier).orElseThrow()
        );
    }
}
