package io.onetouch.slot.configurable.parsers.reels.reelstops;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.reels.providers.reelstops.UniqueInSceneReelStopsProvider;

import java.util.Map;
import java.util.function.Function;

@SuppressWarnings("unused")
public class UniqueInSceneReelStopsProviderParser implements GenericDataParser<UniqueInSceneReelStopsProvider> {

    @Override
    public UniqueInSceneReelStopsProvider parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new UniqueInSceneReelStopsProvider();
    }
}
