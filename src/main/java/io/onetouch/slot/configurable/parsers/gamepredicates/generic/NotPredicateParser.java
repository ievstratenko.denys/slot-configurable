package io.onetouch.slot.configurable.parsers.gamepredicates.generic;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.SlotGameManager;
import io.onetouch.slot.core.gamepredicates.generic.NotPredicate;

import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.external;

@SuppressWarnings("unused")
public class NotPredicateParser implements GenericDataParser<NotPredicate> {

    @SuppressWarnings("unchecked")
    @Override
    public NotPredicate parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new NotPredicate(
                (Predicate<SlotGameManager>) external(element, "delegate", dataSupplier).orElseThrow()
        );
    }
}
