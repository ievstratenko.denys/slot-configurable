package io.onetouch.slot.configurable.parsers.paytable;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.Paytable;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.*;

@SuppressWarnings("unused")
public class PaytableParser implements GenericDataParser<Paytable> {

    @Override
    public Paytable parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        String id = string(element, "id").orElseThrow();
        Map<String, List<Integer>> paytable = map(element, "paytable").orElseThrow()
                .entrySet().stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey, 
                        e -> parsePaytableRecord(e.getValue(), dataSupplier),
                        (u, v) -> v, // impossible to have duplicates here as the source is a map
                        LinkedHashMap::new
                ));
        return new Paytable(id, paytable);
    }

    @SuppressWarnings("unchecked")
    private List<Integer> parsePaytableRecord(Object record, Function<String, Object> dataSupplier) {
        if (record instanceof List<?>) {
            return (List<Integer>) record;
        }
        if (record instanceof String) {
            return external((String) record, dataSupplier)
                    .map(o -> (List<Integer>) o)
                    .orElseThrow();
        }
        throw new UnsupportedOperationException("Unsupported paytable record type: " + record.getClass() + " for: " + record);
    }
}
