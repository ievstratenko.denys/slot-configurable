package io.onetouch.slot.configurable.parsers.dataproviders.many;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.configurable.parsing.SlotConfigParserUtils;
import io.onetouch.slot.core.common.HasRewards;
import io.onetouch.slot.core.dataproviders.many.RewardsMany;

import java.util.Map;
import java.util.function.Function;

@SuppressWarnings("unused")
public class RewardsManyParser implements GenericDataParser<RewardsMany> {

    @Override
    public RewardsMany parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new RewardsMany(
                SlotConfigParserUtils.<HasRewards>one(element, "provider", dataSupplier).orElseThrow()
        );
    }
}
