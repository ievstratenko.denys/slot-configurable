package io.onetouch.slot.configurable.parsers.dataproviders.one;

import io.onetouch.core.rng.WeightEntry;
import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.dataproviders.one.WeightedOne;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.external;

@SuppressWarnings("unused")
public class WeightedOneParser implements GenericDataParser<WeightedOne<?>> {

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    public WeightedOne<?> parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new WeightedOne((List<WeightEntry<?>>) external(element, "weightEntries", dataSupplier).orElseThrow());
    }
}
