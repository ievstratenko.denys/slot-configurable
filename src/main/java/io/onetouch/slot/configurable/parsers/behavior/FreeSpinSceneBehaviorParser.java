package io.onetouch.slot.configurable.parsers.behavior;

import io.onetouch.slot.api.SlotsGameState;
import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.SceneBehavior;
import io.onetouch.slot.core.scene.FreeSpinSceneBehavior;

import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.string;

@SuppressWarnings("unused")
public class FreeSpinSceneBehaviorParser implements GenericDataParser<SceneBehavior> {

    @Override
    public SceneBehavior parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        SlotsGameState gameState = string(element, "gameState")
                .map(SlotsGameState::valueOf)
                .orElse(SlotsGameState.FREE_GAME);
        return new FreeSpinSceneBehavior(gameState);
    }
}
