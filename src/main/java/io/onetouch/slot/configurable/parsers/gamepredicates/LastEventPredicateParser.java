package io.onetouch.slot.configurable.parsers.gamepredicates;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.gamepredicates.LastEventPredicate;

import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.string;

@SuppressWarnings("unused")
public class LastEventPredicateParser implements GenericDataParser<LastEventPredicate> {

    @Override
    public LastEventPredicate parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new LastEventPredicate(string(element, "eventId").orElseThrow());
    }
}
