package io.onetouch.slot.configurable.parsers.dataproviders.many;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.configurable.parsing.SlotConfigParserUtils;
import io.onetouch.slot.core.common.HasRewards;
import io.onetouch.slot.core.dataproviders.many.WinPositionsMany;

import java.util.Map;
import java.util.function.Function;

@SuppressWarnings("unused")
public class WinPositionsManyParser implements GenericDataParser<WinPositionsMany> {

    @Override
    public WinPositionsMany parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new WinPositionsMany(
                SlotConfigParserUtils.<HasRewards>one(element, "provider", dataSupplier).orElseThrow()
        );
    }
}
