package io.onetouch.slot.configurable.parsers.dataproviders.many;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.configurable.parsing.SlotConfigParserUtils;
import io.onetouch.slot.core.common.HasPositions;
import io.onetouch.slot.core.dataproviders.many.PositionsMany;

import java.util.Map;
import java.util.function.Function;

@SuppressWarnings("unused")
public class PositionsManyParser implements GenericDataParser<PositionsMany> {

    @Override
    public PositionsMany parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new PositionsMany(
                SlotConfigParserUtils.<HasPositions>one(element, "provider", dataSupplier).orElseThrow()
        );
    }
}
