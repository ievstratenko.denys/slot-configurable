package io.onetouch.slot.configurable.parsers.calculators.settings;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.calculators.winCalculator.CombinationSettings;
import io.onetouch.slot.core.calculators.winCalculator.combinationSuppliers.OrthogonalCombinationSupplier;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.external;
import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.integer;

@SuppressWarnings("unused")
public class OrthogonalCombinationSupplierParser implements GenericDataParser<OrthogonalCombinationSupplier> {

    @SuppressWarnings("unchecked")
    @Override
    public OrthogonalCombinationSupplier parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        OrthogonalCombinationSupplier result = new OrthogonalCombinationSupplier(
                (List<CombinationSettings>) external(element, "combinationSettings", dataSupplier).orElseThrow(),
                integer(element, "minLength").orElseThrow()
        );
        external(element, "linesCombinationSettings", dataSupplier)
                .map(o -> (List<CombinationSettings>) o)
                .ifPresent(result::setLinesCombinationSettings);
        return result;
    }
}
