package io.onetouch.slot.configurable.parsers.calculators;

import io.onetouch.slot.configurable.parsers.calculators.settings.LineCombinationSupplierParser;
import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.calculators.WinCalculator;
import io.onetouch.slot.core.calculators.winCalculator.CombinationSupplier;
import io.onetouch.slot.core.calculators.winCalculator.WinSupplier;
import io.onetouch.slot.core.calculators.winCalculator.winSuppliers.LineWinSupplier;

import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.external;

@SuppressWarnings("unused")
public class WinCalculatorParser implements GenericDataParser<WinCalculator> {

    private final LineCombinationSupplierParser combinationSupplierParser = new LineCombinationSupplierParser();

    @Override
    public WinCalculator parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new WinCalculator(
                (CombinationSupplier) external(element, "combinationSupplier", dataSupplier)
                        .orElseGet(() -> combinationSupplierParser.parse(element, dataSupplier)),
                (WinSupplier) external(element, "winSupplier", dataSupplier).orElse(new LineWinSupplier())
        );
    }
}
