package io.onetouch.slot.configurable.parsers.events;

import io.onetouch.slot.api.SlotEvent;
import io.onetouch.slot.configurable.parsing.GenericDataParser;

import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.string;

public class SlotEventParser implements GenericDataParser<SlotEvent> {

    @Override
    public SlotEvent parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new SlotEvent(string(element, "id").orElseThrow());
    }
}
