package io.onetouch.slot.configurable.parsers.dataproviders.one;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.dataproviders.DataScope;
import io.onetouch.slot.core.dataproviders.one.PropertyOne;

import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.string;

@SuppressWarnings("unused")
public class PropertyOneParser implements GenericDataParser<PropertyOne> {

    @Override
    public PropertyOne parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new PropertyOne(
                string(element, "scope").map(DataScope::valueOf).orElse(DataScope.ROUND),
                string(element, "key").orElseThrow()
        );
    }
}
