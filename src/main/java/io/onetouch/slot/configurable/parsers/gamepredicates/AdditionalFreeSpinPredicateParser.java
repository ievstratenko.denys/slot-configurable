package io.onetouch.slot.configurable.parsers.gamepredicates;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.gamepredicates.AdditionalFreeSpinPredicate;

import java.util.Map;
import java.util.function.Function;

@SuppressWarnings("unused")
public class AdditionalFreeSpinPredicateParser implements GenericDataParser<AdditionalFreeSpinPredicate> {

    @Override
    public AdditionalFreeSpinPredicate parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new AdditionalFreeSpinPredicate();
    }
}
