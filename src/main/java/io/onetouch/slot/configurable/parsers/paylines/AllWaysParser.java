package io.onetouch.slot.configurable.parsers.paylines;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.reels.BaseLine;
import io.onetouch.slot.core.utils.SlotUtils;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.integer;

@SuppressWarnings("unused")
public class AllWaysParser implements GenericDataParser<List<BaseLine>> {

    @Override
    public List<BaseLine> parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return SlotUtils.allWays(
                integer(element, "height").orElseThrow(), 
                integer(element, "width").orElseThrow()
        );
    }
}
