package io.onetouch.slot.configurable.parsers.dataproviders.one;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.dataproviders.one.WeightedByIndexOne;

import java.util.Map;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.list;

@SuppressWarnings("unused")
public class WeightedByIndexOneParser implements GenericDataParser<WeightedByIndexOne> {

    @Override
    public WeightedByIndexOne parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new WeightedByIndexOne(list(element, Integer.class).orElseThrow());
    }
}
