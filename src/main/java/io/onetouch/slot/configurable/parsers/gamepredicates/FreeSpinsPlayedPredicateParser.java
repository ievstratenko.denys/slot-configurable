package io.onetouch.slot.configurable.parsers.gamepredicates;

import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.gamepredicates.FreeSpinsPlayedPredicate;

import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.integer;

@SuppressWarnings("unused")
public class FreeSpinsPlayedPredicateParser implements GenericDataParser<FreeSpinsPlayedPredicate> {

    @Override
    public FreeSpinsPlayedPredicate parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        return new FreeSpinsPlayedPredicate(parseAllowed(element));
    }

    private Set<Integer> parseAllowed(Map<String, Object> element) {
        return Set.of(integer(element, "allowed").orElseThrow());
    }
}
