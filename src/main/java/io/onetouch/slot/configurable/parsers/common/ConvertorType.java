package io.onetouch.slot.configurable.parsers.common;

import io.onetouch.slot.api.SlotSymbolType;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.function.UnaryOperator;

@Getter
@RequiredArgsConstructor
public enum ConvertorType {
    
    SYMBOL_TYPE(o -> SlotSymbolType.valueOf((String) o));
    
    private final UnaryOperator<Object> convertor;
}
