package io.onetouch.slot.configurable.parsers.calculators.settings;

import io.onetouch.slot.api.SlotSymbolType;
import io.onetouch.slot.configurable.parsing.GenericDataParser;
import io.onetouch.slot.core.calculators.winCalculator.CombinationMatcher;
import io.onetouch.slot.core.calculators.winCalculator.CombinationSettings;
import io.onetouch.slot.core.calculators.winCalculator.combinationsMatchers.LineSubstitutionSymbolMatcher;
import io.onetouch.slot.core.calculators.winCalculator.combinationsMatchers.SingleSymbolMatcher;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import static io.onetouch.slot.configurable.parsing.SlotConfigParserUtils.*;

@SuppressWarnings("unused")
public class CombinationSettingsParser implements GenericDataParser<List<CombinationSettings>> {

    @SuppressWarnings("unchecked")
    @Override
    public List<CombinationSettings> parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        List<SlotSymbolType> symbols = symbols(element, "symbols")
                .orElseGet(() -> (List<SlotSymbolType>) external(element, "symbolsRef", dataSupplier).orElseThrow());

        List<SlotSymbolType> except = symbols(element, "except").orElse(List.of());
        Optional<List<SlotSymbolType>> subBy = symbols(element, "substitutesBy");
        List<SlotSymbolType> subExcept = symbols(element, "substitutesExcept").orElse(List.of());
        boolean allSubstitutesAllowed = bool(element, "allSubstitutesAllowed").orElse(true);

        Function<SlotSymbolType, CombinationMatcher> combinationMatcherGenerator =
                s -> subBy.isEmpty() || subExcept.contains(s)
                        ? new SingleSymbolMatcher(s)
                        : new LineSubstitutionSymbolMatcher(s, subBy.get()).allSubstitutesAllowed(allSubstitutesAllowed);

        return symbols.stream()
                .filter(s -> !except.contains(s))
                .map(s -> new CombinationSettings(s.name(), combinationMatcherGenerator.apply(s)))
                .collect(Collectors.toList());
    }
}
