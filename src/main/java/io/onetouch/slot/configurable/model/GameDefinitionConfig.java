package io.onetouch.slot.configurable.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.Map;

@Data
public class GameDefinitionConfig {
    private String id;
    private boolean defaultDefinition;
    private Map<String, Map<String, Object>> data;
    @JsonIgnore
    private Map<String, Object> parsedData;
}
