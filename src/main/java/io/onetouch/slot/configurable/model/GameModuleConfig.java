package io.onetouch.slot.configurable.model;

import io.onetouch.core.common.type.GameType;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class GameModuleConfig {
    private GameType type;
    private List<GameDefinitionConfig> definitions;
    private Map<String, Map<String, Object>> data;
}
