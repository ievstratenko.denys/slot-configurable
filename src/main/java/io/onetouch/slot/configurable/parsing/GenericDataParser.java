package io.onetouch.slot.configurable.parsing;

import java.util.Map;
import java.util.function.Function;

public interface GenericDataParser<T> {

    T parse(Map<String, Object> element, Function<String, Object> dataSupplier);
}
