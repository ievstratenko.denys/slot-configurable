package io.onetouch.slot.configurable.parsing;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.reflections.Reflections;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
public class GenericDataParserUtils {

    private static final String PARSERS_BASE_PACKAGE = "io.onetouch.slot.configurable.parsers";

    private static final Map<String, GenericDataParser<?>> parsers = new Reflections(PARSERS_BASE_PACKAGE)
            .getSubTypesOf(GenericDataParser.class)
            .stream()
            .map(GenericDataParserUtils::createInstance)
            .collect(Collectors.toMap(p -> p.getClass().getSimpleName(), Function.identity()));

    @SuppressWarnings("rawtypes")
    @SneakyThrows
    private static GenericDataParser<?> createInstance(Class<? extends GenericDataParser> c) {
        return c.getConstructor().newInstance();
    }

    static Object parse(Map<String, Object> element, Function<String, Object> dataSupplier) {
        Map<String, Object> coreData = new LinkedHashMap<>(element);
        try {
            String parser = (String) coreData.remove("parser");
            return Optional.ofNullable(parsers.get(parser))
                    .orElseThrow(() -> new IllegalStateException("No such parser " + parser))
                    .parse(coreData, dataSupplier);
        } catch (Exception e) {
            throw new IllegalArgumentException("Parsing Error of " + element, e);
        }
    }
}
