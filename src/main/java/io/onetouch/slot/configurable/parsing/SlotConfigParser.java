package io.onetouch.slot.configurable.parsing;

import io.onetouch.core.common.type.GameType;
import io.onetouch.slot.configurable.ConfigurableGameDefinition;
import io.onetouch.slot.configurable.ConfigurableGameModule;
import io.onetouch.slot.configurable.model.GameDefinitionConfig;
import io.onetouch.slot.configurable.model.GameModuleConfig;
import io.onetouch.slot.core.SlotGameDefinition;
import io.onetouch.slot.core.reels.BaseLine;
import io.onetouch.slot.core.scene.SlotScene;

import java.util.*;
import java.util.stream.Collectors;

public class SlotConfigParser {

    public static ConfigurableGameModule parse(GameType gameType) {
        GameModuleConfig game = SlotConfigParserUtils.read(gameType);
        validateGame(game);

        Set<SlotGameDefinition> gameDefinitions = game.getDefinitions().stream()
                .peek(v -> combineData(game.getData(), v))
                .peek(SlotConfigParser::parseData)
                .sorted((v1, v2) -> Boolean.compare(v2.isDefaultDefinition(), v1.isDefaultDefinition())) // to put default GD ahead of all
                .map(SlotConfigParser::parseGameDefinition)
                .collect(Collectors.toCollection(LinkedHashSet::new));

        return new ConfigurableGameModule(game.getType(), gameDefinitions, gameDefinitions.iterator().next());
    }

    private static void validateGame(GameModuleConfig game) {
        long countOfDefaults = game.getDefinitions().stream().filter(GameDefinitionConfig::isDefaultDefinition).count();
        if (countOfDefaults != 1)
            throw new IllegalStateException("Must be ONE default game definition. Found " + countOfDefaults);
    }

    private static void combineData(Map<String, Map<String, Object>> commonData, GameDefinitionConfig gd) {
        Map<String, Map<String, Object>> combined = new LinkedHashMap<>(commonData);
        combined.putAll(gd.getData());
        gd.setData(combined);
    }

    private static void parseData(GameDefinitionConfig gd) {
        Map<String, Object> parsedData = new LinkedHashMap<>();
        gd.getData().forEach((k, v) -> parsedData.put(k, GenericDataParserUtils.parse(v, parsedData::get)));
        gd.setParsedData(parsedData);
    }

    @SuppressWarnings("unchecked")
    private static ConfigurableGameDefinition parseGameDefinition(GameDefinitionConfig gd) {
        return ConfigurableGameDefinition.builder()
                .id(gd.getId())
                .paylines((List<BaseLine>) gd.getParsedData().get("paylines"))
                .scenes((List<SlotScene>) gd.getParsedData().get("scenes"))
                .build();
    }
}
