package io.onetouch.slot.configurable.parsing;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.onetouch.core.common.type.GameType;
import io.onetouch.slot.api.SlotSymbolType;
import io.onetouch.slot.configurable.model.GameModuleConfig;
import io.onetouch.slot.core.dataproviders.many.ConstantMany;
import io.onetouch.slot.core.dataproviders.many.Many;
import io.onetouch.slot.core.dataproviders.one.ConstantOne;
import io.onetouch.slot.core.dataproviders.one.One;
import io.onetouch.slot.core.utils.SlotUtils;
import lombok.SneakyThrows;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@SuppressWarnings("unchecked")
public class SlotConfigParserUtils {

    private static final String BASE_CONFIG_FOLDER_PATH = "/slot_configs/";
    private static final String CONFIG_FILES_EXTENSION = ".json";
    private static final ObjectMapper mapper = new ObjectMapper();

    private final static String SINGLE_VALUE_KEY = "value";
    public final static String LIST_VALUE_KEY = "list";
    public final static String EXTERNAL_VALUE_PREFIX = "#";

    @SneakyThrows
    static GameModuleConfig read(GameType gameType) {
        String config = readConfigContent(gameType);
        return mapper.readValue(config, GameModuleConfig.class);
    }

    @SneakyThrows
    private static String readConfigContent(GameType desertTreasure) {
        var pathStr = BASE_CONFIG_FOLDER_PATH + desertTreasure.name() + CONFIG_FILES_EXTENSION;

        InputStream inputStream = SlotUtils.class.getResourceAsStream(pathStr);
        if (inputStream == null) throw new NoSuchConfigException();

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        return reader.lines().collect(Collectors.joining());
    }

    public static class NoSuchConfigException extends RuntimeException {
    }

    public static Optional<Object> object(Map<String, Object> data, String key) {
        return Optional.ofNullable(data).map(map -> map.get(key));
    }

    public static Optional<Integer> integer(Map<String, Object> data) {
        return integer(data, SINGLE_VALUE_KEY);
    }

    public static Optional<Integer> integer(Map<String, Object> data, String key) {
        return object(data, key).map(o -> (Integer) o);
    }

    public static Optional<Boolean> bool(Map<String, Object> data) {
        return bool(data, SINGLE_VALUE_KEY);
    }

    public static Optional<Boolean> bool(Map<String, Object> data, String key) {
        return object(data, key).map(o -> (Boolean) o);
    }

    public static Optional<BigDecimal> bigDecimal(Map<String, Object> data, String key) {
        return object(data, key).map(o -> new BigDecimal(o.toString()));
    }

    public static Optional<String> string(Map<String, Object> data, String key) {
        return object(data, key).map(o -> (String) o);
    }

    public static Optional<Map<String, Object>> map(Map<String, Object> data, String key) {
        return object(data, key).map(o -> (Map<String, Object>) o);
    }

    public static Optional<List<SlotSymbolType>> symbols(Map<String, Object> data, String key) {
        return list(data, key)
                .map(list -> list.stream()
                        .map(o -> (String) o)
                        .map(SlotSymbolType::valueOf)
                        .collect(Collectors.toList()));
    }

    public static Optional<List<Object>> list(Map<String, Object> data) {
        return list(data, LIST_VALUE_KEY);
    }

    public static Optional<List<Object>> list(Map<String, Object> data, String key) {
        return object(data, key).map(o -> (List<Object>) o);
    }

    public static <T> Optional<List<T>> list(Map<String, Object> data, Class<T> tClass) {
        return list(data, LIST_VALUE_KEY, tClass);
    }

    @SuppressWarnings("unused")
    public static <T> Optional<List<T>> list(Map<String, Object> data, String key, Class<T> tClass) {
        return object(data, key).map(o -> (List<T>) o);
    }

    public static Optional<Object> external(Map<String, Object> data, String key, Function<String, Object> dataSupplier) {
        return object(data, key)
                .map(o -> (String) o)
                .flatMap(ref -> external(ref, dataSupplier));
    }

    public static Optional<Object> external(String ref, Function<String, Object> dataSupplier) {
        return Optional.of(ref)
                .map(externalValueKey -> externalValueKey.replace(EXTERNAL_VALUE_PREFIX, ""))
                .map(dataSupplier);
    }

    public static Object selfOrExternal(Object o, Function<String, Object> dataSupplier) {
        if (o instanceof String) {
            String s = (String) o;
            if (s.startsWith(EXTERNAL_VALUE_PREFIX)) return external(s, dataSupplier).orElseThrow();
        }
        return o;
    }

    public static Optional<Object> selfOrExternal(Map<String, Object> data, String key, Function<String, Object> dataSupplier) {
        return object(data, key).map(o -> selfOrExternal(o, dataSupplier));
    }

    public static <T> Optional<One<T>> one(Map<String, Object> data, String key, Function<String, Object> dataSupplier) {
        return selfOrExternal(data, key, dataSupplier)
                .map(o -> o instanceof One ? (One<T>) o : new ConstantOne<>((T) o));
    }

    public static <T> Optional<Many<T>> many(Map<String, Object> data, String key, Function<String, Object> dataSupplier) {
        return selfOrExternal(data, key, dataSupplier)
                .map(o -> o instanceof Many ? (Many<T>) o : new ConstantMany<>((Collection<T>) o));
    }

    public static <T> Optional<List<One<T>>> ones(Map<String, Object> data, String key, Function<String, Object> dataSupplier) {
        return object(data, key)
                .map(o -> (List<Object>) o)
                .map(list -> list.stream()
                        .map(o -> (T) selfOrExternal(o, dataSupplier))
                        .map(o -> o instanceof One ? (One<T>) o : new ConstantOne<>(o))
                        .collect(Collectors.toList()));
    }
}
