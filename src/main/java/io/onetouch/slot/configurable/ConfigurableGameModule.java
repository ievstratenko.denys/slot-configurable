package io.onetouch.slot.configurable;

import io.onetouch.core.common.type.GameType;
import io.onetouch.slot.core.AbstractSlotGameModule;
import io.onetouch.slot.core.SlotGameDefinition;
import lombok.*;

import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConfigurableGameModule extends AbstractSlotGameModule {

    private GameType type;
    private Set<SlotGameDefinition> gameDefinitions;
    private SlotGameDefinition defaultGameDefinition;
}
