package io.onetouch.slot.configurable;

import io.onetouch.GameModule;
import io.onetouch.core.common.type.GameType;
import io.onetouch.slot.configurable.parsing.SlotConfigParser;
import io.onetouch.slot.configurable.parsing.SlotConfigParserUtils;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class ConfigurableGameModuleProvider {

    public static final ConfigurableGameModuleProvider INSTANCE = new ConfigurableGameModuleProvider();

    public Map<GameType, GameModule> getGameModules() {
        return Arrays.stream(GameType.values())
                .map(this::find)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toMap(GameModule::getType, Function.identity()));
    }

    private Optional<GameModule> find(GameType type) {
        try {
            return Optional.of(SlotConfigParser.parse(type));
        } catch (SlotConfigParserUtils.NoSuchConfigException e) {
            return Optional.empty();
        }
    }
}
