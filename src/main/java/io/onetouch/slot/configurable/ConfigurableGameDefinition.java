package io.onetouch.slot.configurable;

import io.onetouch.slot.core.SlotGameDefinition;
import io.onetouch.slot.core.reels.BaseLine;
import io.onetouch.slot.core.scene.SlotScene;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class ConfigurableGameDefinition implements SlotGameDefinition {

    private String id;
    private List<BaseLine> paylines;
    private List<SlotScene> scenes;
}
