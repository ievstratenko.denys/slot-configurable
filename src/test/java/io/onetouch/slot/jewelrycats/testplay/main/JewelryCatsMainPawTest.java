package io.onetouch.slot.jewelrycats.testplay.main;

import io.onetouch.GameState;
import io.onetouch.slot.core.ResultAction;
import io.onetouch.slot.core.TestUtil;
import io.onetouch.slot.jewelrycats.testplay.JewelryCatsAbstractPlayTest;
import lombok.extern.slf4j.Slf4j;
import org.hamcrest.Matcher;
import org.junit.Test;

import java.util.List;

import static io.onetouch.slot.core.matchers.RewardMatcher.lineReward;
import static io.onetouch.slot.jewelrycats.testplay.utils.JewelryCatsCheat.cheat;
import static java.util.List.of;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;

@Slf4j
public class JewelryCatsMainPawTest extends JewelryCatsAbstractPlayTest {

    @Override
    protected ResultAction<GameState> play(TestUtil.Cheat cheat) {
        return super.play(cheat)
                .andExpect("$.events[0].id", equalTo("pawSeed"))
                .andExpect("$.events[1].id", equalTo("initialWindow"));
    }

    @Test
    public void testPawsOutsideWinLines() {
        Matcher<List<List<String>>> windowMatcher = equalTo(of(
                of("A", "B", "C", "D", "E"),
                of("F", "G", "G", "G", "F"),
                of("A", "B", "C", "D", "E"),
                of("F", "G", "F", "G", "F"),
                of("A", "B", "C", "D", "E")));
        play(cheat()
                .line(0, 1, 2, 3, 4)
                .line(5, 6, 6, 6, 5)
                .line(0, 1, 2, 3, 4)
                .line(5, 6, 5, 6, 5)
                .line(0, 1, 2, 3, 4)
                .pawCount(3)
                .paw(0, 0).paw(4, 1).paw(3, 2))

                .andExpect("$.totalWin", equalTo(1))
                .andExpect("$.window.reels", windowMatcher)

                .andExpect("$.rewards", hasSize(1))
                .andExpect("$.events", hasSize(2))

                .andExpect("$.events[0].positions", equalTo(of(of(0, 0), of(4, 1), of(3, 2))))

                .andExpect("$.events[1].totalWin", equalTo(1))
                .andExpect("$.events[1].reelsView.reels", windowMatcher)
                .andExpect("$.events[1].rewards", hasSize(1))
                .andExpect("$.events[1].rewards[0]", lineReward(1).payId("G").positions(of(of(1, 1), of(2, 1), of(3, 1))));
    }

    @Test
    public void testShortFlow() {
        int win = 4, wilds = 2;
        Matcher<List<List<String>>> initialWindowMatcher = equalTo(of(
                of("A", "B", "C", "D", "E"),
                of("F", "G", "G", "G", "F"),
                of("A", "B", "C", "D", "E"),
                of("F", "G", "F", "G", "F"),
                of("A", "B", "C", "D", "E")));
        play(cheat()
                .line(0, 1, 2, 3, 4)
                .line(5, 6, 6, 6, 5)
                .line(0, 1, 2, 3, 4)
                .line(5, 6, 5, 6, 5)
                .line(0, 1, 2, 3, 4)
                .pawCount(3)
                .paw(0, 0).paw(3, 1).paw(3, 2)
                // 1st respin
                .pawTable(0).pawCount(1)
                .paw(4, 2)
                .symbolTable(0)
                .line(0, 1, 2,/**/4)
                .line(5,/* **  **/5)
                .line(0, 1, 2,/**/4)
                .line(5, 6, 5,/**/5)
                .line(0, 1, 2, 3, 4))

                .andExpect("$.totalWin", equalTo(win * wilds))
                .andExpect("$.window.reels", initialWindowMatcher)

                .andExpect("$.rewards", hasSize(3))
                .andExpect("$.events", hasSize(8))

                .andExpect("$.events[0].positions", equalTo(of(of(0, 0), of(3, 1), of(3, 2))))

                .andExpect("$.events[1].totalWin", equalTo(1))
                .andExpect("$.events[1].reelsView.reels", initialWindowMatcher)
                .andExpect("$.events[1].rewards", hasSize(1))
                .andExpect("$.events[1].rewards[0]", lineReward(1).payId("G").lineId(91131).positions(of(of(1, 1), of(2, 1), of(3, 1))))

                .andExpect("$.events[2].id", equalTo("newWilds"))
                .andExpect("$.events[2].positions", equalTo(of(of(3, 1))))

                .andExpect("$.events[3].id", equalTo("updatedWindow"))
                .andExpect("$.events[3].totalWin", equalTo(3))
                .andExpect("$.events[3].reelsView.reels", equalTo(of(
                        of("A", "B", "C", "D", "E"),
                        of("F", "G", "G", "WL", "F"),
                        of("A", "B", "C", "D", "E"),
                        of("F", "G", "F", "G", "F"),
                        of("A", "B", "C", "D", "E"))))
                .andExpect("$.events[3].rewards", hasSize(2))
                .andExpect("$.events[3].rewards[0]", lineReward(1).payId("G").lineId(91131).positions(of(of(1, 1), of(2, 1), of(3, 1))))
                .andExpect("$.events[3].rewards[1]", lineReward(2).payId("D").lineId(93032).positions(of(of(3, 0), of(3, 1), of(3, 2))))

                .andExpect("$.events[4].id", equalTo("newWilds"))
                .andExpect("$.events[4].positions", equalTo(of(of(3, 2))))

                .andExpect("$.events[5].id", equalTo("updatedWindow"))
                .andExpect("$.events[5].totalWin", equalTo(4))
                .andExpect("$.events[5].reelsView.reels", equalTo(of(
                        of("A", "B", "C", "D", "E"),
                        of("F", "G", "G", "WL", "F"),
                        of("A", "B", "C", "WL", "E"),
                        of("F", "G", "F", "G", "F"),
                        of("A", "B", "C", "D", "E"))))
                .andExpect("$.events[5].rewards", hasSize(3))
                .andExpect("$.events[5].rewards[0]", lineReward(1).payId("G").lineId(91131).positions(of(of(1, 1), of(2, 1), of(3, 1))))
                .andExpect("$.events[5].rewards[1]", lineReward(1).payId("G").lineId(93133).positions(of(of(3, 1), of(3, 2), of(3, 3))))
                .andExpect("$.events[5].rewards[2]", lineReward(2).payId("D").lineId(93032).positions(of(of(3, 0), of(3, 1), of(3, 2))));
    }

    @Test
    public void testWildLineWin() {
        int win = 600, wilds = 9;
        Matcher<List<List<String>>> initialWindowMatcher = equalTo(of(
                of("A", "B", "C", "D", "E"),
                of("B", "G", "C", "E", "B"),
                of("A", "A", "A", "A", "A"),
                of("C", "E", "C", "G", "C"),
                of("A", "B", "C", "D", "E")));
        play(cheat()
                .line(0, 1, 2, 3, 4)
                .line(1, 6, 2, 4, 1)
                .line(0, 0, 0, 0, 0)
                .line(2, 4, 2, 6, 2)
                .line(0, 1, 2, 3, 4)
                .pawCount(9)
                .paw(0, 2).paw(1, 2).paw(2, 2).paw(3, 2).paw(4, 2)
                .paw(2, 0).paw(2, 1).paw(2, 3).paw(2, 4))
                .andExpect("$.totalWin", equalTo(win * wilds))
                .andExpect("$.window.reels", initialWindowMatcher)

                .andExpect("$.rewards", hasSize(2))
                .andExpect("$.events", hasSize(6))

                .andExpect("$.events[0].positions", equalTo(of(
                        of(0, 2), of(1, 2), of(2, 2), of(3, 2), of(4, 2),
                        of(2, 0), of(2, 1), of(2, 3), of(2, 4)
                )))

                .andExpect("$.events[1].id", equalTo("initialWindow"))
                .andExpect("$.events[1].totalWin", equalTo(100))
                .andExpect("$.events[1].reelsView.reels", initialWindowMatcher)
                .andExpect("$.events[1].rewards", hasSize(1))
                .andExpect("$.events[1].rewards[0]", lineReward(100).payId("A").positions(of(of(0, 2), of(1, 2), of(2, 2), of(3, 2), of(4, 2))))

                .andExpect("$.events[2].id", equalTo("newWilds"))
                .andExpect("$.events[2].positions", equalTo(of(of(0, 2), of(1, 2), of(2, 2), of(3, 2), of(4, 2))))

                .andExpect("$.events[3].id", equalTo("updatedWindow"))
                .andExpect("$.events[3].totalWin", equalTo(350))
                .andExpect("$.events[3].reelsView.reels", equalTo(of(
                        of("A", "B", "C", "D", "E"),
                        of("B", "G", "C", "E", "B"),
                        of("WL", "WL", "WL", "WL", "WL"),
                        of("C", "E", "C", "G", "C"),
                        of("A", "B", "C", "D", "E"))))
                .andExpect("$.events[3].rewards", hasSize(2))
                .andExpect("$.events[3].rewards[0]", lineReward(50).payId("C").positions(of(of(2, 0), of(2, 1), of(2, 2), of(2, 3), of(2, 4))))
                .andExpect("$.events[3].rewards[1]", lineReward(300).payId("WL").positions(of(of(0, 2), of(1, 2), of(2, 2), of(3, 2), of(4, 2))))

                .andExpect("$.events[4].id", equalTo("newWilds"))
                .andExpect("$.events[4].positions", equalTo(of(of(2, 0), of(2, 1), of(2, 3), of(2, 4))))

                .andExpect("$.events[5].id", equalTo("updatedWindow"))
                .andExpect("$.events[5].totalWin", equalTo(win))
                .andExpect("$.events[5].reelsView.reels", equalTo(of(
                        of("A", "B", "WL", "D", "E"),
                        of("B", "G", "WL", "E", "B"),
                        of("WL", "WL", "WL", "WL", "WL"),
                        of("C", "E", "WL", "G", "C"),
                        of("A", "B", "WL", "D", "E"))))
                .andExpect("$.events[5].rewards", hasSize(2))
                .andExpect("$.events[5].rewards[0]", lineReward(300 * wilds).payId("WL").positions(of(of(0, 2), of(1, 2), of(2, 2), of(3, 2), of(4, 2))))
                .andExpect("$.events[5].rewards[1]", lineReward(300 * wilds).payId("WL").positions(of(of(2, 0), of(2, 1), of(2, 2), of(2, 3), of(2, 4))));
    }

    @Test
    public void testAllWildsLineLessFiveDoesNotPayWin() {
        int wilds = 7;
        int win = 150;
        Matcher<List<List<String>>> initialWindowMatcher = equalTo(of(
                of("A", "B", "C", "D", "E"),
                of("B", "G", "C", "E", "B"),
                of("A", "A", "A", "A", "A"),
                of("C", "E", "C", "G", "C"),
                of("A", "B", "C", "D", "E")));
        play(cheat()
                .line(0, 1, 2, 3, 4)
                .line(1, 6, 2, 4, 1)
                .line(0, 0, 0, 0, 0)
                .line(2, 4, 2, 6, 2)
                .line(0, 1, 2, 3, 4)
                .pawCount(7)
                .paw(1, 2).paw(2, 2).paw(3, 2).paw(4, 2)
                .paw(2, 0).paw(2, 1).paw(2, 3))
                .andExpect("$.totalWin", equalTo(win * wilds))
                .andExpect("$.window.reels", initialWindowMatcher)

                .andExpect("$.rewards", hasSize(2))
                .andExpect("$.events", hasSize(6))

                .andExpect("$.events[0].positions", equalTo(of(
                        of(1, 2), of(2, 2), of(3, 2), of(4, 2),
                        of(2, 0), of(2, 1), of(2, 3)
                )))

                .andExpect("$.events[1].totalWin", equalTo(100))
                .andExpect("$.events[1].reelsView.reels", initialWindowMatcher)
                .andExpect("$.events[1].rewards", hasSize(1))
                .andExpect("$.events[1].rewards[0]", lineReward(100).payId("A").positions(of(of(0, 2), of(1, 2), of(2, 2), of(3, 2), of(4, 2))))

                .andExpect("$.events[2].id", equalTo("newWilds"))
                .andExpect("$.events[2].positions", equalTo(of(of(1, 2), of(2, 2), of(3, 2), of(4, 2))))

                .andExpect("$.events[3].id", equalTo("updatedWindow"))
                .andExpect("$.events[3].totalWin", equalTo(win))
                .andExpect("$.events[3].reelsView.reels", equalTo(of(
                        of("A", "B", "C", "D", "E"),
                        of("B", "G", "C", "E", "B"),
                        of("A", "WL", "WL", "WL", "WL"),
                        of("C", "E", "C", "G", "C"),
                        of("A", "B", "C", "D", "E"))))
                .andExpect("$.events[3].rewards", hasSize(2))
                .andExpect("$.events[3].rewards[0]", lineReward(50).payId("C").positions(of(of(2, 0), of(2, 1), of(2, 2), of(2, 3), of(2, 4))))
                .andExpect("$.events[3].rewards[1]", lineReward(100).payId("A").positions(of(of(0, 2), of(1, 2), of(2, 2), of(3, 2), of(4, 2))))

                .andExpect("$.events[4].id", equalTo("newWilds"))
                .andExpect("$.events[4].positions", equalTo(of(of(2, 0), of(2, 1), of(2, 3))))

                .andExpect("$.events[5].id", equalTo("updatedWindow"))
                .andExpect("$.events[5].totalWin", equalTo(win))
                .andExpect("$.events[5].reelsView.reels", equalTo(of(
                        of("A", "B", "WL", "D", "E"),
                        of("B", "G", "WL", "E", "B"),
                        of("A", "WL", "WL", "WL", "WL"),
                        of("C", "E", "WL", "G", "C"),
                        of("A", "B", "C", "D", "E"))))
                .andExpect("$.events[5].rewards", hasSize(2))
                .andExpect("$.events[5].rewards[0]", lineReward(50 * wilds).payId("C").positions(of(of(2, 0), of(2, 1), of(2, 2), of(2, 3), of(2, 4))))
                .andExpect("$.events[5].rewards[1]", lineReward(100 * wilds).payId("A").positions(of(of(0, 2), of(1, 2), of(2, 2), of(3, 2), of(4, 2))));
    }

    @Test
    public void testLongFlow() {
        int win = 338, wilds = 10;
        Matcher<List<List<String>>> initialWindowMatcher = equalTo(of(
                of("A", "A", "A", "B", "B"),
                of("A", "C", "B", "C", "C"),
                of("G", "A", "A", "C", "C"),
                of("G", "D", "B", "D", "D"),
                of("F", "F", "E", "E", "D")));
        play(cheat()
                .line(0, 0, 0, 1, 1)
                .line(0, 2, 1, 2, 2)
                .line(6, 0, 0, 2, 2)
                .line(6, 3, 1, 3, 3)
                .line(5, 5, 4, 4, 3)
                .pawCount(10)
                .paw(2, 0).paw(4, 0).paw(4, 2).paw(4, 4).paw(2, 4).paw(0, 4).paw(0, 2)
                .paw(2, 2).paw(2, 1).paw(2, 3)
                .jackpotScenario(0).jackpotTable(0).jackpotShufflingIndexes(0, 1, 2, 3, 4, 5))
                .andExpect("$.totalWin", equalTo((win + 150) * wilds))
                .andExpect("$.window.reels", initialWindowMatcher)

                .andExpect("$.rewards", hasSize(14))
                .andExpect("$.events", hasSize(21))

                .andExpect("$.events[0].positions", equalTo(of(
                        of(2, 0), of(4, 0), of(4, 2), of(4, 4), of(2, 4), of(0, 4), of(0, 2),
                        of(2, 2), of(2, 1), of(2, 3)
                )))

                .andExpect("$.events[1].totalWin", equalTo(5))
                .andExpect("$.events[1].reelsView.reels", initialWindowMatcher)
                .andExpect("$.events[1].rewards", hasSize(1))

                .andExpect("$.events[2].id", equalTo("newWilds"))
                .andExpect("$.events[2].positions", equalTo(of(of(2, 0))))
                .andExpect("$.events[3].id", equalTo("updatedWindow"))
                .andExpect("$.events[3].totalWin", equalTo(8))
                .andExpect("$.events[3].reelsView.reels", equalTo(of(
                        of("A", "A", "WL", "B", "B"),
                        of("A", "C", "B", "C", "C"),
                        of("G", "A", "A", "C", "C"),
                        of("G", "D", "B", "D", "D"),
                        of("F", "F", "E", "E", "D"))))
                .andExpect("$.events[3].rewards", hasSize(2))

                .andExpect("$.events[4].id", equalTo("newWilds"))
                .andExpect("$.events[4].positions", equalTo(of(of(4, 0))))
                .andExpect("$.events[5].id", equalTo("updatedWindow"))
                .andExpect("$.events[5].totalWin", equalTo(11))
                .andExpect("$.events[5].reelsView.reels", equalTo(of(
                        of("A", "A", "WL", "B", "WL"),
                        of("A", "C", "B", "C", "C"),
                        of("G", "A", "A", "C", "C"),
                        of("G", "D", "B", "D", "D"),
                        of("F", "F", "E", "E", "D"))))
                .andExpect("$.events[5].rewards", hasSize(3))

                .andExpect("$.events[6].id", equalTo("newWilds"))
                .andExpect("$.events[6].positions", equalTo(of(of(4, 2))))
                .andExpect("$.events[7].id", equalTo("updatedWindow"))
                .andExpect("$.events[7].totalWin", equalTo(13))
                .andExpect("$.events[7].reelsView.reels", equalTo(of(
                        of("A", "A", "WL", "B", "WL"),
                        of("A", "C", "B", "C", "C"),
                        of("G", "A", "A", "C", "WL"),
                        of("G", "D", "B", "D", "D"),
                        of("F", "F", "E", "E", "D"))))
                .andExpect("$.events[7].rewards", hasSize(4))

                .andExpect("$.events[8].id", equalTo("newWilds"))
                .andExpect("$.events[8].positions", equalTo(of(of(4, 4))))
                .andExpect("$.events[9].id", equalTo("updatedWindow"))
                .andExpect("$.events[9].totalWin", equalTo(15))
                .andExpect("$.events[9].reelsView.reels", equalTo(of(
                        of("A", "A", "WL", "B", "WL"),
                        of("A", "C", "B", "C", "C"),
                        of("G", "A", "A", "C", "WL"),
                        of("G", "D", "B", "D", "D"),
                        of("F", "F", "E", "E", "WL"))))
                .andExpect("$.events[9].rewards", hasSize(5))

                .andExpect("$.events[10].id", equalTo("newWilds"))
                .andExpect("$.events[10].positions", equalTo(of(of(2, 4))))
                .andExpect("$.events[11].id", equalTo("updatedWindow"))
                .andExpect("$.events[11].totalWin", equalTo(16))
                .andExpect("$.events[11].reelsView.reels", equalTo(of(
                        of("A", "A", "WL", "B", "WL"),
                        of("A", "C", "B", "C", "C"),
                        of("G", "A", "A", "C", "WL"),
                        of("G", "D", "B", "D", "D"),
                        of("F", "F", "WL", "E", "WL"))))
                .andExpect("$.events[11].rewards", hasSize(6))

                .andExpect("$.events[12].id", equalTo("newWilds"))
                .andExpect("$.events[12].positions", equalTo(of(of(0, 4))))
                .andExpect("$.events[13].id", equalTo("updatedWindow"))
                .andExpect("$.events[13].totalWin", equalTo(17))
                .andExpect("$.events[13].reelsView.reels", equalTo(of(
                        of("A", "A", "WL", "B", "WL"),
                        of("A", "C", "B", "C", "C"),
                        of("G", "A", "A", "C", "WL"),
                        of("G", "D", "B", "D", "D"),
                        of("WL", "F", "WL", "E", "WL"))))
                .andExpect("$.events[13].rewards", hasSize(7))

                .andExpect("$.events[14].id", equalTo("newWilds"))
                .andExpect("$.events[14].positions", equalTo(of(of(0, 2))))
                .andExpect("$.events[15].id", equalTo("updatedWindow"))
                .andExpect("$.events[15].totalWin", equalTo(27))
                .andExpect("$.events[15].reelsView.reels", equalTo(of(
                        of("A", "A", "WL", "B", "WL"),
                        of("A", "C", "B", "C", "C"),
                        of("WL", "A", "A", "C", "WL"),
                        of("G", "D", "B", "D", "D"),
                        of("WL", "F", "WL", "E", "WL"))))
                .andExpect("$.events[15].rewards", hasSize(9))

                .andExpect("$.events[16].id", equalTo("newWilds"))
                .andExpect("$.events[16].positions", equalTo(of(of(2, 2))))
                .andExpect("$.events[17].id", equalTo("updatedWindow"))
                .andExpect("$.events[17].totalWin", equalTo(80))
                .andExpect("$.events[17].reelsView.reels", equalTo(of(
                        of("A", "A", "WL", "B", "WL"),
                        of("A", "C", "B", "C", "C"),
                        of("WL", "A", "WL", "C", "WL"),
                        of("G", "D", "B", "D", "D"),
                        of("WL", "F", "WL", "E", "WL"))))
                .andExpect("$.events[17].rewards", hasSize(11))

                .andExpect("$.events[18].id", equalTo("newWilds"))
                .andExpect("$.events[18].positions", equalTo(of(of(2, 1), of(2, 3))))
                .andExpect("$.events[19].id", equalTo("updatedWindow"))
                .andExpect("$.events[19].totalWin", equalTo(win))
                .andExpect("$.events[19].reelsView.reels", equalTo(of(
                        of("A", "A", "WL", "B", "WL"),
                        of("A", "C", "WL", "C", "C"),
                        of("WL", "A", "WL", "C", "WL"),
                        of("G", "D", "WL", "D", "D"),
                        of("WL", "F", "WL", "E", "WL"))))
                .andExpect("$.events[19].rewards", hasSize(13));
    }
}
