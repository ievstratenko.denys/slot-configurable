package io.onetouch.slot.jewelrycats.testplay.utils;

import io.onetouch.slot.core.TestUtil;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Arrays;

@RequiredArgsConstructor(access = AccessLevel.PUBLIC)
@Accessors(fluent = true, chain = true)
public class JewelryCatsCheat implements TestUtil.Cheat {
    
    private final StringBuilder mock;
    
    public static JewelryCatsCheat cheat(int symbolTable) {
        return new JewelryCatsCheat(new StringBuilder(Integer.toString(symbolTable)));
    }

    public static JewelryCatsCheat cheat() {
        return cheat(0);
    }
    
    private void add(int value) {
        mock.append(DELIMITER).append(value);
    }

    public JewelryCatsCheat line(int... symbols) {
        Arrays.stream(symbols).forEach(this::add);
        return this;
    }

    public JewelryCatsCheat symbolTable(int tableIndex) {
        add(tableIndex);
        return this;
    }

    public JewelryCatsCheat pawTable(int tableIndex) {
        add(tableIndex);
        return this;
    }

    public JewelryCatsCheat pawCount(int pawCount) {
        add(pawCount);
        return this;
    }
    
    public JewelryCatsCheat paw(int reelIndex, int slotIndex) {
        add(reelIndex);
        add(slotIndex);
        return this;
    }

    public JewelryCatsCheat jackpotScenario(int index) {
        add(index);
        return this;
    }

    public JewelryCatsCheat jackpotTable(int index) {
        add(index);
        return this;
    }

    public JewelryCatsCheat jackpotShufflingIndexes(int... indexes) {
        Arrays.stream(indexes).forEach(this::add);
        return this;
    }

    @Override
    public String getMockData() {
        return mock.toString();
    }
}
