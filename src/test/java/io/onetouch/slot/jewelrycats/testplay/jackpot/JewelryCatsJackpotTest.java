package io.onetouch.slot.jewelrycats.testplay.jackpot;

import io.onetouch.slot.jewelrycats.testplay.JewelryCatsAbstractPlayTest;
import lombok.extern.slf4j.Slf4j;
import org.hamcrest.Matcher;
import org.junit.Test;

import java.util.List;

import static io.onetouch.slot.core.matchers.RewardMatcher.reward;
import static io.onetouch.slot.jewelrycats.testplay.utils.JewelryCatsCheat.cheat;
import static java.util.List.of;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;

@Slf4j
public class JewelryCatsJackpotTest extends JewelryCatsAbstractPlayTest {

    @Test
    public void testTriggeredOnInitialWindow() {
        int win = 1000 + 150;
        play(cheat(4)
                .line(0, 0, 0, 0, 0)
                .line(0, 0, 0, 0, 0)
                .line(0, 0, 0, 0, 0)
                .line(0, 0, 0, 0, 0)
                .line(0, 0, 0, 0, 0)
                .pawCount(0)
                .jackpotScenario(0).jackpotTable(0).jackpotShufflingIndexes(2, 1, 0, 4, 3, 5))
                .andExpect("$.totalWin", equalTo(win))
                .andExpect("$.window.reels", equalTo(of(
                        of("A", "A", "A", "A", "A"),
                        of("A", "A", "A", "A", "A"),
                        of("A", "A", "A", "A", "A"),
                        of("A", "A", "A", "A", "A"),
                        of("A", "A", "A", "A", "A"))))

                .andExpect("$.events", hasSize(2))
                .andExpect("$.rewards", hasSize(11))

                .andExpect("$.events[1].id", equalTo("jackpot"))
                .andExpect("$.events[1].totalWin", equalTo(150))
                .andExpect("$.events[1].reelsView.reels", equalTo(of(
                        of("JP2"), of("JP3"), of("JP3"), of("JP1"), of("JP2"), of("JP1"), of("JP1")
                )))
                .andExpect("$.events[1].rewards", hasSize(1))
                .andExpect("$.events[1].rewards[0]", reward(150).payId("JP1").positions(of(of(0, 3), of(0, 5), of(0, 6))));
    }

    @Test
    public void testShuffledScenario() {
        int jackpotWin = 1200;
        int win = 1000 + jackpotWin;
        play(cheat(4)
                .line(0, 0, 0, 0, 0)
                .line(0, 0, 0, 0, 0)
                .line(0, 0, 0, 0, 0)
                .line(0, 0, 0, 0, 0)
                .line(0, 0, 0, 0, 0)
                .pawCount(0)
                .jackpotScenario(1).jackpotTable(2).jackpotShufflingIndexes(6, 5, 4, 3, 2, 1, 0))
                .andExpect("$.totalWin", equalTo(win))
                .andExpect("$.window.reels", equalTo(of(
                        of("A", "A", "A", "A", "A"),
                        of("A", "A", "A", "A", "A"),
                        of("A", "A", "A", "A", "A"),
                        of("A", "A", "A", "A", "A"),
                        of("A", "A", "A", "A", "A"))))

                .andExpect("$.events", hasSize(2))
                .andExpect("$.rewards", hasSize(11))

                .andExpect("$.events[1].id", equalTo("jackpot"))
                .andExpect("$.events[1].totalWin", equalTo(jackpotWin))
                .andExpect("$.events[1].reelsView.reels", equalTo(of(
                        of("JP3"), of("JP1"), of("JP1"), of("JP2"), of("JP2"), of("JP3"), of("JP3")
                )))
                .andExpect("$.events[1].rewards", hasSize(1))
                .andExpect("$.events[1].rewards[0]", reward(1200).payId("JP3").positions(of(of(0, 0), of(0, 5), of(0, 6))));
    }

    @Test
    public void testTriggeredDuringReSpin() {
        int wilds = 6;
        Matcher<List<List<String>>> initialWindowMatcher = equalTo(of(
                of("A", "B", "C", "D", "E"),
                of("F", "G", "G", "G", "F"),
                of("A", "B", "C", "D", "E"),
                of("F", "G", "F", "G", "F"),
                of("A", "B", "C", "D", "E")));
        play(cheat()
                .line(0, 1, 2, 3, 4)
                .line(5, 6, 6, 6, 5)
                .line(0, 1, 2, 3, 4)
                .line(5, 6, 5, 6, 5)
                .line(0, 1, 2, 3, 4)
                .pawCount(3)
                .paw(0, 0).paw(3, 1).paw(3, 2)
                // 1st respin
                .pawTable(0).pawCount(1)
                .paw(4, 2)
                .symbolTable(0)
                .line(0, 1, 2,/**/4)
                .line(5,/* **  **/4)
                .line(0, 1, 2,/**/4)
                .line(5, 6, 5,/**/5)
                .line(0, 1, 2, 3, 4)
                // 2nd respin
                .pawTable(1).pawCount(2)
                .paw(0, 0).paw(0, 4)
                .symbolTable(1)
                .line(0, 1, 2/*  */)
                .line(5 /* ** ** */)
                .line(5, 1 /* ** */)
                .line(5, 6, 5,/**/5)
                .line(0, 1, 2, 3, 4)
                // 3rd respin
                .pawTable(2).pawCount(3)
                .paw(0, 0).paw(0, 4).paw(4, 4)
                .symbolTable(2)
                .line(1, 1, 1/*  */)
                .line(/* * ** ** */)
                .line(/**/5/* ** */)
                .line(/**/5, 5,/**/5)
                .line(5, 5, 5, 5, 5)
                .jackpotScenario(0).jackpotTable(1).jackpotShufflingIndexes(5, 4, 3, 2, 1, 0))

                .andExpect("$.totalWin", equalTo(355 * wilds))
                .andExpect("$.window.reels", initialWindowMatcher)

                .andExpect("$.rewards", hasSize(12))
                .andExpect("$.events", hasSize(17))

                .andExpect("$.events[7].id", equalTo("reSpinWindow"))
                .andExpect("$.events[7].reelsView.reels", equalTo(of(
                        of("A", "B", "C", "D", "E"),
                        of("F", "G", "G", "WL", "E"),
                        of("A", "B", "C", "WL", "E"),
                        of("F", "G", "F", "G", "F"),
                        of("A", "B", "C", "D", "E"))))

                .andExpect("$.events[11].id", equalTo("reSpinWindow"))
                .andExpect("$.events[11].reelsView.reels", equalTo(of(
                        of("A", "B", "C", "D", "E"),
                        of("F", "G", "G", "WL", "E"),
                        of("F", "B", "C", "WL", "WL"),
                        of("F", "G", "F", "G", "F"),
                        of("A", "B", "C", "D", "E"))))

                .andExpect("$.events[13].id", equalTo("reSpinWindow"))
                .andExpect("$.events[13].reelsView.reels", equalTo(of(
                        of("B", "B", "B", "D", "E"),
                        of("F", "G", "G", "WL", "E"),
                        of("F", "F", "C", "WL", "WL"),
                        of("F", "F", "F", "G", "F"),
                        of("F", "F", "F", "F", "F"))))

                .andExpect("$.events[15].id", equalTo("updatedWindow"))
                .andExpect("$.events[15].reelsView.reels", equalTo(of(
                        of("WL", "B", "B", "D", "E"),
                        of("F", "G", "G", "WL", "E"),
                        of("F", "F", "C", "WL", "WL"),
                        of("F", "F", "F", "G", "F"),
                        of("WL", "F", "F", "F", "WL"))))

                .andExpect("$.events[16].id", equalTo("jackpot"))
                .andExpect("$.events[16].reelsView.reels", equalTo(of(
                        of("JP1"), of("JP1"), of("JP2"), of("JP2"), of("JP3"), of("JP3"), of("JP2"))))
                .andExpect("$.events[16].rewards[0]", reward(300 * wilds).payId("JP2").positions(of(of(0, 2), of(0, 3), of(0, 6))));
    }
}
