package io.onetouch.slot.jewelrycats.testplay.main;

import io.onetouch.GameState;
import io.onetouch.slot.core.ResultAction;
import io.onetouch.slot.core.TestUtil;
import io.onetouch.slot.core.matchers.RewardMatcher;
import io.onetouch.slot.jewelrycats.testplay.JewelryCatsAbstractPlayTest;
import lombok.extern.slf4j.Slf4j;
import org.hamcrest.Matcher;
import org.junit.Test;

import java.util.List;

import static io.onetouch.slot.core.matchers.RewardMatcher.lineReward;
import static io.onetouch.slot.jewelrycats.testplay.utils.JewelryCatsCheat.cheat;
import static java.util.List.of;
import static org.hamcrest.Matchers.*;

@Slf4j
public class JewelryCatsMainWinTest extends JewelryCatsAbstractPlayTest {

    @Override
    protected ResultAction<GameState> play(TestUtil.Cheat cheat) {
        return super.play(cheat)
                .andExpect("$.events[0].id", equalTo("initialWindow"));
    }

    @Test
    public void testLoss() {
        play(cheat(0)
                .line(0, 1, 2, 3, 4)
                .line(5, 6, 5, 6, 5)
                .line(0, 1, 2, 3, 4)
                .line(5, 6, 5, 6, 5)
                .line(0, 1, 2, 3, 4)
                .pawCount(0))
                .andExpect("$.totalWin", equalTo(0))
                .andExpect("$.window.reels", equalTo(of(
                        of("A", "B", "C", "D", "E"),
                        of("F", "G", "F", "G", "F"),
                        of("A", "B", "C", "D", "E"),
                        of("F", "G", "F", "G", "F"),
                        of("A", "B", "C", "D", "E"))))
                .andExpect("$.events", hasSize(1))
                .andExpect("$.rewards", empty());
    }

    @Test
    public void testOneLineWin() {
        int win = 1, rewards = 1;
        Matcher<List<List<String>>> windowMatcher = equalTo(of(
                of("A", "B", "C", "D", "E"),
                of("F", "G", "G", "G", "F"),
                of("A", "B", "C", "D", "E"),
                of("F", "G", "F", "G", "F"),
                of("A", "B", "C", "D", "E")));
        RewardMatcher rewardMatcher = lineReward(win)
                .payId("G")
                .lineId(91131)
                .positions(of(of(1, 1), of(2, 1), of(3, 1)));
        play(cheat(1)
                .line(0, 1, 2, 3, 4)
                .line(5, 6, 6, 6, 5)
                .line(0, 1, 2, 3, 4)
                .line(5, 6, 5, 6, 5)
                .line(0, 1, 2, 3, 4)
                .pawCount(0))
                .andExpect("$.totalWin", equalTo(win))
                .andExpect("$.window.reels", windowMatcher)

                .andExpect("$.events", hasSize(1))
                .andExpect("$.rewards", hasSize(rewards))
                .andExpect("$.rewards[0]", rewardMatcher)

                .andExpect("$.events[0].totalWin", equalTo(win))
                .andExpect("$.events[0].reelsView.reels", windowMatcher)
                .andExpect("$.events[0].rewards", hasSize(rewards))
                .andExpect("$.events[0].rewards[0]", rewardMatcher);
    }

    @Test
    public void testSeveralLinesWin() {
        play(cheat(2)
                .line(0, 1, 2, 6, 4)
                .line(5, 5, 5, 6, 5)
                .line(0, 6, 2, 6, 4)
                .line(5, 6, 6, 6, 5)
                .line(0, 2, 2, 6, 4)
                .pawCount(0)
                // 1st respin
                .pawTable(0).pawCount(1)
                .paw(4, 0)
                .symbolTable(0)
                .line(0, 1, 2,/**/4)
                .line(/*       */ 5)
                .line(0, 6, 2,/**/4)
                .line(5, /*    */ 5)
                .line(0, 2, 2, 6, 4)
        )
                .andExpect("$.totalWin", equalTo(22))
                .andExpect("$.window.reels", equalTo(of(
                        of("A", "B", "C", "G", "E"),
                        of("F", "F", "F", "G", "F"),
                        of("A", "G", "C", "G", "E"),
                        of("F", "G", "G", "G", "F"),
                        of("A", "C", "C", "G", "E"))))

                .andExpect("$.events", hasSize(3))
                .andExpect("$.rewards", hasSize(3))
                .andExpect("$.rewards[0]", lineReward(1).payId("F").lineId(90121).positions(of(of(0, 1), of(1, 1), of(2, 1))))
                .andExpect("$.rewards[1]", lineReward(1).payId("G").lineId(91333).positions(of(of(1, 3), of(2, 3), of(3, 3))))
                .andExpect("$.rewards[2]", lineReward(20).payId("G").lineId(93034).positions(of(of(3, 0), of(3, 1), of(3, 2), of(3, 3), of(3, 4))));
    }

    @Test
    public void testManyLinesWin() {
        play(cheat(3)
                .line(0, 0, 0, 1, 0)
                .line(0, 0, 1, 1, 1)
                .line(0, 0, 2, 1, 5)
                .line(3, 3, 3, 1, 5)
                .line(4, 4, 4, 4, 5)
                .pawCount(0)
                // 1st respin
                .pawTable(0).pawCount(1)
                .paw(4, 0)
                .symbolTable(0)
                .line(4) //position [4,0]
                .line(2) //position [2,2]
        )
                .andExpect("$.totalWin", equalTo(29))
                .andExpect("$.window.reels", equalTo(of(
                        of("A", "A", "A", "B", "A"),
                        of("A", "A", "B", "B", "B"),
                        of("A", "A", "C", "B", "F"),
                        of("D", "D", "D", "B", "F"),
                        of("E", "E", "E", "E", "F"))))

                .andExpect("$.events", hasSize(3))
                .andExpect("$.rewards", hasSize(8))
                .andExpect("$.rewards[0]", lineReward(1).payId("F").length(3))
                .andExpect("$.rewards[1]", lineReward(2).payId("D").length(3))
                .andExpect("$.rewards[2]", lineReward(3).payId("B").length(3))
                .andExpect("$.rewards[3]", lineReward(3).payId("E").length(4))
                .andExpect("$.rewards[4]", lineReward(5).payId("A").length(3))
                .andExpect("$.rewards[5]", lineReward(5).payId("A").length(3))
                .andExpect("$.rewards[6]", lineReward(5).payId("A").length(3))
                .andExpect("$.rewards[7]", lineReward(5).payId("B").length(4));
    }

    @Test
    public void testMaxWin() {
        int win = 1000 + 150;
        play(cheat(4)
                .line(0, 0, 0, 0, 0)
                .line(0, 0, 0, 0, 0)
                .line(0, 0, 0, 0, 0)
                .line(0, 0, 0, 0, 0)
                .line(0, 0, 0, 0, 0)
                .pawCount(0)
                .jackpotScenario(0).jackpotTable(0).jackpotShufflingIndexes(0, 1, 2, 3, 4, 5))
                .andExpect("$.totalWin", equalTo(win))
                .andExpect("$.window.reels", equalTo(of(
                        of("A", "A", "A", "A", "A"),
                        of("A", "A", "A", "A", "A"),
                        of("A", "A", "A", "A", "A"),
                        of("A", "A", "A", "A", "A"),
                        of("A", "A", "A", "A", "A"))))

                .andExpect("$.events", hasSize(2))
                .andExpect("$.rewards", hasSize(10 + 1));
    }
}
