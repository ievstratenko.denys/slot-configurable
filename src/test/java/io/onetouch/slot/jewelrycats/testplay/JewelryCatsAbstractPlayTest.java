package io.onetouch.slot.jewelrycats.testplay;

import io.onetouch.slot.jewelrycats.JewelryCatsAbstractTest;
import lombok.Getter;

@Getter
public class JewelryCatsAbstractPlayTest extends JewelryCatsAbstractTest {

    protected final int coinValue = 10; // for simplicity it's the same as the Credits amount
}
