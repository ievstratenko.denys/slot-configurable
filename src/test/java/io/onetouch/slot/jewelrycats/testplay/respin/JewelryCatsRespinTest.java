package io.onetouch.slot.jewelrycats.testplay.respin;

import io.onetouch.GameState;
import io.onetouch.slot.core.ResultAction;
import io.onetouch.slot.core.TestUtil;
import io.onetouch.slot.jewelrycats.testplay.JewelryCatsAbstractPlayTest;
import lombok.extern.slf4j.Slf4j;
import org.hamcrest.Matcher;
import org.junit.Test;

import java.util.List;

import static io.onetouch.slot.core.matchers.RewardMatcher.lineReward;
import static io.onetouch.slot.jewelrycats.testplay.utils.JewelryCatsCheat.cheat;
import static java.util.List.of;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;

@Slf4j
public class JewelryCatsRespinTest extends JewelryCatsAbstractPlayTest {

    @Override
    protected ResultAction<GameState> play(TestUtil.Cheat cheat) {
        return super.play(cheat)
                .andExpect("$.events[0].id", equalTo("pawSeed"))
                .andExpect("$.events[1].id", equalTo("initialWindow"));
    }

    @SuppressWarnings("PointlessArithmeticExpression")
    @Test
    public void testFullFlow() {
        int wilds = 3;
        Matcher<List<List<String>>> initialWindowMatcher = equalTo(of(
                of("A", "B", "C", "D", "E"),
                of("F", "G", "G", "G", "F"),
                of("A", "B", "C", "D", "E"),
                of("F", "G", "F", "G", "F"),
                of("A", "B", "C", "D", "E")));
        play(cheat()
                .line(0, 1, 2, 3, 4)
                .line(5, 6, 6, 6, 5)
                .line(0, 1, 2, 3, 4)
                .line(5, 6, 5, 6, 5)
                .line(0, 1, 2, 3, 4)
                .pawCount(3)
                .paw(0, 0).paw(3, 1).paw(3, 2)
                // 1st respin
                .pawTable(0).pawCount(1)
                .paw(4, 2)
                .symbolTable(0)
                .line(0, 1, 2,/**/4)
                .line(5,/* **  **/4)
                .line(0, 1, 2,/**/4)
                .line(5, 6, 5,/**/5)
                .line(0, 1, 2, 3, 4)
                // 2nd respin
                .pawTable(1).pawCount(2)
                .paw(0, 0).paw(0, 4)
                .symbolTable(1)
                .line(0, 1, 2/*  */)
                .line(5 /* ** ** */)
                .line(5, 1 /* ** */)
                .line(5, 6, 5,/**/5)
                .line(0, 1, 2, 3, 4)
                // 3rd respin
                .pawTable(2).pawCount(3)
                .paw(0, 0).paw(0, 4).paw(4, 4)
                .symbolTable(2)
                .line(3, 1, 2/*  */)
                .line(/* * ** ** */)
                .line(/**/1/* ** */)
                .line(/**/6, 5,/**/5)
                .line(3, 1, 2, 3, 4))

                .andExpect("$.totalWin", equalTo(10 * wilds))
                .andExpect("$.window.reels", initialWindowMatcher)

                .andExpect("$.rewards", hasSize(6))
                .andExpect("$.events", hasSize(14))

                .andExpect("$.events[0].positions", equalTo(of(of(0, 0), of(3, 1), of(3, 2))))

                .andExpect("$.events[1].totalWin", equalTo(1))
                .andExpect("$.events[1].reelsView.reels", initialWindowMatcher)
                .andExpect("$.events[1].rewards", hasSize(1))
                .andExpect("$.events[1].rewards[0]", lineReward(1).payId("G").lineId(91131).positions(of(of(1, 1), of(2, 1), of(3, 1))))

                .andExpect("$.events[2].id", equalTo("newWilds"))
                .andExpect("$.events[2].positions", equalTo(of(of(3, 1))))

                .andExpect("$.events[3].id", equalTo("updatedWindow"))
                .andExpect("$.events[3].totalWin", equalTo(3))
                .andExpect("$.events[3].reelsView.reels", equalTo(of(
                        of("A", "B", "C", "D", "E"),
                        of("F", "G", "G", "WL", "F"),
                        of("A", "B", "C", "D", "E"),
                        of("F", "G", "F", "G", "F"),
                        of("A", "B", "C", "D", "E"))))
                .andExpect("$.events[3].rewards", hasSize(2))
                .andExpect("$.events[3].rewards[0]", lineReward(1).payId("G").lineId(91131).positions(of(of(1, 1), of(2, 1), of(3, 1))))
                .andExpect("$.events[3].rewards[1]", lineReward(2).payId("D").lineId(93032).positions(of(of(3, 0), of(3, 1), of(3, 2))))

                .andExpect("$.events[4].id", equalTo("newWilds"))
                .andExpect("$.events[4].positions", equalTo(of(of(3, 2))))

                .andExpect("$.events[5].id", equalTo("updatedWindow"))
                .andExpect("$.events[5].totalWin", equalTo(4))
                .andExpect("$.events[5].reelsView.reels", equalTo(of(
                        of("A", "B", "C", "D", "E"),
                        of("F", "G", "G", "WL", "F"),
                        of("A", "B", "C", "WL", "E"),
                        of("F", "G", "F", "G", "F"),
                        of("A", "B", "C", "D", "E"))))
                .andExpect("$.events[5].rewards", hasSize(3))
                .andExpect("$.events[5].rewards[0]", lineReward(1).payId("G").lineId(91131).positions(of(of(1, 1), of(2, 1), of(3, 1))))
                .andExpect("$.events[5].rewards[1]", lineReward(1).payId("G").lineId(93133).positions(of(of(3, 1), of(3, 2), of(3, 3))))
                .andExpect("$.events[5].rewards[2]", lineReward(2).payId("D").lineId(93032).positions(of(of(3, 0), of(3, 1), of(3, 2))))

                .andExpect("$.events[6].id", equalTo("pawSeed"))
                .andExpect("$.events[6].positions", equalTo(of(of(4, 2))))

                .andExpect("$.events[7].id", equalTo("reSpinWindow"))
                .andExpect("$.events[7].totalWin", equalTo(6))
                .andExpect("$.events[7].reelsView.reels", equalTo(of(
                        of("A", "B", "C", "D", "E"),
                        of("F", "G", "G", "WL", "E"),
                        of("A", "B", "C", "WL", "E"),
                        of("F", "G", "F", "G", "F"),
                        of("A", "B", "C", "D", "E"))))
                .andExpect("$.events[7].rewards", hasSize(4))
                .andExpect("$.events[7].rewards[0]", lineReward(1).payId("G").lineId(91131).positions(of(of(1, 1), of(2, 1), of(3, 1))))
                .andExpect("$.events[7].rewards[1]", lineReward(1).payId("G").lineId(93133).positions(of(of(3, 1), of(3, 2), of(3, 3))))
                .andExpect("$.events[7].rewards[2]", lineReward(2).payId("D").lineId(93032).positions(of(of(3, 0), of(3, 1), of(3, 2))))
                .andExpect("$.events[7].rewards[3]", lineReward(2).payId("E").lineId(94042).positions(of(of(4, 0), of(4, 1), of(4, 2))))

                .andExpect("$.events[8].id", equalTo("newWilds"))
                .andExpect("$.events[8].positions", equalTo(of(of(4, 2))))

                .andExpect("$.events[9].id", equalTo("updatedWindow"))
                .andExpect("$.events[9].totalWin", equalTo(9))
                .andExpect("$.events[9].reelsView.reels", equalTo(of(
                        of("A", "B", "C", "D", "E"),
                        of("F", "G", "G", "WL", "E"),
                        of("A", "B", "C", "WL", "WL"),
                        of("F", "G", "F", "G", "F"),
                        of("A", "B", "C", "D", "E"))))
                .andExpect("$.events[9].rewards", hasSize(5))
                .andExpect("$.events[9].rewards[0]", lineReward(1).payId("G").lineId(91131).positions(of(of(1, 1), of(2, 1), of(3, 1))))
                .andExpect("$.events[9].rewards[1]", lineReward(1).payId("G").lineId(93133).positions(of(of(3, 1), of(3, 2), of(3, 3))))
                .andExpect("$.events[9].rewards[2]", lineReward(2).payId("D").lineId(93032).positions(of(of(3, 0), of(3, 1), of(3, 2))))
                .andExpect("$.events[9].rewards[3]", lineReward(2).payId("E").lineId(94042).positions(of(of(4, 0), of(4, 1), of(4, 2))))
                .andExpect("$.events[9].rewards[4]", lineReward(3).payId("C").lineId(92242).positions(of(of(2, 2), of(3, 2), of(4, 2))))

                .andExpect("$.events[10].id", equalTo("pawSeed"))
                .andExpect("$.events[10].positions", equalTo(of(of(0, 0), of(0, 4))))

                .andExpect("$.events[11].id", equalTo("reSpinWindow"))
                .andExpect("$.events[11].totalWin", equalTo(10))
                .andExpect("$.events[11].reelsView.reels", equalTo(of(
                        of("A", "B", "C", "D", "E"),
                        of("F", "G", "G", "WL", "E"),
                        of("F", "B", "C", "WL", "WL"),
                        of("F", "G", "F", "G", "F"),
                        of("A", "B", "C", "D", "E"))))
                .andExpect("$.events[11].rewards", hasSize(6))
                .andExpect("$.events[11].rewards[0]", lineReward(1).payId("F").lineId(90103).positions(of(of(0, 1), of(0, 2), of(0, 3))))
                .andExpect("$.events[11].rewards[1]", lineReward(1).payId("G").lineId(91131).positions(of(of(1, 1), of(2, 1), of(3, 1))))
                .andExpect("$.events[11].rewards[2]", lineReward(1).payId("G").lineId(93133).positions(of(of(3, 1), of(3, 2), of(3, 3))))
                .andExpect("$.events[11].rewards[3]", lineReward(2).payId("D").lineId(93032).positions(of(of(3, 0), of(3, 1), of(3, 2))))
                .andExpect("$.events[11].rewards[4]", lineReward(2).payId("E").lineId(94042).positions(of(of(4, 0), of(4, 1), of(4, 2))))
                .andExpect("$.events[11].rewards[5]", lineReward(3).payId("C").lineId(92242).positions(of(of(2, 2), of(3, 2), of(4, 2))))

                .andExpect("$.events[12].id", equalTo("pawSeed"))
                .andExpect("$.events[12].positions", equalTo(of(of(0, 0), of(0, 4), of(4, 4))))

                .andExpect("$.events[13].id", equalTo("reSpinWindow"))
                .andExpect("$.events[13].totalWin", equalTo(10))
                .andExpect("$.events[13].reelsView.reels", equalTo(of(
                        of("D", "B", "C", "D", "E"),
                        of("F", "G", "G", "WL", "E"),
                        of("F", "B", "C", "WL", "WL"),
                        of("F", "G", "F", "G", "F"),
                        of("D", "B", "C", "D", "E"))))
                .andExpect("$.events[13].rewards", hasSize(6))
                .andExpect("$.events[13].rewards[0]", lineReward(1 * wilds).payId("F").lineId(90103).positions(of(of(0, 1), of(0, 2), of(0, 3))))
                .andExpect("$.events[13].rewards[1]", lineReward(1 * wilds).payId("G").lineId(91131).positions(of(of(1, 1), of(2, 1), of(3, 1))))
                .andExpect("$.events[13].rewards[2]", lineReward(1 * wilds).payId("G").lineId(93133).positions(of(of(3, 1), of(3, 2), of(3, 3))))
                .andExpect("$.events[13].rewards[3]", lineReward(2 * wilds).payId("D").lineId(93032).positions(of(of(3, 0), of(3, 1), of(3, 2))))
                .andExpect("$.events[13].rewards[4]", lineReward(2 * wilds).payId("E").lineId(94042).positions(of(of(4, 0), of(4, 1), of(4, 2))))
                .andExpect("$.events[13].rewards[5]", lineReward(3 * wilds).payId("C").lineId(92242).positions(of(of(2, 2), of(3, 2), of(4, 2))))
        ;
    }
}
