package io.onetouch.slot.jewelrycats;

import io.onetouch.core.common.type.GameType;
import io.onetouch.slot.configurable.ConfigurableSlotAbstractTest;
import lombok.Getter;

@Getter
public class JewelryCatsAbstractTest extends ConfigurableSlotAbstractTest {

    private final GameType gameType = GameType.JEWELRY_CATS;
}
