package io.onetouch.slot.jewelrycats.rtp;

import io.onetouch.slot.GameRtpTest;
import io.onetouch.slot.GameRtpTestSettings;
import io.onetouch.slot.jewelrycats.JewelryCatsAbstractTest;
import org.junit.Ignore;
import org.junit.Test;

import static java.util.List.of;

public class JewelryCatsRtpTest extends JewelryCatsAbstractTest {

    private final GameRtpTestSettings settings = GameRtpTestSettings.builder()
            .betSizes(of(1f))
            .coinValues(of(1f))
            .maxRounds(10000_000)
            .build();

    private void test(String gameDefinitionId) {
        settings.setGameDefinitionId(gameDefinitionId);
        new GameRtpTest(gameModule, settings).run().print();
    }

    /**
     * RTP - Expected: 96.123%, Actual: 96.119%
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/362360
     */
    @Test
    @Ignore
    public void test96() {
        test("jewelry_cats_96");
    }

    /**
     * RTP - Expected: 92.179%, Actual: 92.168%
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/362360
     */
    @Test
    @Ignore
    public void test92() {
        test("jewelry_cats_92");
    }

    /**
     * RTP - Expected: 94.110%, Actual: 94.111 %
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/362435
     */
    @Test
    @Ignore
    public void test94() {
        test("jewelry_cats_94");
    }
}
