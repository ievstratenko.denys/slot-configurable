package io.onetouch.slot.deserttreasures.rtp;

import io.onetouch.slot.GameRtpTest;
import io.onetouch.slot.GameRtpTestSettings;
import io.onetouch.slot.deserttreasures.DesertTreasuresAbstractTest;
import org.junit.Ignore;
import org.junit.Test;

import static java.util.List.of;

public class DesertTreasuresRtpTest extends DesertTreasuresAbstractTest {

    private final GameRtpTestSettings settings = GameRtpTestSettings.builder()
            .betSizes(of(1f))
            .coinValues(of(1f))
            .maxRounds(10000_000)
            .build();

    private void test(String gameDefinitionId) {
        settings.setGameDefinitionId(gameDefinitionId);
        new GameRtpTest(gameModule, settings).run().print();
    }

    /**
     * RTP - 96.138%
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/328626
     */
    @Test
    @Ignore
    public void test96() {
        test("desert_treasure_96");
    }

    /**
     * RTP - 94.136%
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/329365
     */
    @Test
    @Ignore
    public void test94() {
        test("desert_treasure_94");
    }


    /**
     * RTP - 60.046
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/328542
     */
    @Test
    @Ignore
    public void testMainScene() {
        test("main_scene");
    }

    /**
     * RTP - 16.087
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/328582
     */
    @Test
    @Ignore
    public void testFreeScene() {
        test("free_scene");
    }

    /**
     * RTP - 10.079%
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/329177
     */
    @Test
    @Ignore
    public void testFreeSceneLow() {
        test("free_scene_low");
    }

    /**
     * RTP - 20.135%
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/329318
     */
    @Test
    @Ignore
    public void testFreeSceneHigh() {
        test("free_scene_high");
    }

    /**
     * RTP - 19.934
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/328576
     */
    @Test
    @Ignore
    public void testBonusScene() {
        test("bonus_scene");
    }
}
