package io.onetouch.slot.deserttreasures.play;

import io.onetouch.GameState;
import io.onetouch.slot.api.SlotReward;
import io.onetouch.slot.api.SlotsGameState;
import io.onetouch.slot.core.ResultAction;
import io.onetouch.slot.core.events.AddFreeSpinsEvent;
import io.onetouch.slot.core.events.EndSceneEvent;
import io.onetouch.slot.core.events.StartSceneEvent;
import lombok.Getter;
import org.junit.Test;

import java.util.List;

import static io.onetouch.slot.core.utils.GameDefinitionUtils.FREE;
import static io.onetouch.slot.core.utils.GameDefinitionUtils.MAIN;
import static java.util.List.of;
import static org.hamcrest.Matchers.*;

@Getter
public class DesertTreasuresFreeGameTest extends DesertTreasuresAbstractPlayTest {

    protected String currentSceneId = FREE;
    protected SlotsGameState state = SlotsGameState.FREE_GAME;
    private final int triggerWin = 10000;

    @Test
    public void testTrigger() {
        List<List<Integer>> positions = of(of(0, 2), of(1, 2), of(2, 2), of(3, 2), of(4, 2));
        trigger()
                .andExpect("$.totalWin", equalTo(triggerWin))
                .andExpect("$.roundWin", equalTo(triggerWin))

                .andExpect("$.window.reelId", equalTo(MAIN))
                .andExpect("$.window.reels", equalTo(of(
                        of("D", "E", "B", "C", "F"),
                        of("G", "A", "H", "D", "E"),
                        of("FG", "FG", "FG", "FG", "FG"))))

                .andExpect("$.rewards", hasSize(1))

                .andExpect("$.rewards[0].id", equalTo(SlotReward.ID))
                .andExpect("$.rewards[0].win", equalTo(triggerWin))
                .andExpect("$.rewards[0].positions", equalTo(positions))
                .andExpect("$.rewards[0].payId", equalTo("FG"))

                .andExpect("$.events", hasSize(1))
                .andExpect("$.events[0].id", equalTo(StartSceneEvent.ID))
                .andExpect("$.events[0].sceneId", equalTo(FREE))
                .andExpect("$.events[0].positions", equalTo(positions))
                .andExpect("$.events[0].spins", equalTo(10));
    }

    private ResultAction<GameState> trigger() {
        return play("3,3,3,3,3");
    }

    @Test
    public void testReTrigger() {
        trigger();

        clientCurrentScene = FREE;
        int win = 3 * 10000;
        List<List<Integer>> positions = of(of(0, 2), of(1, 2), of(2, 2), of(3, 2), of(4, 2));
        play("0," + "3,3,3,3,3")
                .andExpect("$.totalWin", equalTo(win))
                .andExpect("$.roundWin", equalTo(triggerWin + win))

                .andExpect("$.window.reelId", equalTo("free_1"))
                .andExpect("$.window.reels", equalTo(of(
                        of("D", "E", "B", "C", "F"),
                        of("G", "A", "H", "D", "E"),
                        of("FG", "FG", "FG", "FG", "FG"))))

                .andExpect("$.rewards", hasSize(1))

                .andExpect("$.rewards[0].id", equalTo(SlotReward.ID))
                .andExpect("$.rewards[0].win", equalTo(win))
                .andExpect("$.rewards[0].positions", equalTo(positions))
                .andExpect("$.rewards[0].payId", equalTo("FG"))

                .andExpect("$.events", hasSize(1))
                .andExpect("$.events[0].id", equalTo(AddFreeSpinsEvent.ID))
                .andExpect("$.events[0].positions", equalTo(positions))
                .andExpect("$.events[0].spins", equalTo(10))

                .andExpect("$.sceneState.sceneWin", equalTo(win))
                .andExpect("$.sceneState.spins", equalTo(19))
                .andExpect("$.sceneState.initialSpins", equalTo(10))
                .andExpect("$.sceneState.totalSpins", equalTo(20));
    }

    @Test
    public void testFreeSpinsFullRound() {
        trigger();

        for (int spin = 1; spin <= 10; spin++) {
            clientCurrentScene = FREE;
            if (spin == 10) {
                currentSceneId = MAIN;
                state = SlotsGameState.RESOLVED;
            }
            int win = 3 * 10402;
            int reelSetIndex = spin % 2;
            ResultAction<GameState> result = play(reelSetIndex + ",0,0,0,0,0")
                    .andExpect("$.totalWin", equalTo(win))
                    .andExpect("$.roundWin", equalTo(triggerWin + win * spin))

                    .andExpect("$.window.reelId", equalTo("free_" + (reelSetIndex + 1)))
                    .andExpect("$.window.reels", equalTo(of(
                            of("A", "C", "E", "G", "D"),
                            of("B", "D", "F", "H", "A"),
                            of("WL", "WL", "WL", "WL", "WL"))))

                    .andExpect("$.rewards", hasSize(7));

            if (spin < 10) {
                result
                        .andExpect("$.events", empty())
                        .andExpect("$.sceneState.sceneWin", equalTo(win * spin))
                        .andExpect("$.sceneState.spins", equalTo(10 - spin))
                        .andExpect("$.sceneState.initialSpins", equalTo(10))
                        .andExpect("$.sceneState.totalSpins", equalTo(10));
            } else {
                result
                        .andExpect("$.events", hasSize(1))
                        .andExpect("$.events[0].id", equalTo(EndSceneEvent.ID))
                        .andExpect("$.events[0].nextSceneId", equalTo(MAIN))
                        .andExpect("$.events[0].sceneWin", equalTo(win * spin));
            }
        }
    }
}
