package io.onetouch.slot.deserttreasures.play;

import io.onetouch.GameState;
import io.onetouch.slot.api.SlotReward;
import io.onetouch.slot.core.ResultAction;
import io.onetouch.slot.core.TestUtil;
import io.onetouch.slot.core.rewards.LineWinReward;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static io.onetouch.slot.core.utils.GameDefinitionUtils.MAIN;
import static java.util.List.of;
import static org.hamcrest.Matchers.*;

public class DesertTreasuresBaseGameTest extends DesertTreasuresAbstractPlayTest {

    @Test
    public void testLoss() {
        play("10,10,10,10,10").andExpect("$.totalWin", equalTo(0))
                .andExpect("$.roundWin", equalTo(0))

                .andExpect("$.window.reelId", equalTo(MAIN))
                .andExpect("$.window.reels", equalTo(of(
                        of("E", "H", "C", "F", "G"),
                        of("A", "C", "E", "G", "D"),
                        of("B", "D", "F", "H", "A"))))

                .andExpect("$.rewards", empty())
                .andExpect("$.events", empty());
    }

    @Test
    public void testOneLineWin() {
        int win = 20;
        play("6,0,10,10,10")
                .andExpect("$.totalWin", equalTo(win))
                .andExpect("$.roundWin", equalTo(win))

                .andExpect("$.window.reels", equalTo(of(
                        of("C", "C", "C", "F", "G"),
                        of("H", "D", "E", "G", "D"),
                        of("XX", "WL", "F", "H", "A"))))

                .andExpect("$.rewards", hasSize(1))
                .andExpect("$.rewards[0].id", equalTo(LineWinReward.ID))
                .andExpect("$.rewards[0].win", equalTo(win))
                .andExpect("$.rewards[0].positions", equalTo(of(of(0, 0), of(1, 0), of(2, 0))))
                .andExpect("$.rewards[0].payId", equalTo("C"))
                .andExpect("$.rewards[0].lineId", equalTo(2))

                .andExpect("$.events", empty());
    }

    @Test
    public void testOneScatterWin() {
        int win = 40;
        play("4,6,0,0,3")
                .andExpect("$.totalWin", equalTo(win))
                .andExpect("$.roundWin", equalTo(win))

                .andExpect("$.window.reels", equalTo(of(
                        of("G", "B", "E", "G", "F"),
                        of("FG", "F", "F", "H", "E"),
                        of("C", "XX", "WL", "WL", "FG"))))

                .andExpect("$.rewards", hasSize(1))
                .andExpect("$.rewards[0].id", equalTo(SlotReward.ID))
                .andExpect("$.rewards[0].win", equalTo(win))
                .andExpect("$.rewards[0].positions", equalTo(of(of(0, 1), of(4, 2))))
                .andExpect("$.rewards[0].payId", equalTo("FG"))

                .andExpect("$.events", empty());
    }

    @Test
    public void testManyLinesWin() {
        int win = 10402;
        play("0,0,0,0,0")
                .andExpect("$.totalWin", equalTo(win))
                .andExpect("$.roundWin", equalTo(win))

                .andExpect("$.window.reels", equalTo(of(
                        of("A", "C", "E", "G", "D"),
                        of("B", "D", "F", "H", "A"),
                        of("WL", "WL", "WL", "WL", "WL"))))

                .andExpect("$.rewards", hasSize(7))
                .andExpect("$.events", empty());
    }

    @Override
    protected ResultAction<GameState> play(String mock) {
        List<Integer> reelStops = Arrays.stream(mock.split(TestUtil.Cheat.DELIMITER))
                .map(Integer::valueOf)
                .collect(Collectors.toList());
        return super.play(mock)
                .andExpect("$.window.reelStops", equalTo(reelStops));
    }
}
