package io.onetouch.slot.deserttreasures.play;

import lombok.Getter;
import org.junit.Test;

import static io.onetouch.slot.api.SlotsGameState.BONUS_GAME;
import static io.onetouch.slot.api.SlotsGameState.FREE_GAME;
import static io.onetouch.slot.core.utils.GameDefinitionUtils.BONUS;
import static io.onetouch.slot.core.utils.GameDefinitionUtils.FREE;
import static java.util.List.of;
import static org.hamcrest.Matchers.*;

@Getter
public class DesertTreasuresBonusGameTest extends DesertTreasuresAbstractPlayTest {
    
    protected final int coinValue = 10; // for simplicity it's the same as the Credits amount

    @Test
    public void testTrigger() {
        currentSceneId = BONUS;
        state = BONUS_GAME;
        play("6,6,6,6,6")
                .andExpect("$.events", hasSize(1));
    }

    @Test
    public void testNotTriggerWhenNoLine() {
        play("6,6,0,6,6")
                .andExpect("$.events", empty());
    }

    @Test
    public void testBonusGame() {
        currentSceneId = BONUS;
        state = BONUS_GAME;
        play("6,6,6,6,6");

        clientCurrentScene = BONUS;

        int bonusWin = 5;
        play("0")
                .andExpect("$.totalWin", equalTo(bonusWin))
                .andExpect("$.rewards", hasSize(1))
                .andExpect("$.window.reels", equalTo(of(
                        of("S0")
                )));
    }

    @Test
    public void testBonusGameTriggeredFromFreeGame() {
        currentSceneId = FREE;
        state = FREE_GAME;
        play("3,3,3,3,3");

        for (int i = 0; i < 2; i++) {
            clientCurrentScene = FREE;
            currentSceneId = BONUS;
            state = BONUS_GAME;
            play("0,6,6,6,6,6");

            clientCurrentScene = BONUS;
            play("0");
            play("1");
            play("2");
            play("3");
            currentSceneId = FREE;
            state = FREE_GAME;
            play("4");
        }
    }
}
