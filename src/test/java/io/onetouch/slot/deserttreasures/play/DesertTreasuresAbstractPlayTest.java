package io.onetouch.slot.deserttreasures.play;

import io.onetouch.slot.deserttreasures.DesertTreasuresAbstractTest;
import lombok.Getter;

@Getter
public class DesertTreasuresAbstractPlayTest extends DesertTreasuresAbstractTest {

    protected final int coinValue = 20; // for simplicity it's the same as the Credits amount
}