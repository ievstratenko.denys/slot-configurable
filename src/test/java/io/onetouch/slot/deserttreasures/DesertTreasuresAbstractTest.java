package io.onetouch.slot.deserttreasures;

import io.onetouch.core.common.type.GameType;
import io.onetouch.slot.configurable.ConfigurableSlotAbstractTest;
import lombok.Getter;

@Getter
public class DesertTreasuresAbstractTest extends ConfigurableSlotAbstractTest {

    private final GameType gameType = GameType.DESERT_TREASURE;
}
