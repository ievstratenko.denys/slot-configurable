package io.onetouch.slot.seatreasure.teststatic;

import io.onetouch.slot.api.Reel;
import io.onetouch.slot.api.SlotSymbolType;
import io.onetouch.slot.core.reels.BaseSlotSymbol;
import io.onetouch.slot.core.scene.ReelScene;
import io.onetouch.slot.seatreasure.SeaTreasureAbstractTest;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static io.onetouch.slot.api.SlotSymbolType.*;

@SuppressWarnings("FieldCanBeLocal")
public class SeaTreasureStaticTest extends SeaTreasureAbstractTest {

    private final SeaTreasureStaticExpectedData expectedData = new SeaTreasureStaticExpectedData();
    private final List<SlotSymbolType> symbolMapping = List.of(J, I, H, G, F, E, D, C, B, A, WL, SC, SC);
    private final int reelsCount = 5, reelSize = 192, reelTotalWeights = 65536, reelShift = 2;

    @Test
    public void testBaseReelSet() {
        ReelScene mainScene = (ReelScene) gameDefinition.getScenes().get(0);
        List<? extends Reel> reels = mainScene.getReelsProvider().getOriginalReelSets().iterator().next().getReels();
        for (int reel = 0; reel < reelsCount; reel++) {
            int totalWeight = 0;
            for (int pos = 0; pos < reelSize; pos++) {
                int expectedSymbolIndex = expectedData.normalReelData[pos][reel];
                SlotSymbolType expectedSymbol = symbolMapping.get(expectedSymbolIndex);
                BaseSlotSymbol actualSymbol = (BaseSlotSymbol) reels.get(reel).getSymbols().get(pos);
                Assert.assertEquals("Symbol type check. Reel " + reel + ", pos " + pos,
                        expectedSymbol, actualSymbol.getSymbolType());

                int bottomBasedPos = (pos + reelShift) % reelSize;
                int expectedWeight = expectedData.normalStopNum[bottomBasedPos][reel];
                int actualWeight = actualSymbol.getWeight();
                Assert.assertEquals("Symbol weight check. Reel " + reel + ", pos " + pos,
                        expectedWeight, actualWeight);
                totalWeight += actualWeight;
            }
            Assert.assertEquals("Total weight check. Reel " + reel, reelTotalWeights, totalWeight);
        }
    }

    @Test
    public void testFreeReelSet() {
        ReelScene freeScene = (ReelScene) gameDefinition.getScenes().get(1);
        List<? extends Reel> reels = freeScene.getReelsProvider().getOriginalReelSets().iterator().next().getReels();
        for (int reel = 0; reel < reelsCount; reel++) {
            int totalWeight = 0;
            for (int pos = 0; pos < reelSize; pos++) {
                int expectedSymbolIndex = expectedData.freeSpinReelData[pos][reel];
                SlotSymbolType expectedSymbol = symbolMapping.get(expectedSymbolIndex);
                BaseSlotSymbol actualSymbol = (BaseSlotSymbol) reels.get(reel).getSymbols().get(pos);
                Assert.assertEquals("Symbol type check. Reel " + reel + ", pos " + pos,
                        expectedSymbol, actualSymbol.getSymbolType());

                int bottomBasedPos = (pos + reelShift) % reelSize;
                int expectedWeight = expectedData.freeSpinStopNum[bottomBasedPos][reel];
                int actualWeight = actualSymbol.getWeight();
                Assert.assertEquals("Symbol weight check. Reel " + reel + ", pos " + pos,
                        expectedWeight, actualWeight);
                totalWeight += actualWeight;
            }
            Assert.assertEquals("Total weight check. Reel " + reel, reelTotalWeights, totalWeight);
        }
    }
}
