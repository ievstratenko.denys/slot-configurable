package io.onetouch.slot.seatreasure.testplay.main;

import io.onetouch.GameState;
import io.onetouch.slot.core.ResultAction;
import io.onetouch.slot.seatreasure.testplay.SeaTreasureAbstractPlayTest;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;

@Slf4j
public class SeaTreasureMainWinMultiplierTest extends SeaTreasureAbstractPlayTest {

    @Getter
    private int baseWin = 15000;

    private ResultAction<GameState> lose() {
        return play("12,12,12,12,12" + BASE_GAME_MOCK_SUFFIX).andExpect("$.totalWin", equalTo(0));
    }

    private ResultAction<GameState> win(int expectedMultiplier) {
        return play("0,0,0,0,0")
                .andExpect(APPLIED_MULTIPLIER_PATH, equalTo(expectedMultiplier))
                .andExpect("$.totalWin", equalTo(getBaseWin() * expectedMultiplier));
    }

    @Test
    public void testIncrementWhenWin() {
        win(1).andExpect(defaultMultiplierPath, equalTo(2));
    }

    @Test
    public void testNoIncrementWhenLose() {
        lose().andExpect(defaultMultiplierPath, equalTo(1));
    }

    @Test
    public void testLimit() {
        win(1).andExpect(defaultMultiplierPath, equalTo(2));
        win(2).andExpect(defaultMultiplierPath, equalTo(3));
        win(3).andExpect(defaultMultiplierPath, equalTo(4));
        win(4).andExpect(defaultMultiplierPath, equalTo(5));
        win(5).andExpect(defaultMultiplierPath, equalTo(5));
        win(5).andExpect(defaultMultiplierPath, equalTo(5));
    }

    @Test
    public void testReset() {
        win(1).andExpect(defaultMultiplierPath, equalTo(2));
        win(2).andExpect(defaultMultiplierPath, equalTo(3));
        win(3).andExpect(defaultMultiplierPath, equalTo(4));
        lose().andExpect(defaultMultiplierPath, equalTo(1));
    }

    @Test
    public void testMultiplierForSeveralBet() {
        win(1).andExpect(defaultMultiplierPath, equalTo(2));
        win(2).andExpect(defaultMultiplierPath, equalTo(3));

        setCoinValue(2 * coinValue);
        baseWin *= 2;
        win(1)
                .andExpect(defaultMultiplierPath, equalTo(3))
                .andExpect(BASE_MULTIPLIER_PATH + 20, equalTo(2));
        win(2)
                .andExpect(defaultMultiplierPath, equalTo(3))
                .andExpect(BASE_MULTIPLIER_PATH + 20, equalTo(3));

        setCoinValue(1);
        baseWin = 1500;
        win(1)
                .andExpect(defaultMultiplierPath, equalTo(3))
                .andExpect(BASE_MULTIPLIER_PATH + 20, equalTo(3))
                .andExpect(BASE_MULTIPLIER_PATH + 1, equalTo(2));
        lose()
                .andExpect(defaultMultiplierPath, equalTo(3))
                .andExpect(BASE_MULTIPLIER_PATH + 20, equalTo(3))
                .andExpect(BASE_MULTIPLIER_PATH + 1, equalTo(1));

        setCoinValue(20);
        lose()
                .andExpect(defaultMultiplierPath, equalTo(3))
                .andExpect(BASE_MULTIPLIER_PATH + 20, equalTo(1))
                .andExpect(BASE_MULTIPLIER_PATH + 1, equalTo(1));
    }
}
