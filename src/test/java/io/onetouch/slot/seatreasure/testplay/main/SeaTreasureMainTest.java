package io.onetouch.slot.seatreasure.testplay.main;

import io.onetouch.GameState;
import io.onetouch.slot.core.ResultAction;
import io.onetouch.slot.seatreasure.testplay.SeaTreasureAbstractPlayTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.stream.IntStream;

import static io.onetouch.slot.core.matchers.RewardMatcher.lineReward;
import static java.util.List.of;
import static org.hamcrest.Matchers.*;

@Slf4j
public class SeaTreasureMainTest extends SeaTreasureAbstractPlayTest {

    @Test
    public void testLoss() {
        play("12,12,12,12,12" + BASE_GAME_MOCK_SUFFIX)
                .andExpect("$.totalWin", equalTo(0))
                .andExpect("$.window.reels", equalTo(of(
                        of("A", "C", "G", "A", "SC"),
                        of("A", "C", "SC", "A", "A"),
                        of("A", "C", "C", "A", "A"))))
                .andExpect("$.events", empty())
                .andExpect("$.rewards", empty());
    }

    @Test
    public void testFewLinesWin() {
        play("10,34,164,136,24" + BASE_GAME_MOCK_SUFFIX)
                .andExpect("$.totalWin", equalTo(1000 + 50))
                .andExpect("$.window.reels", equalTo(of(
                        of("G", "J", "H", "G", "A"),
                        of("G", "J", "J", "WL", "SC"),
                        of("A", "A", "A", "B", "E"))))
                .andExpect("$.events", empty())
                .andExpect("$.rewards", hasSize(2))
                .andExpect("$.rewards[0]", lineReward(50).lineId(3).payId("A").positions(of(of(0, 2), of(1, 2), of(2, 2))))
                .andExpect("$.rewards[1]", lineReward(1000).lineId(30).payId("A").positions(of(of(0, 2), of(1, 2), of(2, 2), of(3, 1), of(4, 0))));
    }

    @Test
    public void testSeveralLinesWin() {
        int lines = 11, winPerLine = 1000;
        ResultAction<GameState> resultAction = play("186,186,9,186,186")
                .andExpect("$.totalWin", equalTo(lines * winPerLine))
                .andExpect("$.window.reels", equalTo(of(
                        of("A", "A", "B", "A", "A"),
                        of("A", "A", "WL", "A", "A"),
                        of("A", "A", "G", "A", "A"))))
                .andExpect("$.events", empty())
                .andExpect("$.rewards", hasSize(lines));

        IntStream.range(0, lines).forEach(i -> resultAction.andExpect("$.rewards[" + i + "]", lineReward(winPerLine).payId("A").length(5)));
    }

    @Test
    public void testMaxLinesAmountWin() {
        play("0,0,0,0,0")
                .andExpect("$.totalWin", equalTo(15000))
                .andExpect("$.window.reels", equalTo(of(
                        of("B", "B", "B", "B", "B"),
                        of("B", "B", "B", "B", "B"),
                        of("B", "B", "B", "B", "B"))))
                .andExpect("$.events", empty())
                .andExpect("$.rewards", hasSize(30));
    }
}
