package io.onetouch.slot.seatreasure.testplay.winlimit;

import io.onetouch.slot.api.SlotsGameState;
import io.onetouch.slot.core.events.EndSceneEvent;
import io.onetouch.slot.seatreasure.testplay.SeaTreasureAbstractPlayTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;

import static io.onetouch.slot.core.utils.GameDefinitionUtils.FREE;
import static io.onetouch.slot.core.utils.GameDefinitionUtils.MAIN;
import static java.util.List.of;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;

@Slf4j
public class SeaTreasureWinLimitTest extends SeaTreasureAbstractPlayTest {
    
    private final static int WIN_LIMIT = 20_000;
    private int maxWin;

    @Before
    public void init() {
        super.init();
        maxWin = WIN_LIMIT * betSize * coinValue;
        gameDefinition = getGameModule().getGameDefinitions()
                .stream()
                .filter(gd -> gd.getId().equals("sea_treasure_96_limit_20k"))
                .findFirst()
                .orElseThrow();
        currentSceneId = FREE;
        state = SlotsGameState.FREE_GAME;
    }

    @Test
    public void testWinLimitDuringFreeGame() {
        play("9,0,11,0,12" + ",2");

        clientCurrentScene = FREE;

        currentSceneId = MAIN;
        state = SlotsGameState.RESOLVED;
        
        int lineWin = 1000, lines = 30, multiplier = 12;
        int baseWin = lineWin * lines, win = multiplier * baseWin, roundWin = win;
        play("186,186,186,186,186," + "8")
                .andExpect("$.roundWin", equalTo(maxWin))
                .andExpect("$.totalWin", equalTo(maxWin))
                .andExpect("$.window.reels", equalTo(of(
                        of("A", "A", "A", "A", "A"),
                        of("A", "A", "A", "A", "A"),
                        of("A", "A", "A", "A", "A"))))
                .andExpect("$.events", hasSize(1))
                .andExpect("$.events[0].id", equalTo(EndSceneEvent.ID))
                .andExpect("$.rewards", hasSize(lines))

                .andExpect(CRASH_DIVE_PATH, equalTo(10))
                .andExpect(APPLIED_MULTIPLIER_PATH, equalTo(12))
                .andExpect(defaultMultiplierPath, equalTo(1));
    }
}
