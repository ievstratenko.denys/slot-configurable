package io.onetouch.slot.seatreasure.testplay.free;

import io.onetouch.slot.api.SlotsGameState;
import io.onetouch.slot.core.events.AddFreeSpinsEvent;
import io.onetouch.slot.core.events.EndSceneEvent;
import io.onetouch.slot.core.events.StartSceneEvent;
import io.onetouch.slot.seatreasure.testplay.SeaTreasureAbstractPlayTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static io.onetouch.slot.core.matchers.RewardMatcher.reward;
import static io.onetouch.slot.core.utils.GameDefinitionUtils.FREE;
import static io.onetouch.slot.core.utils.GameDefinitionUtils.MAIN;
import static java.util.List.of;
import static org.hamcrest.Matchers.*;

@Slf4j
public class SeaTreasureFreeTest extends SeaTreasureAbstractPlayTest {

    static final Map<Integer, Integer> SPINS_PER_TRIGGER_OPTIONS = Map.of(
            0, 3,
            1, 5,
            2, 10);

    @Before
    public void init() {
        super.init();
        currentSceneId = FREE;
        state = SlotsGameState.FREE_GAME;
    }

    private void testTrigger(int option) {
        play("9,0,11,0,12," + option)
                .andExpect("$.totalWin", equalTo(0))
                .andExpect("$.window.reels", equalTo(of(
                        of("SC", "B", "G", "B", "SC"),
                        of("G", "B", "G", "B", "A"),
                        of("G", "B", "SC", "B", "A"))))
                .andExpect("$.events", hasSize(1))
                .andExpect("$.events[0].id", equalTo(StartSceneEvent.ID))
                .andExpect("$.events[0].sceneId", equalTo(FREE))
                .andExpect("$.events[0].positions", equalTo(of(of(0, 0), of(2, 2), of(4, 0))))
                .andExpect("$.events[0].spins", equalTo(SPINS_PER_TRIGGER_OPTIONS.get(option)))
                .andExpect("$.rewards", empty())
                .andExpect(APPLIED_MULTIPLIER_PATH, equalTo(1))
                .andExpect(defaultMultiplierPath, equalTo(2));
    }

    @Test
    public void testTrigger_3_GuaranteedFreeSpins() {
        testTrigger(0);
    }

    @Test
    public void testTrigger_5_GuaranteedFreeSpins() {
        testTrigger(1);
    }

    @Test
    public void testTrigger_10_GuaranteedFreeSpins() {
        testTrigger(2);
    }

    @Test
    public void testFullFlow() {
        testTrigger(0);

        clientCurrentScene = FREE;

        int win = 20 * 2, roundWin = win;
        play("9,12,12,12,12," + "0")
                .andExpect("$.roundWin", equalTo(roundWin))
                .andExpect("$.totalWin", equalTo(win))
                .andExpect("$.window.reels", equalTo(of(
                        of("SC", "C", "G", "WL", "SC"),
                        of("G", "C", "G", "WL", "C"),
                        of("G", "C", "SC", "WL", "C"))))
                .andExpect("$.events", empty())
                .andExpect("$.rewards", hasSize(1))
                .andExpect("$.rewards[0]", reward(win).payId("SC").positions(of(of(0, 0), of(2, 2), of(4, 0))))

                .andExpect("$.sceneState.sceneWin", equalTo(win))
                .andExpect("$.sceneState.spinsPlayed", equalTo(1))
                .andExpect("$.sceneState.spins", equalTo(2))
                .andExpect("$.sceneState.initialSpins", equalTo(3))
                .andExpect("$.sceneState.totalSpins", equalTo(3))

                .andExpect(CRASH_DIVE_PATH, equalTo(0))
                .andExpect(APPLIED_MULTIPLIER_PATH, equalTo(2))
                .andExpect(defaultMultiplierPath, equalTo(3));

        play("10,10,0,0,0," + "1")
                .andExpect("$.roundWin", equalTo(roundWin))
                .andExpect("$.totalWin", equalTo(0))
                .andExpect("$.events", empty())
                .andExpect("$.rewards", empty())

                .andExpect("$.sceneState.sceneWin", equalTo(40))
                .andExpect("$.sceneState.spinsPlayed", equalTo(2))
                .andExpect("$.sceneState.spins", equalTo(1))
                .andExpect("$.sceneState.initialSpins", equalTo(3))
                .andExpect("$.sceneState.totalSpins", equalTo(3))

                .andExpect(CRASH_DIVE_PATH, equalTo(3))
                .andExpect(APPLIED_MULTIPLIER_PATH, equalTo(6))
                .andExpect(defaultMultiplierPath, equalTo(6));

        play("10,10,0,0,0," + "8")
                .andExpect("$.roundWin", equalTo(roundWin))
                .andExpect("$.totalWin", equalTo(0))
                .andExpect("$.events", hasSize(1))
                .andExpect("$.rewards", empty())
                
                .andExpect("$.sceneState.spinsPlayed", equalTo(3))
                .andExpect("$.sceneState.spins", equalTo(1))

                .andExpect("$.events[0].id", equalTo(AddFreeSpinsEvent.ID))
                .andExpect("$.events[0].spins", equalTo(1))

                .andExpect(CRASH_DIVE_PATH, equalTo(10))
                .andExpect(APPLIED_MULTIPLIER_PATH, equalTo(16))
                .andExpect(defaultMultiplierPath, equalTo(16));

        currentSceneId = MAIN;
        state = SlotsGameState.RESOLVED;

        win = 20 * 16;
        roundWin += win;
        play("9,12,12,12,12," + "0")
                .andExpect("$.roundWin", equalTo(roundWin))
                .andExpect("$.totalWin", equalTo(win))
                .andExpect("$.events", hasSize(1))
                .andExpect("$.rewards", hasSize(1))
                .andExpect("$.rewards[0]", reward(win).payId("SC").positions(of(of(0, 0), of(2, 2), of(4, 0))))

                .andExpect("$.events[0].id", equalTo(EndSceneEvent.ID))

                .andExpect(CRASH_DIVE_PATH, equalTo(0))
                .andExpect(APPLIED_MULTIPLIER_PATH, equalTo(16))
                .andExpect(defaultMultiplierPath, equalTo(1));
    }
}
