package io.onetouch.slot.seatreasure.testplay;

import io.onetouch.slot.seatreasure.SeaTreasureAbstractTest;
import lombok.Getter;

@Getter
public class SeaTreasureAbstractPlayTest extends SeaTreasureAbstractTest {

    protected static final String CRASH_DIVE_PATH = "$.additionalInfo.crashDive";
    protected static final String APPLIED_MULTIPLIER_PATH = "$.additionalInfo.appliedWinMultiplier";
    protected static final String BASE_MULTIPLIER_PATH = "$.featureStates.winMultiplier.valueBerBet.";
    protected static final String BASE_GAME_MOCK_SUFFIX = ",0"; // required if there is at least one Scatter on the window
    protected final String defaultMultiplierPath;


    public SeaTreasureAbstractPlayTest() {
        coinValue = 10; // for simplicity it's the same as the Credits amount
        defaultMultiplierPath = BASE_MULTIPLIER_PATH + coinValue;
    }
}
