package io.onetouch.slot.seatreasure;

import io.onetouch.core.common.type.GameType;
import io.onetouch.slot.configurable.ConfigurableSlotAbstractTest;
import lombok.Getter;

@Getter
public class SeaTreasureAbstractTest extends ConfigurableSlotAbstractTest {

    private final GameType gameType = GameType.SEA_TREASURE;
}
