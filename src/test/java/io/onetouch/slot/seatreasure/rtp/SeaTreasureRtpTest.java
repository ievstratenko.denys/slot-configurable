package io.onetouch.slot.seatreasure.rtp;

import io.onetouch.slot.GameRtpTest;
import io.onetouch.slot.GameRtpTestSettings;
import io.onetouch.slot.seatreasure.SeaTreasureAbstractTest;
import org.junit.Ignore;
import org.junit.Test;

import static java.util.List.of;

public class SeaTreasureRtpTest extends SeaTreasureAbstractTest {

    private final GameRtpTestSettings settings = GameRtpTestSettings.builder()
            .betSizes(of(1f))
            .coinValues(of(1f))
            .maxRounds(10000_000)
            .build();

    private void test(String gameDefinitionId) {
        settings.setGameDefinitionId(gameDefinitionId);
        new GameRtpTest(gameModule, settings).run().print();
    }

    /**
     * RTP - Expected: 96.149%, Actual: 96.133%
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/378406 - 96.115 - 2B
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/377478 - 96.179 - 2B
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/381243 - 96.069 - 2B
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/381500 - 96.168 - 2B
     */
    @Test
    @Ignore
    public void test96() {
        test("sea_treasure_96");
    }

    /**
     * RTP - Expected: 92.084%, Actual: 92.099% on 8B
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/379069 - 92.099 - 2B
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/378846 - 92.114 - 2B
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/377937 - 92.054 - 2B
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/377019 - 92.130 - 2B
     */
    @Test
    @Ignore
    public void test92() {
        test("sea_treasure_92");
    }

    /**
     * RTP - Expected: 94.134%, Actual: 94.149% on 8B
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/378867 - 94.164 - 2B
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/377651 - 94.103 - 2B
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/377037 - 94.204 - 2B
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/381860 - 94.126 - 2B
     */
    @Test
    @Ignore
    public void test94() {
        test("sea_treasure_94");
    }

    /**
     * RTP - Expected: 96.145%, Actual: 96.103%
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/382208 - 96.148 - 2B
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/382521 - 96.035 - 2B
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/382988 - 96.044 - 2B
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/383192 - 96.184 - 2B
     */
    @Test
    @Ignore
    public void test96Limit() {
        test("sea_treasure_96_limit_20k");
    }

    /**
     * RTP - Expected: 92.091%, Actual: 92.079%
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/383193 - 92.050 - 2B
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/383211 - 92.037 - 2B
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/383780 - 92.157 - 2B
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/383264 - 92.073 - 2B
     */
    @Test
    @Ignore
    public void test92Limit() {
        test("sea_treasure_92_limit_20k");
    }

    /**
     * RTP - Expected: 94.126%, Actual: 94.148%
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/384253 - 94.188 - 2B
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/384897 - 94.108 - 2B
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/385271 - 94.149 - 2B
     */
    @Test
    @Ignore
    public void test94Limit() {
        test("sea_treasure_94_limit_20k");
    }
}
