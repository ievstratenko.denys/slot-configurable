package io.onetouch.slot.beatex.testplay;

import io.onetouch.GameState;
import io.onetouch.slot.api.SlotsGameState;
import io.onetouch.slot.core.ResultAction;
import io.onetouch.slot.core.events.AddFreeSpinsEvent;
import io.onetouch.slot.core.events.EndSceneEvent;
import io.onetouch.slot.core.events.StartSceneEvent;
import io.onetouch.slot.core.rewards.LineWinReward;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.List;

import static io.onetouch.slot.core.utils.GameDefinitionUtils.MAIN;
import static java.util.List.of;
import static org.hamcrest.Matchers.*;

@Slf4j
@Getter
public class BeatExFreeSpinsTest extends BeatExAbstractPlayTest {

    protected String currentSceneId = "free_1";
    protected SlotsGameState state = SlotsGameState.FREE_GAME;

    /**
     * According to the game's math FS trigger does not work basing on scatters but lines
     */
    @Test
    public void testScattersDoNotTrigger() {
        currentSceneId = MAIN;
        state = SlotsGameState.RESOLVED;

        int win = 0;
        play("4,13,0,0,0")
                .andExpect("$.totalWin", equalTo(win))
                .andExpect("$.roundWin", equalTo(win))

                .andExpect("$.window.reelId", equalTo(MAIN))
                .andExpect("$.window.reels", equalTo(of(
                        of("S1", "C", "H", "G", "S1"),
                        of("S1", "C", "H", "H", "C"),
                        of("D", "D", "S1", "G", "H"))))

                .andExpect("$.rewards", empty())
                .andExpect("$.events", empty());
    }

    @Test
    public void testTrigger() {
        int win = 100;
        trigger()
                .andExpect("$.totalWin", equalTo(win))
                .andExpect("$.roundWin", equalTo(win))

                .andExpect("$.window.reelId", equalTo(MAIN))
                .andExpect("$.window.reels", equalTo(of(
                        of("S1", "S1", "H", "G", "S1"),
                        of("S1", "C", "H", "H", "C"),
                        of("D", "C", "S1", "G", "H"))))

                .andExpect("$.rewards", hasSize(2))

                .andExpect("$.rewards[0].id", equalTo(LineWinReward.ID))
                .andExpect("$.rewards[0].win", equalTo(50))
                .andExpect("$.rewards[0].positions", equalTo(of(of(0, 1), of(1, 0), of(2, 2))))
                .andExpect("$.rewards[0].payId", equalTo("S1"))

                .andExpect("$.rewards[1].id", equalTo(LineWinReward.ID))
                .andExpect("$.rewards[1].win", equalTo(50))
                .andExpect("$.rewards[1].positions", equalTo(of(of(0, 0), of(1, 0), of(2, 2))))
                .andExpect("$.rewards[1].payId", equalTo("S1"))

                .andExpect("$.events", hasSize(1))
                .andExpect("$.events[0].id", equalTo(StartSceneEvent.ID))
                .andExpect("$.events[0].sceneId", equalTo("free_1"))
                .andExpect("$.events[0].positions", equalTo(of(of(0, 0), of(1, 0), of(2, 2))))
                .andExpect("$.events[0].spins", equalTo(1))
                
                .andExpect("$.additionalInfo.sceneSummary.totalFreeSpinsPlayed", equalTo(0));
    }

    private ResultAction<GameState> trigger() {
        return play("4,12,0,0,0");
    }

    @Test
    public void testExitsOnFirstStage() {
        trigger();

        clientCurrentScene = "free_1";

        List<List<Integer>> positions = of(of(0, 1), of(1, 2), of(2, 2));
        play("0,0,0,0,0")
                .andExpect("$.totalWin", equalTo(50))
                .andExpect("$.roundWin", equalTo(150))

                .andExpect("$.window.reelId", equalTo("free_1"))
                .andExpect("$.window.reels", equalTo(of(
                        of("BL", "BL", "BL", "BL", "BL"),
                        of("S1", "BL", "BL", "BL", "S1"),
                        of("BL", "S1", "S1", "BL", "BL"))))

                .andExpect("$.rewards", hasSize(1))

                .andExpect("$.rewards[0].id", equalTo(LineWinReward.ID))
                .andExpect("$.rewards[0].win", equalTo(50))
                .andExpect("$.rewards[0].positions", equalTo(positions))
                .andExpect("$.rewards[0].payId", equalTo("S1"))

                .andExpect("$.additionalInfo.sceneSummary.totalFreeSpinsPlayed", equalTo(1))
                .andExpect("$.events", hasSize(1))
                .andExpect("$.events[0].id", equalTo(AddFreeSpinsEvent.ID))
                .andExpect("$.events[0].positions", equalTo(positions))
                .andExpect("$.events[0].spins", equalTo(1));

        currentSceneId = MAIN;
        state = SlotsGameState.RESOLVED;
        play("0,0,3,0,0")
                .andExpect("$.totalWin", equalTo(0))
                .andExpect("$.roundWin", equalTo(150))

                .andExpect("$.window.reelId", equalTo("free_1"))
                .andExpect("$.window.reels", equalTo(of(
                        of("BL", "BL", "BL", "BL", "BL"),
                        of("S1", "BL", "BL", "BL", "S1"),
                        of("BL", "S1", "BL", "BL", "BL"))))

                .andExpect("$.rewards", empty())

                .andExpect("$.additionalInfo.sceneSummary.totalFreeSpinsPlayed", equalTo(2))
                .andExpect("$.events", hasSize(1))
                .andExpect("$.events[0].id", equalTo(EndSceneEvent.ID))
                .andExpect("$.events[0].nextSceneId", equalTo(MAIN))
                .andExpect("$.events[0].rewards", hasSize(2));
    }

    @Test
    public void testExitsOnSecondStage() {
        trigger();

        clientCurrentScene = "free_1";

        play("0,0,0,0,0")
                .andExpect("$.additionalInfo.sceneSummary.totalFreeSpinsPlayed", equalTo(1))
                .andExpect("$.window.reelId", equalTo("free_1"));
        currentSceneId = "free_2";
        play("0,0,0,0,0")
                .andExpect("$.window.reelId", equalTo("free_1"))
                .andExpect("$.events", hasSize(1))
                .andExpect("$.events[0].id", equalTo(StartSceneEvent.ID))
                .andExpect("$.events[0].sceneId", equalTo("free_2"))
                .andExpect("$.events[0].positions", equalTo(of(of(0, 1), of(1, 2), of(2, 2))))
                .andExpect("$.events[0].spins", equalTo(1));

        clientCurrentScene = "free_2";
        int win = 75;
        List<List<Integer>> positions = of(of(0, 1), of(1, 1), of(2, 1));
        play("0,0,0,0,0")
                .andExpect("$.totalWin", equalTo(win))
                .andExpect("$.roundWin", equalTo(275))

                .andExpect("$.window.reelId", equalTo("free_2"))
                .andExpect("$.window.reels", equalTo(of(
                        of("BL", "BL", "BL", "BL", "BL"),
                        of("S2", "S2", "S2", "BL", "BL"),
                        of("BL", "BL", "BL", "BL", "BL"))))

                .andExpect("$.rewards", hasSize(1))

                .andExpect("$.rewards[0].id", equalTo(LineWinReward.ID))
                .andExpect("$.rewards[0].win", equalTo(win))
                .andExpect("$.rewards[0].positions", equalTo(positions))
                .andExpect("$.rewards[0].payId", equalTo("S2"))

                .andExpect("$.events", hasSize(1))
                .andExpect("$.events[0].id", equalTo(AddFreeSpinsEvent.ID))
                .andExpect("$.events[0].positions", equalTo(positions))
                .andExpect("$.events[0].spins", equalTo(1))

                .andExpect("$.sceneState.sceneWin", equalTo(75))
                .andExpect("$.sceneState.spins", equalTo(1))
                .andExpect("$.sceneState.spinsPlayed", equalTo(1))
                .andExpect("$.sceneState.initialSpins", equalTo(1))
                .andExpect("$.sceneState.totalSpins", equalTo(2));

        play("0,0,0,0,0")
                .andExpect("$.window.reelId", equalTo("free_2"));
        play("0,0,0,0,0")
                .andExpect("$.window.reelId", equalTo("free_2"));
        play("0,0,0,0,0")
                .andExpect("$.window.reelId", equalTo("free_2"));

        currentSceneId = MAIN;
        state = SlotsGameState.RESOLVED;
        play("0,0,18,0,0")
                .andExpect("$.totalWin", equalTo(0))
                .andExpect("$.roundWin", equalTo(500))

                .andExpect("$.window.reelId", equalTo("free_2"))
                .andExpect("$.window.reels", equalTo(of(
                        of("BL", "BL", "BL", "BL", "BL"),
                        of("S2", "S2", "BL", "BL", "BL"),
                        of("BL", "BL", "BL", "BL", "BL"))))

                .andExpect("$.rewards", empty())

                .andExpect("$.additionalInfo.sceneSummary.totalFreeSpinsPlayed", equalTo(7))
                .andExpect("$.events", hasSize(2))
                .andExpect("$.events[0].id", equalTo(EndSceneEvent.ID))
                .andExpect("$.events[0].sceneWin", equalTo(300))
                .andExpect("$.events[0].nextSceneId", equalTo("free_1"))
                .andExpect("$.events[0].rewards", hasSize(1))
                .andExpect("$.events[1].id", equalTo(EndSceneEvent.ID))
                .andExpect("$.events[1].sceneWin", equalTo(100))
                .andExpect("$.events[1].nextSceneId", equalTo(MAIN))
                .andExpect("$.events[1].rewards", hasSize(2));
    }

    @Test
    public void testExitsOnThirdStage() {
        trigger();

        clientCurrentScene = "free_1";

        play("0,0,0,0,0")
                .andExpect("$.window.reelId", equalTo("free_1"));
        currentSceneId = "free_2";
        play("0,0,0,0,0")
                .andExpect("$.window.reelId", equalTo("free_1"));

        clientCurrentScene = "free_2";
        play("0,0,0,0,0")
                .andExpect("$.window.reelId", equalTo("free_2"));
        play("0,0,0,0,0")
                .andExpect("$.window.reelId", equalTo("free_2"));
        play("0,0,0,0,0")
                .andExpect("$.window.reelId", equalTo("free_2"));
        play("0,0,0,0,0")
                .andExpect("$.window.reelId", equalTo("free_2"));
        currentSceneId = "free_3";
        play("0,0,0,0,0")
                .andExpect("$.window.reelId", equalTo("free_2"))
                .andExpect("$.additionalInfo.sceneSummary.totalFreeSpinsPlayed", equalTo(7))
                .andExpect("$.events", hasSize(1))
                .andExpect("$.events[0].id", equalTo(StartSceneEvent.ID))
                .andExpect("$.events[0].sceneId", equalTo("free_3"))
                .andExpect("$.events[0].positions", equalTo(of(of(0, 1), of(1, 1), of(2, 1))))
                .andExpect("$.events[0].spins", equalTo(1));

        clientCurrentScene = "free_3";
        int win = 415, lines = 3;
        play("0,0,0,7,0")
                .andExpect("$.totalWin", equalTo(lines * win))
                .andExpect("$.roundWin", equalTo(1820))

                .andExpect("$.window.reelId", equalTo("free_3"))
                .andExpect("$.window.reels", equalTo(of(
                        of("BL", "BL", "BL", "S3", "BL"),
                        of("S3", "BL", "BL", "S3", "BL"),
                        of("BL", "S3", "S3", "S3", "S3"))))

                .andExpect("$.rewards", hasSize(3))
                .andExpect("$.rewards[0].win", equalTo(win))
                .andExpect("$.rewards[0].positions", hasSize(5))
                .andExpect("$.rewards[0].payId", equalTo("S3"))

                .andExpect("$.rewards[1].win", equalTo(win))
                .andExpect("$.rewards[1].positions", hasSize(5))
                .andExpect("$.rewards[1].payId", equalTo("S3"))

                .andExpect("$.rewards[2].win", equalTo(win))
                .andExpect("$.rewards[2].positions", hasSize(5))
                .andExpect("$.rewards[2].payId", equalTo("S3"))

                .andExpect("$.events", hasSize(1))
                .andExpect("$.events[0].id", equalTo(AddFreeSpinsEvent.ID))
                .andExpect("$.events[0].spins", equalTo(1))

                .andExpect("$.additionalInfo.sceneSummary.totalFreeSpinsPlayed", equalTo(8))
                .andExpect("$.sceneState.sceneWin", equalTo(lines * win))
                .andExpect("$.sceneState.spins", equalTo(1))
                .andExpect("$.sceneState.initialSpins", equalTo(1))
                .andExpect("$.sceneState.totalSpins", equalTo(2));

        play("0,0,0,7,0")
                .andExpect("$.window.reelId", equalTo("free_3"))
                .andExpect("$.additionalInfo.sceneSummary.totalFreeSpinsPlayed", equalTo(9))
                .andExpect("$.additionalInfo.sceneSummary.scenes.free_1.reelStopsPerSpin", hasSize(2))
                .andExpect("$.additionalInfo.sceneSummary.scenes.free_2.reelStopsPerSpin", hasSize(5))
                .andExpect("$.additionalInfo.sceneSummary.scenes.free_3.reelStopsPerSpin", hasSize(2));

        currentSceneId = MAIN;
        state = SlotsGameState.RESOLVED;
        play("0,42,0,0,0")
                .andExpect("$.window.reelId", equalTo("free_3"))

                .andExpect("$.events", hasSize(3))
                .andExpect("$.events[0].id", equalTo(EndSceneEvent.ID))
                .andExpect("$.events[0].sceneWin", equalTo(2490))
                .andExpect("$.events[0].nextSceneId", equalTo("free_2"))
                .andExpect("$.events[0].rewards", hasSize(1))
                .andExpect("$.events[1].id", equalTo(EndSceneEvent.ID))
                .andExpect("$.events[1].sceneWin", equalTo(375))
                .andExpect("$.events[1].nextSceneId", equalTo("free_1"))
                .andExpect("$.events[1].rewards", hasSize(1))
                .andExpect("$.events[2].id", equalTo(EndSceneEvent.ID))
                .andExpect("$.events[2].sceneWin", equalTo(100))
                .andExpect("$.events[2].nextSceneId", equalTo(MAIN))
                .andExpect("$.events[2].rewards", hasSize(2))

                .andExpect("$.additionalInfo.sceneSummary.totalFreeSpinsPlayed", equalTo(10))
                .andExpect("$.additionalInfo.sceneSummary.scenes.free_1.reelStopsPerSpin", hasSize(2))
                .andExpect("$.additionalInfo.sceneSummary.scenes.free_1.totalSpins", equalTo(2))
                .andExpect("$.additionalInfo.sceneSummary.scenes.free_2.reelStopsPerSpin", hasSize(5))
                .andExpect("$.additionalInfo.sceneSummary.scenes.free_2.totalSpins", equalTo(5))
                .andExpect("$.additionalInfo.sceneSummary.scenes.free_3.reelStopsPerSpin", hasSize(3))
                .andExpect("$.additionalInfo.sceneSummary.scenes.free_3.totalSpins", equalTo(3));
    }
}
