package io.onetouch.slot.beatex.testplay;

import io.onetouch.slot.core.rewards.LineWinReward;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import static io.onetouch.slot.core.utils.GameDefinitionUtils.MAIN;
import static java.util.List.of;
import static org.hamcrest.Matchers.*;

@Slf4j
public class BeatExMainTest extends BeatExAbstractPlayTest {

    @Test
    public void testLoss() {
        play("0,0,0,0,0")
                .andExpect("$.totalWin", equalTo(0))
                .andExpect("$.roundWin", equalTo(0))

                .andExpect("$.window.reelId", equalTo(MAIN))
                .andExpect("$.window.reels", equalTo(of(
                        of("E", "H", "H", "G", "S1"),
                        of("F", "H", "H", "H", "C"),
                        of("B", "B", "S1", "G", "H"))))

                .andExpect("$.rewards", empty())
                .andExpect("$.events", empty());
    }

    @Test
    public void testOneLineWin() {
        int win = 300;
        play("3,22,3,19,3")
                .andExpect("$.totalWin", equalTo(win))
                .andExpect("$.roundWin", equalTo(win))

                .andExpect("$.window.reels", equalTo(of(
                        of("A", "A", "A", "A", "A"),
                        of("S1", "S1", "B", "D", "E"),
                        of("S1", "S1", "C", "E", "C"))))

                .andExpect("$.rewards", hasSize(1))
                .andExpect("$.rewards[0].id", equalTo(LineWinReward.ID))
                .andExpect("$.rewards[0].win", equalTo(win))
                .andExpect("$.rewards[0].positions", equalTo(of(of(0, 0), of(1, 0), of(2, 0), of(3, 0), of(4, 0))))
                .andExpect("$.rewards[0].payId", equalTo("A"))
                .andExpect("$.rewards[0].lineId", equalTo(1))
                
                .andExpect("$.events", empty());
    }

    @Test
    public void testManyLinesWin() {
        int win = 5120;
        play("19,13,17,42,17")
                .andExpect("$.totalWin", equalTo(win))
                .andExpect("$.roundWin", equalTo(win))

                .andExpect("$.window.reels", equalTo(of(
                        of("G", "C", "C", "C", "F"),
                        of("C", "C", "C", "C", "C"),
                        of("C", "D", "D", "E", "C"))))

                .andExpect("$.rewards", hasSize(32))
                .andExpect("$.events", empty());
    }
}
