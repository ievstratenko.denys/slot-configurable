package io.onetouch.slot.beatex.rtp;

import io.onetouch.slot.GameRtpTest;
import io.onetouch.slot.GameRtpTestSettings;
import io.onetouch.slot.beatex.BeatExAbstractTest;
import org.junit.Ignore;
import org.junit.Test;

import static java.util.List.of;

public class BeatExRtpTest extends BeatExAbstractTest {

    private final GameRtpTestSettings settings = GameRtpTestSettings.builder()
            .betSizes(of(1f))
            .coinValues(of(1f))
            .maxRounds(1000_000)
            .build();

    private void test(String gameDefinitionId) {
        settings.setGameDefinitionId(gameDefinitionId);
        new GameRtpTest(gameModule, settings).run().print();
    }

    /**
     * RTP - Expected: 96.174%, Actual: 96.184%
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/345056
     */
    @Test
    @Ignore
    public void test96() {
        test("beats_ex_96");
    }

    /**
     * RTP - Expected: 92.073%, Actual: 92.121%
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/346095
     */
    @Test
    @Ignore
    public void test92() {
        test("beats_ex_92");
    }

    /**
     * RTP - Expected: 94.163%, Actual: 94.172%
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/346438
     */
    @Test
    @Ignore
    public void test94() {
        test("beats_ex_94");
    }
}
