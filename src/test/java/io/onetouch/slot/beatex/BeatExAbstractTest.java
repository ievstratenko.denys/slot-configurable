package io.onetouch.slot.beatex;

import io.onetouch.core.common.type.GameType;
import io.onetouch.slot.configurable.ConfigurableSlotAbstractTest;
import lombok.Getter;

@Getter
public class BeatExAbstractTest extends ConfigurableSlotAbstractTest {

    private final GameType gameType = GameType.BEATS_EX;
}
