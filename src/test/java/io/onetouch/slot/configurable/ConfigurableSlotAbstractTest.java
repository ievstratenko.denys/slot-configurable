package io.onetouch.slot.configurable;

import io.onetouch.BaseGameConfig;
import io.onetouch.GameState;
import io.onetouch.PlayRequest;
import io.onetouch.core.common.type.GameType;
import io.onetouch.slot.api.SlotGameState;
import io.onetouch.slot.api.SlotsGameState;
import io.onetouch.slot.configurable.parsing.SlotConfigParser;
import io.onetouch.slot.core.ResultAction;
import io.onetouch.slot.core.SlotGameDefinition;
import io.onetouch.slot.core.TestUtil;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;

import static io.onetouch.slot.core.TestUtil.totalBet;
import static io.onetouch.slot.core.utils.GameDefinitionUtils.MAIN;
import static org.hamcrest.Matchers.equalTo;

@Slf4j
@Getter
public abstract class ConfigurableSlotAbstractTest {

    protected ConfigurableGameModule gameModule;
    protected SlotGameDefinition gameDefinition;

    protected PlayRequest playRequest;
    protected SlotGameState gameState;
    protected String currentSceneId = MAIN, clientCurrentScene = MAIN;
    protected SlotsGameState state = SlotsGameState.RESOLVED;
    protected BaseGameConfig gameConfig;
    
    protected int betSize = 1, coinValue = 1;
    
    @Before
    public void init() {
        gameState = new SlotGameState();
        gameModule = SlotConfigParser.parse(getGameType());
        gameDefinition = getGameModule().getDefaultGameDefinition();
        initPlayRequest();
    }

    private void initPlayRequest() {
        playRequest = TestUtil.spinRequest(getBetSize(), getCoinValue());
        gameConfig = TestUtil.createDefaultSlotConfig(getGameModule(), getPlayRequest());
    }

    public void setCoinValue(int coinValue) {
        this.coinValue = coinValue;
        initPlayRequest();
    }

    protected abstract GameType getGameType();

    public int getBaseBet() {
        return getClientCurrentScene().equals(MAIN) ? totalBet(getPlayRequest()).intValue() : 0;
    }

    public int getRoundBet() {
        return totalBet(getPlayRequest()).intValue();
    }

    public int getTotalBet() {
        return getBaseBet();
    }

    protected ResultAction<GameState> play(String mock) {
        return play(() -> mock);
    }

    protected ResultAction<GameState> play(TestUtil.Cheat cheat) {
        log.info("Mock {}", cheat.getMockData());
        return TestUtil.play(cheat, getPlayRequest(), getGameModule(), getGameState(), getGameDefinition().getId(), getGameConfig())
                .andExpect("$.baseBet", equalTo(getBaseBet()))
                .andExpect("$.roundBet", equalTo(getRoundBet()))
                .andExpect("$.totalBet", equalTo(getTotalBet()))
                .andExpect("$.coinValue", equalTo(getCoinValue()))
                .andExpect("$.betSize", equalTo(getBetSize()))

                .andExpect("$.currentSceneId", equalTo(getCurrentSceneId()))
                .andExpect("$.clientCurrentScene", equalTo(getClientCurrentScene()))

                .andExpect("$.state", equalTo(getState().name()))
                .andExpect("$.roundStatus", equalTo(getState().name()))

                .andExpect("$.actionState.sceneId", equalTo(getCurrentSceneId()))
                .andExpect("$.sceneState.sceneId", equalTo(getCurrentSceneId()))
                .andExpect("$.gameDefinitionId", equalTo(getGameDefinition().getId()));
    }
}
