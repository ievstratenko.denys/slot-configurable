package io.onetouch.slot.godhand.testplay;

import io.onetouch.GameState;
import io.onetouch.slot.core.ResultAction;
import io.onetouch.slot.core.TestUtil;
import io.onetouch.slot.godhand.GodHandAbstractTest;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.equalTo;

@Getter
public class GodHandAbstractPlayTest extends GodHandAbstractTest {

    protected final int coinValue = 20; // for simplicity it's the same as the Credits amount
    
    @Override
    protected ResultAction<GameState> play(String mock) {
        List<Integer> reelStops = Arrays.stream(mock.split(TestUtil.Cheat.DELIMITER))
                .limit(1)
                .map(Integer::valueOf)
                .collect(Collectors.toList());
        return super.play(mock)
                .andExpect("$.window.reelStops", equalTo(reelStops));
    }
}
