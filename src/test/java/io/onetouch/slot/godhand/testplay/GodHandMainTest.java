package io.onetouch.slot.godhand.testplay;

import io.onetouch.slot.core.rewards.LineWinReward;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import static io.onetouch.slot.core.utils.GameDefinitionUtils.MAIN;
import static java.util.List.of;
import static org.hamcrest.Matchers.*;

@Slf4j
public class GodHandMainTest extends GodHandAbstractPlayTest {

    @Test
    public void testLoss() {
        play("4")
                .andExpect("$.totalWin", equalTo(0))
                .andExpect("$.roundWin", equalTo(0))

                .andExpect("$.window.reelId", equalTo(MAIN))
                .andExpect("$.window.reels", equalTo(of(of("BL"))))

                .andExpect("$.rewards", empty())
                .andExpect("$.events", empty());
    }

    @Test
    public void testWinD() {
        int win = 20;
        String symbol = "D";
        play("0,0,0")
                .andExpect("$.totalWin", equalTo(win))
                .andExpect("$.roundWin", equalTo(win))

                .andExpect("$.window.reels", equalTo(of(of(symbol))))

                .andExpect("$.rewards", hasSize(1))
                .andExpect("$.rewards[0].id", equalTo(LineWinReward.ID))
                .andExpect("$.rewards[0].win", equalTo(win))
                .andExpect("$.rewards[0].positions", equalTo(of(of(0, 0))))
                .andExpect("$.rewards[0].payId", equalTo(symbol))
                .andExpect("$.rewards[0].lineId", equalTo(1))

                .andExpect("$.events", hasSize(2))
                .andExpect("$.events[0].id", equalTo("event_Normal_Win"))
                .andExpect("$.events[1].id", equalTo("event_Ball_Blue"));
    }

    @Test
    public void testWinF() {
        int win = 60;
        String symbol = "F";
        play("1,4,1")
                .andExpect("$.totalWin", equalTo(win))
                .andExpect("$.window.reels", equalTo(of(of(symbol))))
                .andExpect("$.rewards", hasSize(1))
                .andExpect("$.rewards[0].payId", equalTo(symbol))

                .andExpect("$.events", hasSize(2))
                .andExpect("$.events[0].id", equalTo("event_God_Flash_Sp2_Blue"))
                .andExpect("$.events[1].id", equalTo("event_Ball_Green"));
    }

    @Test
    public void testWinG() {
        int win = 100;
        String symbol = "G";
        play("2,3,2")
                .andExpect("$.totalWin", equalTo(win))
                .andExpect("$.window.reels", equalTo(of(of(symbol))))
                .andExpect("$.rewards", hasSize(1))
                .andExpect("$.rewards[0].payId", equalTo(symbol))

                .andExpect("$.events", hasSize(2))
                .andExpect("$.events[0].id", equalTo("event_monkeys_3"))
                .andExpect("$.events[1].id", equalTo("event_Ball_Red"));
    }
}
