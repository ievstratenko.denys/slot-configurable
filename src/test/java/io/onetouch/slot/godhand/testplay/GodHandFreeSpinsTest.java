package io.onetouch.slot.godhand.testplay;

import io.onetouch.GameState;
import io.onetouch.slot.api.SlotsGameState;
import io.onetouch.slot.core.ResultAction;
import io.onetouch.slot.core.events.AddFreeSpinsEvent;
import io.onetouch.slot.core.events.EndSceneEvent;
import io.onetouch.slot.core.events.StartSceneEvent;
import io.onetouch.slot.core.rewards.LineWinReward;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.hamcrest.Matcher;
import org.junit.Test;

import java.util.Collection;

import static io.onetouch.slot.core.TestUtil.Cheat.DELIMITER;
import static io.onetouch.slot.core.utils.GameDefinitionUtils.MAIN;
import static java.util.List.of;
import static org.hamcrest.Matchers.*;

@Slf4j
@Getter
public class GodHandFreeSpinsTest extends GodHandAbstractPlayTest {

    protected final String[] FREE_SCENE_IDS = {"free_a", "free_b", "free_c", "free_d", "free_e"};
    protected final int DEFAULT_FREE_SCENE_INDEX = 4;

    private final static int FREE_SPINS_COUNT = 10;
    private final static int LAST_FREE_SPIN = FREE_SPINS_COUNT - 1;

    private final static int VISUAL_EVENT_INDEX = 0;
    private final static String VISUAL_EVENT_ID = "event_FS_Coin";

    protected String currentSceneId = FREE_SCENE_IDS[DEFAULT_FREE_SCENE_INDEX];
    protected SlotsGameState state = SlotsGameState.FREE_GAME;

    private void trigger() {
        trigger(DEFAULT_FREE_SCENE_INDEX);
    }

    private ResultAction<GameState> trigger(int freeSceneOption) {
        return play("3,11,0," + freeSceneOption + ",0");
    }

    @Test
    public void testTriggerAllOptions() {
        int win = 0;
        for (int i = 0; i < FREE_SCENE_IDS.length; i++) {
            currentSceneId = FREE_SCENE_IDS[i];
            init();
            trigger(i)
                    .andExpect("$.totalWin", equalTo(win))
                    .andExpect("$.roundWin", equalTo(win))

                    .andExpect("$.window.reelId", equalTo(MAIN))
                    .andExpect("$.window.reels", equalTo(of(of("FG"))))

                    .andExpect("$.rewards", empty())

                    .andExpect("$.events", hasSize(4))
                    .andExpect("$.events[0].id", equalTo("event_God_Flash_Sp2_Gold"))
                    .andExpect("$.events[1].id", equalTo("event_Ball_Gold"))
                    .andExpect("$.events[2].id", equalTo(StartSceneEvent.ID))
                    .andExpect("$.events[2].sceneId", equalTo(FREE_SCENE_IDS[i]))
                    .andExpect("$.events[2].positions", equalTo(of(of(0, 0))))
                    .andExpect("$.events[2].spins", equalTo(10))
                    .andExpect("$.events[3].id", equalTo("event_gr_monkeys_1"));
        }
    }

    @Test
    public void testFullFlow() {
        trigger();
        String[] symbols = {"H", "I", "J", "K", "L", "M"};
        int[] wins = {200, 400, 600, 1000, 2000, 6000};
        int roundWin = 0;

        clientCurrentScene = currentSceneId;

        for (int i = 0; i < FREE_SPINS_COUNT; i++) {
            int mock = i % wins.length;
            int win = wins[mock];
            roundWin += win;
            String symbol = symbols[mock];
            Matcher<Collection<?>> eventsMatcher = hasSize(i == LAST_FREE_SPIN ? 2 : 1);
            String mockStr = mock + DELIMITER + VISUAL_EVENT_INDEX;

            if (i == 0) {
                int notToTrigger = 0;
                mockStr += DELIMITER + notToTrigger;
            }
            if (i == LAST_FREE_SPIN) {
                currentSceneId = MAIN;
                state = SlotsGameState.RESOLVED;
            }

            ResultAction<GameState> resultAction = play(mockStr)
                    .andExpect("$.totalWin", equalTo(win))
                    .andExpect("$.roundWin", equalTo(roundWin))

                    .andExpect("$.window.reels", equalTo(of(of(symbol))))

                    .andExpect("$.rewards", hasSize(1))
                    .andExpect("$.rewards[0].id", equalTo(LineWinReward.ID))
                    .andExpect("$.rewards[0].win", equalTo(win))
                    .andExpect("$.rewards[0].positions", equalTo(of(of(0, 0))))
                    .andExpect("$.rewards[0].payId", equalTo(symbol))
                    .andExpect("$.rewards[0].lineId", equalTo(1))

                    .andExpect("$.events", eventsMatcher)
                    .andExpect("$.events[0].id", equalTo(VISUAL_EVENT_ID));

            if (i == LAST_FREE_SPIN) {
                resultAction
                        .andExpect("$.events[1].id", equalTo(EndSceneEvent.ID))
                        .andExpect("$.events[1].nextSceneId", equalTo(MAIN))
                        .andExpect("$.events[1].sceneWin", equalTo(roundWin));
            }
        }
    }

    @Test
    public void testFullFlowWithRetrigger() {
        trigger();
        clientCurrentScene = currentSceneId;
        int win = 200;
        String symbol = "H";
        int totalFreeSpins = 2 * FREE_SPINS_COUNT;

        for (int i = 1; i <= totalFreeSpins; i++) {
            int events = 1; // regular visual event
            String mockStr = 0 + DELIMITER + VISUAL_EVENT_INDEX;

            if (i == 1) {
                events += 2; // addFreeSpins, eventBGM
                int doRetrigger = 1;
                int eventBGM_On = 1;
                mockStr += DELIMITER + doRetrigger + DELIMITER + eventBGM_On;
            }
            if (i == FREE_SPINS_COUNT) {
                events ++; // eventMonkeyIndex
                int eventMonkeyIndex = 1;
                mockStr += DELIMITER + eventMonkeyIndex;
            }
            if (i == FREE_SPINS_COUNT + 1) {
                int doRetrigger = 0;
                mockStr += DELIMITER + doRetrigger;
            }
            if (i == totalFreeSpins) {
                events++; // endSceneEvent
                currentSceneId = MAIN;
                state = SlotsGameState.RESOLVED;
            }

            Matcher<Collection<?>> eventsMatcher = hasSize(events);
            ResultAction<GameState> resultAction = play(mockStr)
                    .andExpect("$.totalWin", equalTo(win))
                    .andExpect("$.roundWin", equalTo(win * i))
                    .andExpect("$.window.reels", equalTo(of(of(symbol))))
                    .andExpect("$.rewards", hasSize(1))
                    .andExpect("$.events", eventsMatcher)
                    .andExpect("$.events[0].id", equalTo(VISUAL_EVENT_ID));

            if (i == 1) {
                resultAction
                        .andExpect("$.events[1].id", equalTo(AddFreeSpinsEvent.ID))
                        .andExpect("$.events[1].spins", equalTo(FREE_SPINS_COUNT))
                        .andExpect("$.events[1]", not(hasKey("positions")))
                        .andExpect("$.events[2].id", equalTo("event_BGM_On"));
            }

            if (i == FREE_SPINS_COUNT) {
                resultAction
                        .andExpect("$.events[1].id", equalTo("event_gr_monkeys_2"));
            }

            if (i == totalFreeSpins) {
                resultAction
                        .andExpect("$.events[1].id", equalTo(EndSceneEvent.ID))
                        .andExpect("$.events[1].nextSceneId", equalTo(MAIN))
                        .andExpect("$.events[1].sceneWin", equalTo(win * 2 * FREE_SPINS_COUNT));
            }
        }
    }
}
