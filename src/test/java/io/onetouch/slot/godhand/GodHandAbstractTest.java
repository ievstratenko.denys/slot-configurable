package io.onetouch.slot.godhand;

import io.onetouch.core.common.type.GameType;
import io.onetouch.slot.configurable.ConfigurableSlotAbstractTest;
import lombok.Getter;

@Getter
public class GodHandAbstractTest extends ConfigurableSlotAbstractTest {

    private final GameType gameType = GameType.GOD_HAND;
}
