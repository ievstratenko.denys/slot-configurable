package io.onetouch.slot.godhand.rtp;

import io.onetouch.slot.GameRtpTest;
import io.onetouch.slot.GameRtpTestSettings;
import io.onetouch.slot.godhand.GodHandAbstractTest;
import org.junit.Ignore;
import org.junit.Test;

import static java.util.List.of;

public class GodHandRtpTest extends GodHandAbstractTest {

    private final GameRtpTestSettings settings = GameRtpTestSettings.builder()
            .betSizes(of(1f))
            .coinValues(of(1f))
            .maxRounds(100_000_000)
            .build();

    private void test(String gameDefinitionId) {
        settings.setGameDefinitionId(gameDefinitionId);
        new GameRtpTest(gameModule, settings).run().print();
    }

    /**
     * 2B
     * Expected: 96.155%
     * Actual  : 96.121%
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/348018
     */
    @Test
    @Ignore
    public void test96() {
        test("god_hand_96");
    }

    /**
     * 2B
     * Expected: 94.155%
     * Actual  : 94.203%
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/348014
     */
    @Test
    @Ignore
    public void test94() {
        test("god_hand_94");
    }

    /**
     * 2B
     * Expected: 92.194%
     * Actual  : 92.176%
     * https://gitlab.onetouch.io/game-engine/onetouch-slot-games/-/jobs/348016
     */
    @Test
    @Ignore
    public void test92() {
        test("god_hand_92");
    }
}
