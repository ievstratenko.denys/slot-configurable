# Slot Configurable

A PoC for a slots engine which allows to configure slots via JSON.

This is not a 'ready to use' tool as it's actually a Gradle module taken from a common project. 

I'm allowed to share only this part because it was created in non-working hours and it's not in production use.

The main goal of the PoC is to allow creation of slot configs with no Java code.

It allows to configure a slot in a declarative way using JSON format (find examples in src/main/resources)

